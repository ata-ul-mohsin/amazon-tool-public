@extends('main_template')
@section ("title")
    Brands to Product
@stop
@section("right_header")
    <!--<div class="col-sm-6" xmlns="http://www.w3.org/1999/html">
        <div class="add_link">
            <a href="<?php echo url('brands/create'); ?>"><span class="fa fa-plus"></span> Add New</a>
        </div>
    </div>-->
@stop
@section("content")
    <form class="bs-example form-horizontal" enctype="multipart/form-data" id="productform"  data-validate="parsley" >
        <div class="row">
            <div class="col-md-12">
                {!! csrf_field() !!}
                <div class="form-group">
                    <label class="col-lg-4 control-label">Account :</label>
                    <div class="col-lg-4">
                        <select class="form-control chosen_select" name="account" id="region">
                            <option selected="selected" value="">~Select~</option>
                            <option value="acc1">Acc1</option>
                            <option value="acc2">Acc2</option>
                            <option value="acc3">Acc3</option>
                            <option value="acc4">Acc4</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label">Brand :</label>
                    <div class="col-lg-4">
                        <select class="form-control chosen_select" name="brand" >
                            <option selected="selected" value="">~Select~</option>
                            <option value="brand1">Brand1</option>
                            <option value="brand2">Brand2</option>
                            <option value="brand3">Brand3</option>
                            <option value="brand4">Brand4</option>
                        </select>
                    </div>
                </div>


            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <div class="table-responsive">
                    <table class="table table-bordered table-striped data_table" data-ride="datatables" id="productdata">
                        <thead>
                        <tr bgcolor="#f4f4f4">
                            <th>ASIN</th>
                            <th>Product Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!empty($brands) && is_array($brands))

                            <tr>
                                <td>brand1</td>
                                <td>product1</td>
                                <td nowrap>

                                    <input type="checkbox" name="action_brand1" value="1">
                                </td>
                            </tr>

                            <tr>
                                <td>brand2</td>
                                <td>product2</td>
                                <td nowrap>

                                    <input type="checkbox" name="action_brand2" value="2">
                                </td>
                            </tr>
                            <tr>
                                <td>brand3</td>
                                <td>product3</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand3" value="3">
                                </td>
                            </tr>
                            <tr>
                                <td>brand4</td>
                                <td>product4</td>
                                <td nowrap>

                                    <input type="checkbox" name="action_brand4" value="4">
                                </td>
                            </tr>
                            <tr>
                                <td>brand5</td>
                                <td>product5</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand5" value="5">
                                </td>
                            </tr>
                            <tr>
                                <td>brand6</td>
                                <td>product7</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand6" value="6">
                                </td>
                            </tr>
                            <tr>
                                <td>brand7</td>
                                <td>product7</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand7" value="7">
                                </td>
                            </tr>
                            <tr>
                                <td>brand8</td>
                                <td>product8</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand8" value="8">
                                </td>
                            </tr>
                            <tr>
                                <td>brand9</td>
                                <td>product9</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand9" value="9">
                                </td>
                            </tr>
                            <tr>
                                <td>brand10</td>
                                <td>product10</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand10" value="10">
                                </td>
                            </tr>
                            <tr>
                                <td>brand11</td>
                                <td>product11</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand11" value="11">
                                </td>
                            </tr>
                            <tr>
                                <td>brand12</td>
                                <td>product12</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand12" value="12">
                                </td>
                            </tr>
                            <tr>
                                <td>brand13</td>
                                <td>product13</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand13" value="13">
                                </td>
                            </tr>
                            <tr>
                                <td>brand14</td>
                                <td>product14</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand14" value="14">
                                </td>
                            </tr>
                            <tr>
                                <td>brand15</td>
                                <td>product15</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand15" value="15">
                                </td>
                            </tr>
                            <tr>
                                <td>brand16</td>
                                <td>product16</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand16" value="16">
                                </td>
                            </tr>
                            <tr>
                                <td>brand17</td>
                                <td>product17</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand17" value="17">
                                </td>
                            </tr>
                            <tr>
                                <td>brand18</td>
                                <td>product18</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand18" value="18">
                                </td>
                            </tr>
                            <tr>
                                <td>brand19</td>
                                <td>product19</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand19" value="19">
                                </td>
                            </tr>
                            <tr>
                                <td>brand20</td>
                                <td>product20</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand20" value="20">
                                </td>
                            </tr>
                            <tr>
                                <td>brand31</td>
                                <td>product31</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand21" value="21">
                                </td>
                            </tr>
                            <tr>
                                <td>brand32</td>
                                <td>product32</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand22" value="22">
                                </td>
                            </tr>
                            <tr>
                                <td>brand33</td>
                                <td>produc33</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand23" value="23">
                                </td>
                            </tr>
                            <tr>
                                <td>brand34</td>
                                <td>product34</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand24" value="24">
                                </td>
                            </tr>
                            <tr>
                                <td>brand35</td>
                                <td>product35</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand25" value="25">
                                </td>
                            </tr>
                            <tr>
                                <td>brand36</td>
                                <td>product36</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand26" value="26">
                                </td>
                            </tr>
                            <tr>
                                <td>brand37</td>
                                <td>product37</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand27" value="27">
                                </td>
                            </tr>
                            <tr>
                                <td>brand38</td>
                                <td>product39</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand28" value="28">
                                </td>
                            </tr>
                            <tr>
                                <td>brand40</td>
                                <td>product41</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand29" value="29">
                                </td>
                            </tr>
                            <tr>
                                <td>brand42</td>
                                <td>product42</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand30" value="30">
                                </td>
                            </tr>
                            <tr>
                                <td>brand43</td>
                                <td>product43</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand31" value="31">
                                </td>
                            </tr>
                            <tr>
                                <td>brand44</td>
                                <td>product44</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand32" value="32">
                                </td>
                            </tr>
                            <tr>
                                <td>brand45</td>
                                <td>product45</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand33" value="33">
                                </td>
                            </tr>
                            <tr>
                                <td>brand46</td>
                                <td>product46</td>
                                <td nowrap>
                                    <input type="checkbox" name="action_brand34" value="34">
                                </td>
                            </tr>


                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="row">
                <div class="col-md-12 text-center button-top">

                    {!! Form::submit('Save', ['id' => 'savedata','name' => 'save','class' => 'btn btn-primary']) !!}
                    {!! Form::submit('Save &amp; Add New', ['name' => 'save_add','class' => 'btn btn-black']) !!}
                </div>
            </div>
        </div>

    </form>
@stop
<script type="text/javascript" src="resources/assets/js/jquery-1.12.0.min.js"></script>
<script type="text/javascript" src="resources/assets/js/jquery.dataTables.js"></script>
<script type="text/javascript">

    /*
     * Function: fnGetHiddenTrNodes
     * Purpose:  Get all of the hidden TR nodes (i.e. the ones which aren't on display)
     * Returns:  array:
     * Inputs:   object:oSettings - DataTables settings object
     */
    /* $.fn.dataTableExt.oApi.fnGetHiddenTrNodes = function ( oSettings )
     {
     *//* Note the use of a DataTables 'private' function thought the 'oApi' object *//*
     var anNodes = this.oApi._fnGetTrNodes( oSettings );
     var anDisplay = $('tbody tr', oSettings.nTable);

     *//* Remove nodes which are being displayed *//*
     for ( var i=0 ; i<anDisplay.length ; i++ )
     {
     var iIndex = jQuery.inArray( anDisplay[i], anNodes );
     if ( iIndex != -1 )
     {
     anNodes.splice( iIndex, 1 );
     }
     }

     *//* Fire back the array to the caller *//*
     return anNodes;
     }*/

    /* Init the table and fire off a call to get the hidden nodes. */
    var oTable;
    $(document).ready(function() {

        var oTable = $('#productdata').dataTable();

        $('#savedata').click( function () {
            alert('hello');
            event.preventDefault();
            var nHidden = oTable.fnGetHiddenTrNodes();
            //var hiddenCheckboxes = $(oTable.fnGetHiddenNodes()).find('input[type=checkbox]:checked');
            //alert(nHidden.length );
            console.log(nHidden);
        } );
    } );


</script>