<?php
/*
 * $old_logo   =   "brand-logo-placeholder.png";
if(!empty($brand['logo']))
    $old_logo   =   $brand['logo'];
*/
$old_logo   =   "";
if(!empty($brand['logo'])){
    $old_logo   =   $brand['logo'];
}
$logo_file=file_exists('assets/users/brands/logos/'.$old_logo);
if($logo_file!=1){
    $logo_file=0;
}
if($old_logo==''){
    $old_logo   =   "brand-logo-placeholder.png";
}
else if($old_logo!='' and $logo_file==1){
    $old_logo   =   $brand['logo'];
}
else if($old_logo!='' and $logo_file==0){
    $old_logo   =   "not-found.png";
}
?>
@extends('main_template')
@section ("title")
    Brand Definition
@stop
@section("content")
@if(isset($brand))
    {!! Form::model($brand, ['route' => ['brands.update', $brand->id], 'method' => 'patch', 'data-parsley-validate' => '', 'enctype' => "multipart/form-data" ]) !!}
@else
    {!! Form::open(['route' => 'brands.store', 'data-parsley-validate' => " ", 'enctype' => "multipart/form-data" ]) !!}
@endif
<div class="row">
    <div class="col-md-6">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Brand Name:</label>
                <div class="col-sm-7">
            {!! Form::text('name', Input::old('name'), ['class' => 'form-control','data-parsley-trigger' => 'change','required' => '']) !!}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Brand Signature:</label>
                <div class="col-sm-7">
            {!! Form::textarea('signature', Input::old('signature'), [ 'rows' => '5','class' => 'form-control','data-parsley-trigger' => 'change','required' => '']) !!}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Seller Account:</label>
                <div class="col-sm-7">
             {!! Form::select('setting_id', $marketplace_settings, Input::old('setting_id'), ['id' => 'setting_id', 'class' => 'form-control','data-parsley-trigger' => 'change','required' => '']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group"> <img src="<?php echo asset('assets/users/brands/logos/'.$old_logo); ?>" style="width: 150px !important;height: 150px !important" alt=""> </div>
        <div class="form-group">
            <label>Brand Logo:</label>
            <div class="input-group">
                 <input type="text" class="form-control" readonly>
                <span class="input-group-btn"> <span class="btn btn-default btn-file"> Browse&hellip;
                        {!! Form::file('brand_logo', ['class' => 'field']) !!}
                </span> </span> </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        {!! Form::hidden('id',Input::old('id'), ['name' => 'id']) !!}
        {!! Form::submit('Save', ['name' => 'save','class' => 'btn btn-primary','value' => '1']) !!}
        {!! Form::submit('Save &amp; Add New', ['name' => 'save_add','class' => 'btn btn-black']) !!}
    </div>
</div>
{!! Form::close() !!}
@stop