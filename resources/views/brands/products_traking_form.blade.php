@extends('main_template')
@section ("title")
    Product Definition
@stop
@section('content')
    <div class="col-sm-12">
        {!! Form::open(['route' => 'track-product', 'data-parsley-validate' => " ", 'enctype' => "multipart/form-data" ]) !!}
        <div class="form-horizontal">

            <div class="form-group">
                <label class="col-sm-4 control-label">Seller Account:</label>
                <div class="col-sm-4">
                    {!! Form::select('setting_id', $marketplace_settings, Input::old('setting_id'), ['id' => 'setting_id', 'class' => 'form-control chosen_select','data-parsley-trigger' => 'change','required' => '']) !!}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">ASIN:</label>
                <div class="col-sm-4">
                    {!! Form::text('asin', Input::old('asin'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-4 col-sm-offset-4">
                    {!! Form::submit('  Save  ', ['name' => 'save','class' => 'btn btn-primary', 'type' => "button"]) !!}
                </div>


            </div>
        </div>



        {!! Form::close() !!} </div>
@stop