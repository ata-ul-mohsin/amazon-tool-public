@extends('main_template')
@section ("title")
    Brand List
@stop
@section("right_header")
    <div class="col-sm-6" xmlns="http://www.w3.org/1999/html">
        <div class="add_link">
            <a href="<?php echo url('brands/create'); ?>"><span class="fa fa-plus"></span> Add New</a>
        </div>
    </div>
@stop
@section("content")
    <div class="row">
        <div class="col-md-12">

            <div class="table-responsive">
                <table class="table table-bordered table-striped data_table" data-ride="datatables">
                    <thead>
                    <tr bgcolor="#f4f4f4">
                        <th>Brand Name</th>
                        <th>Logo</th>
                        <th>Signature</th>
                        <th>Account Name</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($brands) && is_array($brands))
                        @foreach($brands as $brand)
                            <?php
                            $logo=$brand['logo'];
                            $logo_file=file_exists('assets/users/brands/logos/'.$logo);
                            if($logo_file!=1)
                                $logo_file=0;
                            ?>
                            <tr>
                                <td> {{{ $brand['name'] }}}</td>
                                <td><?php if($logo==''){?><img src='<?php echo asset("assets/users/brands/logos/brand-logo-placeholder.png"); ?>' width='50' height='50' /><?php }else if($logo!='' and $logo_file==1){?> <img src='<?php echo asset("assets/users/brands/logos/".$brand['logo']); ?>' width='50' height='50' /> <?php } else if($logo!='' and $logo_file==0){?><img src='<?php echo asset("assets/users/brands/logos/not-found.png"); ?>' width='50' height='50' /><?php }?></td>
                                <td> {{{ $brand['signature'] }}}</td>
                                <td> {{{ $brand['sellers']['account_name']. "  (".$brand['sellers']['seller_id'].")" }}}</td>
                                <td nowrap>
                                    <a href=" {{{ route('brands.edit',array("id"=>$brand['id'])) }}}"class="btn btn-default"><i class="fa fa-pencil text"></i></a>&nbsp;
    <a data-href="{!! route('brands.destroy',array('id'=>$brand['id'])) !!}" class="btn btn-default  confirm_delete"><i class="fa fa-trash-o text"></i></a>

                                </td>

                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
