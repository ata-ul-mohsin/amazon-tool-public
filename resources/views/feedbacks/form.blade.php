@extends('main_template')
@section ("title")
    Feedback Detail
@stop
@section("right_header")
    <!--<div class="col-sm-6" xmlns="http://www.w3.org/1999/html">
        <div class="add_link">
            <a href="<?php echo url('brands/create'); ?>"><span class="fa fa-plus"></span> Add New</a>
        </div>
    </div>-->
@stop
@section("content")
    <div class="row">
        <div class="col-md-5 nopadding">


            <h3>Order Information</h3>

            <table>
                <tr>
                    <td>Order ID:</td>
                    <td><a target="_blank" href="https://sellercentral.amazon.com/gp/orders-v2/details/ref=sm_orddet_cont_myo?ie=UTF8&orderID="> [SC]</a></td>
                </tr>
                <tr>
                    <td>Status:</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Time:</td>
                    <td>01/21/2016 14:14</td>
                </tr>
                <tr>
                    <td>Buyer:</td>
                    <td> <a href="mailto:">{{ $feedback->rater }}</a></td>
                </tr>
                <tr>
                    <td>Shipped To:</td>
                    <td><br />
                        <br />
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-md-7 nopadding">
            <form method="post" action="{{  url("feedbacks/update",array("id"=>$feedback->id)) }}">
                {!! csrf_field() !!}
                <input type="hidden" name="id" value="71dc668fdc51d69f2336ce194a7eb9dd" />
                <input type="hidden" name="action" value="save" />
                <table style="float:left; width:400px;margin-top:30px;">
                    <tr>
                        <td>Feedback Left:</td>
                        <td>{{ $feedback->rating_date }}</td>
                    </tr>
                    <tr>
                        <td>Rating:</td>
                        <td>{{ $feedback->rating }}</td>
                    </tr>
                    <tr>
                        <td>Comments:</td>
                        <td>{{ $feedback->rating_text }}</td>
                    </tr>
                    <tr>
                        <td>On Time:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>As Described:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Customer Service:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>FeedbackGenius Status:</td>
                        <td> <select class="" id="status" name="status" >
                                <option value="New" >New</option>
                                <option value="Collected" >Collected</option>
                                <option value="Waiting for user response" >Waiting for user response</option>
                                <option value="Responded" >Responded</option>
                                <option value="Ticket Opened" >Ticket Opened</option>
                                <option value="Resolved" >Resolved</option>
                                <option value="Unresolvable" >Unresolvable</option>
                            </select>
                            <input type="submit"  value="Save" />
                        </td>
                    </tr>
                    <tr><td valign="top">Notes</td><td valign="top">
                            <textarea class="clean" type="text" name="notes" id="notes" rows="4" cols="30"  /></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <ul style="list-style: none;">
                                <li><a href="#">Open a Case</a></li>
                            </ul>

                        </td>
                    </tr>

                </table>
            </form>
        </div>
    </div>
    </div>
@stop
<script type="text/javascript" src="resources/assets/js/jquery-1.12.0.min.js"></script>