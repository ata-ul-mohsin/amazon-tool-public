

@extends('main_template')

@section ("title")

    Feedbacks

@stop

@section("right_header")

    <!--<div class="col-sm-6" xmlns="http://www.w3.org/1999/html">

        <div class="add_link">

            <a href="<?php echo url('brands/create'); ?>"><span class="fa fa-plus"></span> Add New</a>

        </div>

    </div>-->

@stop

@section("content")

    <form class="bs-example form-horizontal" enctype="multipart/form-data" id="productform"  data-validate="parsley" >

        <div class="row">

            <div class="col-md-12">

                {!! csrf_field() !!}

                <div class="form-group">

                 <?php /*   <label class="col-lg-4 control-label">Market Place :</label> */ ?>



                </div>



                <div class="form-group">

                    <?php /*     <label class="col-lg-4 control-label">Rating :</label> */ ?>

                        <div class="col-lg-4">

                            <div  name="marketplace_select" id="marketplace_select">


                            </div>



                        </div>

                    <div class="col-lg-4">



                            <div  name="rating_select" id="rating_select">



                            </div>



                    </div>

                        <div class="col-lg-4">

                            <div  name="status_select" id="status_select" >

                            </div>

                        </div>

                </div>

                <div class="form-group">

                    <?php /*      <label class="col-lg-4 control-label">Status :</label> */ ?>



                </div>

            </div>

        </div>

        <div class="row">

            <div class="col-md-12">

                <div class="table-responsive">

                    <table class="table table-bordered table-striped" data-ride="datatables" id="server_sisde_dtable">

                        <thead>

                        <tr bgcolor="#f4f4f4">

                            <th width="10%">Sr</th>

                            <th width="15%">Buyer</th>

                           <?php /* <th width="15%"Date</th> */ ?>

                            <th width="10%">Rating</th>

                            <th width="10%">Status</th>

                            <th width="40%">Comment</th>

                        </tr>

                        </thead>

                        <tbody>



                        <?php

                                /*

                                if(!empty($feedbacks) && is_array($feedbacks))

                                    foreach($feedbacks as $feedback)

                                {

                            ?>

                        <tr>

                            <td><a href="<?php echo url('feedback-detail'); ?>"><?php echo $feedback['rater']; ?></a></td>

                            <td><?php echo $feedback['rating_date']; ?></td>

                            <td><?php echo $feedback['rating']; ?></td>

                            <td><?php //echo $feedback['']; ?></td>

                            <td><?php echo $feedback['rating_text']; ?></td>



                        </tr>

                    <?php

}

*/



    ?>



                        </tbody>

                    </table>

                </div>

            </div>

        </div>



        </div>



    </form>

@stop

@section('additional_js')

<script>

    $(function() {

        var table   =   $('#server_sisde_dtable').DataTable( {

            "processing": true,

            "serverSide": true,

            "ajax": "<?php echo url("feedbacks-data"); ?>",

            fnInitComplete : function() {

                this.columnFilter({

                    sPlaceHolder: "head:after",

                    aoColumns: [

                        { sSelector: "#marketplace_select", type: "select", values: [<?php echo  $setting_str; ?>]},

                        null,

                       

                        { sSelector: "#rating_select", type: "select", values: ['1','2','3','4','5']},

                        { sSelector: "#status_select",type: "select", values: ['Collected','New','Waiting for user response','Responded','Ticket Opened','Resolved','Unresolvable']},

                        null

                            ]

                });

            }

        });





    } );





</script>



@stop