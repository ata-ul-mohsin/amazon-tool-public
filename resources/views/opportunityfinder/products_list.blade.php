
@extends('main_template')
@section ("title")
Opportunity Finder
@stop
@section("content")

<form action="<?php echo url('opportunityfinder/find_opportunity') ?>"  method="post"  id="publisher_form" data-validate="parsley" >

    {!! csrf_field() !!}
    <div class="row">
        <div class="col-md-4">
            <div class="form-horizontal">
                <div class="form-bar">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <input type="text" name="keywords" id="keywords" placeholder="Keyword Search..." class="form-control">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="default" id="default" />
                                    Default </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="top_seller" checked="" id="top_seller" />
                            Top Seller </label>
                    </div>
                </div>

                <div class="form-group" style="height: 200px; overflow: scroll;">
                    <table class="table" >
                        <tbody>
                            <tr>
                                <th>Category</th>
                                <th>Action</th>
                            </tr>

                            <tr>
                                <td>All Departments</td>
                                <td><label><input type="checkbox" name="category[]" value="0"></td>
                                        </tr>
                                        <?php
                                        if (!empty($categories)) {

                                            foreach ($categories as $category) {
                                                if ($category['marketplace'] == 'webservices.amazon.com') {
                                                    $marketplace = 1;
                                                } elseif ($category['marketplace'] == 'webservices.amazon.co.uk') {
                                                    $marketplace = 2;
                                                }
                                                ?>
                                                <tr class="cls-<?php echo $marketplace; ?>" style="<?php echo ($category['marketplace'] != 'webservices.amazon.com') ? 'display:none;' : ''; ?>">
                                                    <td><?php echo $category['name']; ?></td>
                                                    <td>
                                                        <input type="checkbox" name="category[]" value="<?php echo $category['browse_nodes']; ?>">
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-md-offset-2">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-inline">
                        <div class="form-group">
                            <label>Market Place</label>
                            <select name="marketplace" id="marketplace">
                                <option value="1" selected="">US</option>
                                <option value="2">UK</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <table class="table-form default-wrap">
                <tbody><tr>
                        <th></th>
                        <th>Min</th>
                        <th>Max</th>
                        <th>Default</th>
                    </tr>
                    <tr>
                        <td>Price:</td>
                        <td><input type="text" name="min_price" id="min_price" class="form-control" ></td>
                        <td><input type="text" name="max_price" id="max_price" class="form-control"></td>
                        <td><div class="checkbox">
                                <label>
                                    <input type="checkbox" name="price_default" id="price_default">
                                    Low </label>
                            </div></td>
                    </tr>
                    <tr>
                        <td>Rank:</td>
                        <td><input type="text" name="min_rank" id="min_rank" class="form-control"></td>
                        <td><input type="text" name="max_rank" id="max_rank"  class="form-control"></td>
                        <td><div class="checkbox">
                                <label>
                                    <input name="rank_default" id="rank_default" type="checkbox" checked="">
                                    High </label>
                            </div></td>
                    </tr>
                    <tr>
                        <td>Est.Sales:</td>
                        <td><input type="text" name="min_estsales" id="min_estsales" class="form-control"></td>
                        <td><input type="text" name="max_estsales" id="max_estsales" class="form-control"></td>
                        <td><div class="checkbox">
                                <label>
                                    <input name="estsales_default" id="estsales_default" type="checkbox">
                                    High </label>
                            </div></td>
                    </tr>
                    <tr>
                        <td>Est.Rev:</td>
                        <td><input type="text" name="min_estrev" id="min_estrev" class="form-control"></td>
                        <td><input type="text" name="max_estrev" id="max_estrev" class="form-control"></td>
                        <td><div class="checkbox">
                                <label>
                                    <input name="estrev_default" id="estrev_default" type="checkbox">
                                    Any </label>
                            </div></td>
                    </tr>
                    <tr>
                        <td>Rating:</td>
                        <td><input type="text" name="min_rating" id="min_rating" class="form-control"></td>
                        <td><input type="text" name="max_rating" id="max_rating" class="form-control"></td>
                        <td><div class="checkbox">
                                <label>
                                    <input name="rating_default" id="rating_default" type="checkbox">
                                    Any </label>
                            </div></td>
                    </tr>

                </tbody></table>
            <br>
            <div class="row">
                <div class="col-md-12 text-right">
                    <input class="btn btn-primary" type="button" id="submit" value="Submit">
                </div>
            </div>
        </div>

    </div>
    <hr>

    <div class="row">

        <div class="col-md-12">

            <section class="scrollable padder">

                <table id="example1" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Prodcut Name</th>
                            <!--<th>Category</th>-->
                            <th>Price</th>
                            <th>Rank</th>
                            <th>Est. Sale</th>
                            <th>Est. Revenue</th>
                            <th>Reviews</th>
                            <th>Rating</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Prodcut Name</th>
                            <!--<th>Category</th>-->
                            <th>Price</th>
                            <th>Rank</th>
                            <th>Est. Sale</th>
                            <th>Est. Revenue</th>
                            <th>Reviews</th>
                            <th>Rating</th>
                        </tr>
                    </tfoot>
                </table>

            </section>
        </div>

    </div>

</form>


<script>

    $(document).ready(function () {

        var Qstring = '';
        marketplace = $("#marketplace").val();
        defaultvar = '';
        Qstring = "marketplace=" + marketplace + "&top_seller=on&defaultvar=&rank_default=on&flag=true";

        $("#submit").click(function () {
            urlString = getRecords(true);
            oTable.ajax.url(urlString).load();
        })
        $("#example1 th").on("click", function () {
             urlString = getRecords(false);
            oTable.ajax.url(urlString).load();
        });

        var oTable = $('#example1').DataTable({
            "processing": true,
            "serverSide": true,
            ajax: {
                url: "<?php echo url('opportunityfinder/opportunity_ajax?'); ?>" + Qstring,
            }
        });

    });

    function getRecords(flag) {
        catstr = '';
        $('input[name="category[]"]:checked:enabled').each(function () {
            catstr += $(this).val() + '_';
        });
        var strLen = catstr.length;
        catstr = catstr.slice(0, strLen - 1);

        var obj = {
            marketplace: $("#marketplace").val(),
            defaultvar: ($("#default").is(":checked")) ? 'on' : '',
            top_seller: ($("#top_seller").is(":checked")) ? 'on' : '',
            price_default: ($("#default").is(":checked")) ? '' : (($("#price_default").is(":checked")) ? 'on' : ''),
            rank_default: ($("#default").is(":checked")) ? '' : (($("#rank_default").is(":checked")) ? 'on' : ''),
            estsales_default: ($("#default").is(":checked")) ? '' : (($("#estsales_default").is(":checked")) ? 'on' : ''),
            estrev_default: ($("#default").is(":checked")) ? '' : (($("#estrev_default").is(":checked")) ? 'on' : ''),
            rating_default: ($("#default").is(":checked")) ? '' : (($("#rating_default").is(":checked")) ? 'on' : ''),
            keywords: $("#keywords").val(),
            min_price: ($("#min_price").val() != '') ? $("#min_price").val() : '',
            max_price: ($("#max_price").val() != '') ? $("#max_price").val() : '',
            min_rank: ($("#min_rank").val() != '') ? $("#min_rank").val() : '',
            max_rank: ($("#max_rank").val() != '') ? $("#max_rank").val() : '',
            min_estsales: ($("#min_estsales").val() != '') ? $("#min_estsales").val() : '',
            max_estsales: ($("#max_estsales").val() != '') ? $("#max_estsales").val() : '',
            min_estrev: ($("#min_estrev").val() != '') ? $("#min_estrev").val() : '',
            max_estrev: ($("#max_estrev").val() != '') ? $("#max_estrev").val() : '',
            min_weight: ($("#min_weight").val() != '') ? $("#min_weight").val() : '',
            max_weight: ($("#max_weight").val() != '') ? $("#max_weight").val() : '',
            category: catstr,
            flag: flag
        }

        Qstring = $.param(obj);
        urlString = "<?php echo url('opportunityfinder/opportunity_ajax?'); ?>" + Qstring;
        return urlString;


    }

    $(function () {

//        $(".cat_chkboxes input[type=checkbox]").change(function () {
//            
////            $("#keywords").val('');
//            var index = $(".cat_chkboxes input[type=checkbox]").index(this);
//            
//            var nodeid = ($(".cat_chkboxes input[type=checkbox]:eq(" + index + ")").val());
//            if (nodeid == 0) { // all departments category
//                //alert(nodeid);
//                if ($(this).is(":checked")) {
//                    //$('.cat_chkboxes input[type="checkbox"]').removeAttr('checked');
//                    //$(".cat_chkboxes input[type=checkbox]:eq(1)").prop('checked', 'checked');
//                    $(this).prop('checked', 'checked');
//                } else {
//                    $(".cat_chkboxes input[type=checkbox]:eq(1)").removeAttr('checked');
//                }
//            } else {
//                $(".cat_chkboxes input[type=checkbox]:eq(2)").removeAttr('checked');
//                //$(".cat_chkboxes input[type=checkbox]:eq(2)").removeAttr('checked');
//
//            }
//
//        })

//        $("#keywords").keyup(function () {
//            if ($(this).val().length > 0) {
//                $(".cat_chkboxes input[type=checkbox]").removeAttr('checked');
//            }
//        })

//        $('#top_seller').change(function () {
//            if ($(this).is(":checked")) {
////                $("#keywords").val('');
//                $('.cat_chkboxes input[type="checkbox"]').removeAttr('checked');
//                $(".cat_chkboxes input[type=checkbox]:eq(1)").prop('checked', 'checked');
//                $(".cat_chkboxes input[type=checkbox]:eq(2)").prop('checked', 'checked');
//            } else {
//                $(".cat_chkboxes input[type=checkbox]").removeAttr('checked');
//            }
//        });

        $('#default').change(function () {
            if ($(this).is(":checked")) {
                $(".default-wrap input[type=text]").val('').attr('disabled', 'disabled');
                $(".default-wrap input[type=checkbox]").prop('checked', 'checked').attr('disabled', 'disabled');
            } else {
                $(".default-wrap input[type=text]").val('').removeAttr('disabled');
                $(".default-wrap input[type=checkbox]").removeAttr('checked').removeAttr('disabled');
            }
        });

        $('#price_default').change(function () {
            if ($(this).is(":checked")) {
                $("#min_price, #max_price").val('').attr('disabled', 'disabled');
            } else {
                $("#min_price, #max_price").val('').removeAttr('disabled');
            }
        });

        $("#min_rank, #max_rank").val('').attr('disabled', 'disabled');
        $('#rank_default').change(function () {
            if ($(this).is(":checked")) {
                $("#min_rank, #max_rank").val('').attr('disabled', 'disabled');
            } else {
                $("#min_rank, #max_rank").val('').removeAttr('disabled');
            }
        });
        $('#estsales_default').change(function () {
            if ($(this).is(":checked")) {
                $("#min_estsales, #max_estsales").val('').attr('disabled', 'disabled');
            } else {
                $("#min_estsales, #max_estsales").val('').removeAttr('disabled');
            }
        });
        $('#estrev_default').change(function () {
            if ($(this).is(":checked")) {
                $("#min_estrev, #max_estrev").val('').attr('disabled', 'disabled');
            } else {
                $("#min_estrev, #max_estrev").val('').removeAttr('disabled');
            }
        });
        $('#rating_default').change(function () {
            if ($(this).is(":checked")) {
                $("#min_rating, #max_rating").val('').attr('disabled', 'disabled');
            } else {
                $("#min_rating, #max_rating").val('').removeAttr('disabled');
            }
        });

        $('#marketplace').val(1);
        $('.cls-2').hide();
        $('.cls-1').show();

        $('#marketplace').change(function () {

            if ($(this).val() == 1) {
                $('.cls-2').hide();
                $('.cls-1').show();
            } else if ($(this).val() == 2) {
                $('.cls-2').show();
                $('.cls-1').hide();
            }
        });


    })


</script>

@stop