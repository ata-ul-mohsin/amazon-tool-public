<?php

    $region	   		  =	(old('region'))?old('region'):(isset($user_setting['region'])?$user_setting['region']:'');
    $account_name	  =	(old('account_name'))?old('account_name'):(isset($user_setting['account_name'])?$user_setting['account_name']:'');
    $access_key_id	  =	(old('access_key_id'))?old('access_key_id'):(isset($user_setting['access_key_id'])?$user_setting['access_key_id']:'');
    $secret_key	      =	(old('secret_key'))?old('secret_key'):(isset($user_setting['secret_key'])?$user_setting['secret_key']:'');
    $associate_tag	  =	(old('associate_tag'))?old('associate_tag'):(isset($user_setting['associate_tag'])?$user_setting['associate_tag']:'');
    $user_setting_id  =	(old('user_setting_id'))?old('user_setting_id'):(isset($user_setting['id'])?$user_setting['id']:'');

?>
@extends('main_template')
@section ("title")
    AWS Setting Definition
@stop
@section('content')
    <div class="col-sm-12">
        <form action="{!! url('user/save-product-advertising-setting') !!}" method="post" class="bs-example form-horizontal" enctype="multipart/form-data" id="user_setting__form"  data-parsley-validate ="" >
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Region</label>
                                <div class="col-lg-4">
                                    <select class="form-control chosen_select" name="region" id="region" data-parsley-trigger="change"   required="required" >
                                        <option selected="selected" value="">~Select~</option>
                                            @if(isset($regions))
                                                @foreach($regions as $region_current)
                                                    <option  <?php if(!empty($region) && ($region_current['id'] == $region)) echo "selected"; ?> value="<?php echo $region_current['id']; ?>"><?php echo $region_current['name']; ?></option>
                                                @endforeach
                                            @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Account Name</label>
                                <div class="col-lg-4">
                                    <input type="text" placeholder="Enter Account Name" class="form-control" name="account_name" id="account_name" value="<?php echo $account_name; ?>" data-parsley-trigger="keyup"   required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Access Key Id</label>
                                <div class="col-lg-4">
                                    <input type="text" placeholder="Enter Access Key Id" class="form-control" name="access_key_id" id="access_key_id" value="<?php echo $access_key_id; ?>"  data-parsley-trigger="keyup"   required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Secret Key</label>
                                <div class="col-lg-4">
                                    <input type="text" placeholder="Enter Secret Key" class="form-control" name="secret_key" id="secret_key" value="<?php echo $secret_key; ?>"  required="required" data-parsley-trigger="keyup">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Associate Tag</label>
                                <div class="col-lg-4">
                                    <input type="text" placeholder="Enter Associate Tag" class="form-control" name="associate_tag" id="associate_tag" value="<?php echo $associate_tag; ?>"  required="required" data-parsley-trigger="keyup">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label"></label>
                                <div class="col-lg-4">
                                    <input type="hidden" id="user_setting_id" name="user_setting_id" value="<?php echo $user_setting_id; ?>" />
                                    <input class="btn btn-sm btn-success" type="submit" >
                                </div>
                            </div>
                        </form>
    </div>
@stop