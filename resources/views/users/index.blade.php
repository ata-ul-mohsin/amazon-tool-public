@extends('main_template')

@section('title')
Dashboard
@stop
@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Order and Feedback</div>
                <div class="panel-body"><div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Process</th>
                                <th>7 Days</th>
                                <th>30 Days</th>
                                <th>90 Days</th>
                                <th>1 Year</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                            <td>Orders</td>
                                <td>{{ $data['seven_days_orders'] }}</td>
                                <td>{{ $data['one_month_orders'] }}</td>
                                <td>{{ $data['three_month_orders'] }}</td>
                                <td>{{ $data['full_year_orders'] }}</td>
                            </tr>
                            <tr>
                                <td>Total Feedbacks</td>
                                <td>{{ $data['seven_days_feedbacks'] }}</td>
                                <td>{{ $data['one_month_feedbacks'] }}</td>
                                <td>{{ $data['three_month_feedbacks'] }}</td>
                                <td>{{ $data['full_year_feedbacks'] }}</td>
                            </tr>
                            <tr>
                                <td>PositiveFeedbacks</td>
                                <td>{{ $data['seven_days_positive_feedbacks'] }}</td>
                                <td>{{ $data['one_month_positive_feedbacks'] }}</td>
                                <td>{{ $data['three_month_positive_feedbacks'] }}</td>
                                <td>{{ $data['full_year_positive_feedbacks'] }}</td>
                            </tr>
                            <tr>
                                <td>Negative Feedbacks</td>
                                <td>{{ $data['seven_days_negative_feedbacks'] }}</td>
                                <td>{{ $data['one_month_negative_feedbacks'] }}</td>
                                <td>{{ $data['three_month_negative_feedbacks'] }}</td>
                                <td>{{ $data['full_year_negative_feedbacks'] }}</td>
                            </tr>
                            <tr >
                                <td rowspan="2">&nbsp;</td>
                                <td rowspan="2">&nbsp;</td>
                                <td rowspan="2">&nbsp;</td>
                                <td rowspan="2">&nbsp;</td>
                                <td rowspan="2">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </div></div>
            </div>
        </div>
        <div class="col-md-6">
           <div class="panel panel-default">
                <div class="panel-heading">Summary of Reviews &amp; Ratings</div>
                <div class="panel-body" ><div class="table-responsive">
                        <table class="table" id="server_side_rating_dtable">
                            <thead>
                            <tr>
                                <th>Sr</th>
                                <th>Brand</th>
                                <th>Product</th>
                                <th>Reviews</th>
                                <th>Ratings</th>
                            </tr>
                            </thead>
                            <tbody>
                           @if(!empty($data['products_ratings']))
                                @foreach($data['products_ratings'] as $product)
                                <tr <?php if($product[3] <=3){ ?>  style="background: #ffcce6" <?php } ?> >
                                    <td>{{ $product[0] }}</td>
                                    <td>{{ $product[1] }}</td>
                                    <td>{{ $product[2] }}</td>
                                    <td>{{ $product[4] }}</td>
                                    <td><span >{{ $product[3] }}</span></td>
                                </tr>
                            @endforeach
                                <tr>
                                    <td colspan="5" align="center"><a href="{{ url('products') }}" ><b>More Products</b></a> </td>

                                </tr>
                            @else
                               <tr>
                                   <td colspan="5" align="center"><a href="{{ url('products') }}" ><b>No More Records</b></a> </td>

                               </tr>
                            @endif

                            </tbody>

                        </table>
                    </div></div>
            </div>
       </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Recent Sent Messages</div>
                <div class="panel-body"><div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>Sr</th>
                                <th>Message</th>
                                <th>Order</th>
                            </tr>
                            @if(!empty($sent_messages))
                            @foreach($sent_messages as $message)
                                <tr>
                                    <td>{{ $count++    }}</td>
                                    <td>{{ $message['content']  }}</td>
                                    <td>{{ $message['order']['order_id'] }}</td>
                                </tr>
                            @endforeach
                            @else
                                <tr>
                                    <td colspan="3" align="center"><b>No Message Sent</b></a> </td>

                                </tr>
                            @endif


                        </table>
                    </div></div>
            </div>
        </div>
    </div>
@stop
@section('additional_js')
<script>

</script>
@stop
