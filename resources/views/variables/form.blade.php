<?php


$name                    =       Input::old('name');
$value                   =       Input::old('value');
$description             =       Input::old('description');
?>
@extends('main_template')
@section ("title")
    Message Variable Definition
@stop
@section("content")
    @if(isset($variable))
        {!! Form::model($variable, ['route' => ['variables.update', $variable->id], 'method' => 'patch', 'data-parsley-validate' => '', 'enctype' => "multipart/form-data" ]) !!}
    @else
        {!! Form::open(['route' => 'variables.store', 'data-parsley-validate' => " ", 'enctype' => "multipart/form-data" ]) !!}
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="form-horizontal">
          <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Variable Name</label>
                    <div class="col-sm-4">
 {!! Form::text('name', $name, ['class' => 'form-control','data-parsley-trigger' => 'change','required' => '']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Variable Value:</label>
                    <div class="col-sm-4">
   {!! Form::text('value', $value, ['class' => 'form-control','data-parsley-trigger' => 'change','required' => '']) !!}

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Variable Description:</label>
                    <div class="col-sm-4">
 {!! Form::textarea('description', $description, [ 'rows' => '5','class' => 'form-control','data-parsley-trigger' => 'change','required' => '']) !!}

                    </div>
                </div>

            </div>
            </div>
        </div>
    <div class="row">
        <div class="col-md-12 text-center">
            {!! Form::submit('Save', ['name' => 'save','class' => 'btn btn-primary']) !!}
        </div>
    </div>
        {!! Form::close() !!}
@stop