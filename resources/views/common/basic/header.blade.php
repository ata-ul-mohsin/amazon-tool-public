<nav class="navbar navbar-default">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#topMenu" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" id="menu-toggle" href="#"><i class="fa fa-caret-square-o-right"></i></a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="topMenu">
			{{--<ul class="nav navbar-nav left-nav">
				<li class="active"><a href="#">Settings</a></li>
				<li><a href="#">Distributor Management</a></li>
				<li><a href="#">Henkel Management</a></li>
				<li><a href="#">Reports</a></li>

			</ul>--}}

			<ul class="nav navbar-nav navbar-right">
				<li><a href="#"><i class="fa fa-user"></i> Welcome to User</a></li>
				<li><a href="#"><i class="fa  fa-sign-out"></i> Logout</a></li>

			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>