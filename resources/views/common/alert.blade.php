@if(Session::has("success_message"))
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-success">  {!!  Session::get("success_message") !!} </div>
    </div>
</div>
@endif
@if(Session::has("error_message"))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger">  {!!  Session::get("error_message") !!} </div>
        </div>
    </div>
@endif
@if (count($errors) > 0)
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endif