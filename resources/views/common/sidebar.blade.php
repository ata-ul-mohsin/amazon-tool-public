<?php







$dashboard_routes	= array("/","/user/dashboard","/user/rating-data");



$oppertunity_routes	=

        array(

                "/opportunityfinder",

                "/opportunityfinder/find_opportunity",

                "/opportunityfinder/get_review_rating",

                "/opportunityfinder/make_asin",

                "/opportunityfinder/opportunity_ajax",

                "/opportunityfinder/opportunity_cron",

                "/opportunityfinder/populate_general_products",

                "/opportunityfinder/populate_products",

                "/opportunityfinder/saved-search",

                "/opportunityfinder/set_price",

                "/opportunityfinder/get_review_rating"

        );

$setting_routes	=

        array( "/user/add-aws-setting",

                "/user/add-marketplace-setting",

                "/user/aws-settings",

                "/user/choose-setting",

                "/user/delete-marketplace-setting",

                "/user/delete-product-advertising-setting",

                "/user/edit-aws-setting",

                "/user/edit-marketplace-setting",

                "/user/marketplace-settings",

                "/user/save-marketplace-setting",

                "/user/save-product-advertising-setting");



$messaging_routes	=

        array("/blacklist-emails",
                "/blacklist-emails/create",
                "/messages",
                "/messages/create",
                "/messages/settings",
                "/messages/sent",
                "/messages/outbox",
                "/variables",
                "/variables/create",
                "/track-product",
                "/assign-brand",
                "/products",
                "/brand-data",
                "/brands",
                "/brands/create",
                "/feedback-detail",
                "/feedbacks",
                "/feedbacks-data",
                "/product-data",
                "/orders",
                "/save-brand-assignment");

/*
 * Messages
Setting
Sent
Outbox

Orders

Brand===Add brand

Products
Assign===Add product

Feedback

 */
?>
<div id="sidebar-wrapper">
    <div class="logo"><h2><a href="<?php echo url('/'); ?>">Amazon Tool </a> </h2></div>
    <?php
    if(in_array($current_route_name,$messaging_routes))

    {

    ?>
    <h4>Menu</h4>
    <div class="sidebar-menu">
        <ul class="nav nav-stacked" id="accordion1">


            <li class="<?php if (strcmp($current_route_name,'/messages')==0) { echo 'active'; } ?>"><a href="<?php echo url('messages'); ?>">Messages</a></li>


            <li class="<?php if (strcmp($current_route_name,'/orders')==0) { echo 'active'; } ?>"><a href="<?php echo url('orders'); ?>">Orders</a></li>
            <li class="<?php if (strcmp($current_route_name,'/brands')==0) { echo 'active'; } ?>"><a href="<?php echo url('brands'); ?>">Brands</a></li>
            <li class="<?php if (strcmp($current_route_name,'/products')==0) { echo 'active'; } ?>"><a href="<?php echo url('products'); ?>">Products</a></li>

          <?php
          /*
            <li class="panel <?php if (strcmp($current_route_name,'/products')==0) { echo 'active'; } ?>"> <a data-toggle="collapse" data-parent="#accordion1" href="#secondLink">Products</a>
                <ul id="secondLink" class="collapse">
                    <li><a href="<?php echo url('products'); ?>">Products List</a></li>
                    <li><a href="<?php echo url('assign-brand'); ?>">Assign Brand To Products</a></li>
                </ul>
            </li>
            */
         ?>
            <li class="<?php if (strcmp($current_route_name,'/feedbacks')==0) { echo 'active'; } ?>"><a href="<?php echo url('feedbacks'); ?>">Feedbacks</a></li>

        </ul>

    </div>
    <?php

    }
    else if(in_array($current_route_name,$oppertunity_routes))

    {

    ?>
    <h4>Menu</h4>
    <div class="sidebar-menu">
        <ul class="nav nav-stacked" id="accordion1">
            <li class="<?php if (strcmp($current_route_name,'/opportunityfinder')==0) { echo 'active'; } ?>"><a href="<?php echo url('opportunityfinder'); ?>">Opportunity Finder</a> </li>
        <div></div>
        </ul>

    </div>
    <?php
    }
    else if(in_array($current_route_name,$setting_routes))
    {

    ?>
        <h4>Menu</h4>
        <div class="sidebar-menu">
            <ul class="nav nav-stacked" id="accordion1">
                <li class="<?php if (strcmp($current_route_name,'/user/add-marketplace-setting')==0) { echo 'active'; } ?>"><a href="<?php echo url('user/add-marketplace-setting'); ?>">Add Market Place Setting</a></li>
                <li class="<?php if (strcmp($current_route_name,'/user/marketplace-settings')==0) { echo 'active'; } ?>"><a href="<?php echo url('user/marketplace-settings'); ?>">Market Place Settings</a></li>
                <li class="<?php if (strcmp($current_route_name,'/user/add-aws-setting')==0) { echo 'active'; } ?>"><a href="<?php echo url('user/add-aws-setting'); ?>">Add AWS Setting</a></li>
                <li class="<?php if (strcmp($current_route_name,'/user/aws-settings')==0) { echo 'active'; } ?>"><a href="<?php echo url('user/aws-settings'); ?>">AWS Settings</a></li>
            </ul>

        </div>
    <?php
    }
    ?>
</div>
