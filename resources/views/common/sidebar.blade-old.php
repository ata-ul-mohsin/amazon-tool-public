<?php







$dashboard_routes	= array("/","/user/dashboard","/user/rating-data");



$oppertunity_routes	=

        array(

                "/opportunityfinder",

                "/opportunityfinder/find_opportunity",

                "/opportunityfinder/get_review_rating",

                "/opportunityfinder/make_asin",

                "/opportunityfinder/opportunity_ajax",

                "/opportunityfinder/opportunity_cron",

                "/opportunityfinder/populate_general_products",

                "/opportunityfinder/populate_products",

                "/opportunityfinder/saved-search",

                "/opportunityfinder/set_price",

                "/opportunityfinder/get_review_rating"

        );

$setting_routes	=

        array( "/user/add-aws-setting",

                "/user/add-marketplace-setting",

                "/user/aws-settings",

                "/user/choose-setting",

                "/user/delete-marketplace-setting",

                "/user/delete-product-advertising-setting",

                "/user/edit-aws-setting",

                "/user/edit-marketplace-setting",

                "/user/marketplace-settings",

                "/user/save-marketplace-setting",

                "/user/save-product-advertising-setting");



$messaging_routes	=

        array("/blacklist-emails",

                "/blacklist-emails/create",

                "/brand-data",

                "/brands",

                "/brands/create",

                "/feedback-detail",

                "/feedbacks",

                "/feedbacks-data",

                "/messages",

                "/messages/create",

                "/messages/settings",

                "/product-data",

                "/save-brand-assignment",

                "/variables",

                "/variables/create");
$product_routes	=	array("/track-product","/assign-brand","/products");
?>



<div id="sidebar-wrapper">
    <div class="logo"><h2><a href="#">Amazon Tool </a> </h2></div>

    <div id="cssmenu">

            <?php
            if(in_array($current_route_name,$messaging_routes))

            {

            ?>
                <h4>Menu</h4>
                <ul>
            <li class="<?php if (strcmp($current_route_name,'/messages')==0) { echo 'active'; } ?>"> <a href="<?php echo url('messages'); ?>"><span>Messages</span></a> </li>

            <li class="<?php if (strcmp($current_route_name,'/brands')==0) { echo 'active'; } ?>"> <a href="<?php echo url('brands'); ?>"><span>Brands</span></a> </li>

            <li class="<?php if (strcmp($current_route_name,'/messages/settings')==0) { echo 'active'; } ?>"> <a href="<?php echo url('messages/settings'); ?>"><span>Message Settings</span></a> </li>

            <li class="<?php if (strcmp($current_route_name,'/feedbacks')==0) { echo 'active'; } ?>"> <a href="<?php echo url('feedbacks'); ?>"><span>Feedbacks</span></a> </li>


                    </ul>

<?php

}
                else if(in_array($current_route_name,$oppertunity_routes))

                {

                ?>

                <h4>Menu</h4>
                <ul>
                <li class="last <?php if (strcmp($current_route_name,'/opportunityfinder')==0) { echo 'active'; } ?>"> <a href="<?php echo url('opportunityfinder'); ?>">Opportunity Finder</a> </li>

                </ul>

                <?php

                }
                else if(in_array($current_route_name,$setting_routes))

                {

                ?>

                <h4>Menu</h4>
                <ul>
                <li class="<?php if (strcmp($current_route_name,'/user/add-marketplace-setting')==0) { echo 'active'; } ?>"> <a href="<?php echo url('user/add-marketplace-setting'); ?>"><span>Add Market Place Setting</span></a> </li>

                <li class="<?php if (strcmp($current_route_name,'/user/marketplace-settings')==0) { echo 'active'; } ?>"> <a href="<?php echo url('user/marketplace-settings'); ?>"><span>Market Place Settings</span></a> </li>

                <li class="<?php if (strcmp($current_route_name,'/user/add-aws-setting')==0) { echo 'active'; } ?>"> <a href="<?php echo url('user/add-aws-setting'); ?>"><span>Add AWS Setting</span></a> </li>

                <li class="last <?php if (strcmp($current_route_name,'/user/aws-settings')==0) { echo 'active'; } ?>"> <a href="<?php echo url('user/aws-settings'); ?>"><span>AWS Settings</span></a> </li>
                </ul>
                <?php

                }

                else if(in_array($current_route_name,$product_routes))

                {

                ?>

                <h4>Menu</h4>
                <ul>
                    <li class="has-sub<?php if (in_array($current_route_name,$product_routes)) { echo 'active'; } ?>"><a href="#"><span>Products</span></a>
                        <ul style="<?php if (in_array($current_route_name,$product_routes)) { echo 'display: block;'; }else 'display: none;'; ?>"  >
                            <li class="even <?php if (strcmp($current_route_name,'/products')==0) { echo 'active'; } ?>"><a href="<?php echo url('products'); ?>"><span>Products List</span></a></li>
                            <li class="odd <?php if (strcmp($current_route_name,'/assign-brand')==0) { echo 'active'; } ?>"><a href="<?php echo url('assign-brand'); ?>"><span>Assign Brand To Products</span></a></li>
                            <li class="even <?php if (strcmp($current_route_name,'/track-product')==0) { echo 'active'; } ?>"> <a href="<?php echo url('track-product'); ?>"><span>Add Product</span></a></li>
                        </ul>
                    </li>
                </ul>
                <?php

                }
?>



    </div>


</div>
