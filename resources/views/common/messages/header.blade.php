<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="topMenu">
            <ul class="nav navbar-nav left-nav">
                <li class="<?php if(strcmp($temp[2],'')==0)echo "active"; ?>"><a href="{!! url('messages'); !!}">Messages</a></li>
                <li class="<?php if(strcmp($temp[2],'sent')==0)echo "active"; ?>"><a href="{!! url('/') !!}">Sent</a></li>
                <li class="<?php if(strcmp($temp[2],'settings')==0)echo "active"; ?>"><a href="{!! url('settings') !!}">Settings</a></li>
                <?php
                    $user=Session::get('user');

                    if(!empty($user['is_admin']))
                    {
                ?>
                        <li class="<?php if(strcmp($temp[2],'variables')==0)echo "active"; ?>"><a href="{!! url('variables') !!}">Variables</a></li>
                <?php
                    }

                ?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div class="row">
    <div class="col-md-12">
        @if(Session::has("message"))
            {!! CommonHelper::alertMessage("success",Session::get("message")) !!}
        @endif
    </div>
</div>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif