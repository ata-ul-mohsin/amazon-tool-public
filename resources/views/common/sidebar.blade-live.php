<div id="sidebar-wrapper">
	<div class="logo"><h2><a href="<?php echo url('/'); ?>">Amazon Tool </a> </h2></div>
	<h4>Menu</h4>
	<ul class="sidebar-nav">
            
		<li class="<?php if ($current_route_name=='/messages') { echo 'active'; } ?>"> <a href="<?php echo url('messages'); ?>">Messages</a> </li>
		<li class="<?php if ($current_route_name=='/user/add-marketplace-setting') { echo 'active'; } ?>"> <a href="<?php echo url('user/add-marketplace-setting'); ?>">Add Market Place Setting</a> </li>
		<li class="<?php if ($current_route_name=='/user/marketplace-settings') { echo 'active'; } ?>"> <a href="<?php echo url('user/marketplace-settings'); ?>">Market Place Settings</a> </li>
		<li class="<?php if ($current_route_name=='/user/add-aws-setting') { echo 'active'; } ?>"> <a href="<?php echo url('user/add-aws-setting'); ?>">Add AWS Setting</a> </li>
		<li class="<?php if ($current_route_name=='/user/aws-settings') { echo 'active'; } ?>"> <a href="<?php echo url('user/aws-settings'); ?>">AWS Settings</a> </li>
		<li class="<?php if ($current_route_name=='/brands') { echo 'active'; } ?>"> <a href="<?php echo url('brands'); ?>">Brands</a> </li>
		<li class="<?php if ($current_route_name=='/brands/create') { echo 'active'; } ?>"> <a href="<?php echo url('brands/create'); ?>">Add Brand</a> </li>
		<li class="<?php if ($current_route_name=='/messages/settings') { echo 'active'; } ?>"> <a href="<?php echo url('messages/settings'); ?>">Message Settings</a> </li>
		<li class="<?php if ($current_route_name=='/feedbacks') { echo 'active'; } ?>"> <a href="<?php echo url('feedbacks'); ?>">Feedbacks</a> </li>
		<li class="<?php if ($current_route_name=='/assign-brand') { echo 'active'; } ?>"> <a href="<?php echo url('assign-brand'); ?>">Assign Brand To Products</a> </li>
		<li class="<?php if ($current_route_name=='/track-product') { echo 'active'; } ?>"> <a href="<?php echo url('track-product'); ?>">Track Product</a> </li>
                
                <div></div>
	</ul>
</div>
<?php //if(strcmp($module,"messages")==0) echo 'active';

//<?php if ($current_route_name=='/amazontool/messages') { echo 'active'; }

?>