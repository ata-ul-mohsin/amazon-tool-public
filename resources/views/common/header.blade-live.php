<?php







$dashboard_routes	= array("/","/user/dashboard","/user/rating-data");



$oppertunity_routes	=

	array(

		"/opportunityfinder",

		"/opportunityfinder/find_opportunity",

		"/opportunityfinder/get_review_rating",

		"/opportunityfinder/make_asin",

		"/opportunityfinder/opportunity_ajax",

		"/opportunityfinder/opportunity_cron",

		"/opportunityfinder/populate_general_products",

		"/opportunityfinder/populate_products",

		"/opportunityfinder/saved-search",

		"/opportunityfinder/set_price",

		"/opportunityfinder/get_review_rating"

	);

$setting_routes	=

	array( "/user/add-aws-setting",

		"/user/add-marketplace-setting",

		"/user/aws-settings",

		"/user/choose-setting",

		"/user/delete-marketplace-setting",

		"/user/delete-product-advertising-setting",

		"/user/edit-aws-setting",

		"/user/edit-marketplace-setting",

		"/user/marketplace-settings",

		"/user/save-marketplace-setting",

		"/user/save-product-advertising-setting");



$messaging_routes	=

	array("/blacklist-emails",
		"/blacklist-emails/create",
		"/messages",
		"/messages/create",
		"/messages/settings",
		"/messages/sent",
		"/messages/outbox",
		"/variables",
		"/variables/create",
		"/track-product",
		"/assign-brand",
		"/products",
		"/brand-data",
		"/brands",
		"/brands/create",
		"/feedback-detail",
		"/feedbacks",
		"/feedbacks-data",
		"/product-data",
		"/orders",
		"/save-brand-assignment");

/*
 * Messages
Setting
Sent
Outbox

Orders

Brand===Add brand

Products
Assign===Add product

Feedback

 */
?>

<script src="<?php echo  asset('assets/js/jquery.js'); ?>"></script>
<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#topMenu" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" id="menu-toggle" href="#"><i class="fa fa-caret-square-o-right"></i></a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="topMenu">
			<ul class="nav navbar-nav left-nav">
				<li class="<?php if(in_array($current_route_name,$dashboard_routes)) { echo 'active'; } ?>"><a href="<?php echo url('/'); ?>">Dashboard</a></li>
				<li class="<?php if(in_array($current_route_name,$messaging_routes)) { echo 'active'; } ?>"><a href="<?php echo url('messages'); ?>">Email Management</a></li>
				<li class="<?php if(in_array($current_route_name,$oppertunity_routes)){ echo 'active'; } ?>"><a href="<?php echo url('opportunityfinder'); ?>">Opportunity Finder </a></li>
				<li class="<?php if(in_array($current_route_name,$setting_routes)) { echo 'active'; } ?>"><a href="<?php echo url('user/marketplace-settings'); ?>">Settings </a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="<?php echo url("/"); ?>"><i class="fa fa-user"></i> Welcome to User</a></li>
				<li><a href="<?php echo url("user/logout"); ?>"><i class="fa  fa-sign-out"></i> Logout</a></li>

			</ul>
		</div>
	</div>
</nav>