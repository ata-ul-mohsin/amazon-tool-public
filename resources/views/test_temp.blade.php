@extends('main_template')
@section ("title")
    Opportunity Finder
@stop
@section("content")

    <style>
        .dataTables_filter { visibility: hidden;}
        .numericCol{text-align: right;}
    </style>

    <form action="<?php echo url('opportunityfinder/find_opportunity') ?>"  method="post" id="publisher_form" data-validate="parsley" >

        {!! csrf_field() !!}


        <div id="strip-collaps" style="text-align: left;display: none;cursor: pointer;background-image:linear-gradient(to bottom, #f5f5f5 0px, #e8e8e8 100%);padding: 4px;border-radius: 10px;">
            <img src="assets/images/downarrow.png" width="40" > Click to expand filters section.
        </div>
        <div class="row" id="frm-wrap">
            <div class="col-md-4">
                <div class="form-horizontal">

                    <div class="col-sm-12">
                        <div class="form-group">
                            <input type="text" name="keywords" id="keywords" data-toggle="tooltip" title="Keyword Search" placeholder="Keyword Search..." class="form-control">

                        </div>

                        <!--                    <div class="form-group">
                                                <div class="checkbox"> <label> <input type="checkbox" name="top_seller" checked="" id="top_seller" />
                                                        Top Seller </label>
                                                </div>
                                            </div>-->
                        <div class="form-group" style="height: 265px; overflow-y: scroll;">

                            <table class="table" >

                                <tr>
                                    <th>Category</th>
                                    <th>Action</th>
                                </tr>

                                <tr>
                                    <td>All Categories</td>
                                    <td>
                                        <label><input id="allCats" type="checkbox" name="category[]" value="0"></label>
                                    </td>
                                </tr>
                                <?php
                                if (!empty($categories)) {
                                foreach ($categories as $category) {
                                if ($category['marketplace'] == 'webservices.amazon.com') {
                                    $marketplace = 1;
                                } elseif ($category['marketplace'] == 'webservices.amazon.co.uk') {
                                    $marketplace = 2;
                                }
                                ?>

                                <tr class="cls-<?php echo $marketplace; ?>" style="<?php echo ($category['marketplace'] != 'webservices.amazon.com') ? 'display:none;' : ''; ?>">
                                    <td><?php echo $category['name']; ?></td>
                                    <td>
                                        <input class="cats" type="checkbox" name="category[]" value="<?php echo $category['browse_nodes']; ?>">
                                    </td>
                                </tr>
                                <?php
                                }
                                }
                                ?>

                            </table>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-4 col-md-offset-2">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-inline">
                            <div class="form-group">
                                <label>Market Place</label>
                                <select name="marketplace" id="marketplace">
                                    <option value="1" selected="">US</option>
                                    <option value="2">UK</option>
                                </select>


                            </div>
                            <div class="form-group pull-right">
                                <div class="checkbox">
                                    <!--<label>
                                        <input type="checkbox" name="default" id="default777" />
                                        Default </label> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <table class="table-form  default-wrap default-inputs">
                    <tbody><tr>
                        <th></th>
                        <th>Min</th>
                        <th>Max</th>
                        <th><div class="checkbox">
                                <label>
                                    <input type="checkbox" name="default" id="default" />
                                    <b>All</b></label>
                            </div></th>
                    </tr>
                    <tr>
                        <td>Price:</td>
                        <td><input type="text" name="min_price" id="min_price" class="form-control" ></td>
                        <td><input type="text" name="max_price" id="max_price" class="form-control"></td>
                        <td><div class="checkbox">
                                <label>
                                    <input type="checkbox" name="price_default" id="price_default">
                                    Low </label>
                            </div></td>
                    </tr>
                    <tr>
                        <td>Rank:</td>
                        <td><input type="text" name="min_rank" id="min_rank" class="form-control"></td>
                        <td><input type="text" name="max_rank" id="max_rank" class="form-control"></td>
                        <td><div class="checkbox">
                                <label>
                                    <input name="rank_default" id="rank_default" type="checkbox" checked="">
                                    High </label>
                            </div></td>
                    </tr>
                    <tr>
                        <td>Est.Sales:</td>
                        <td><input type="text" name="min_estsales" id="min_estsales" class="form-control"></td>
                        <td><input type="text" name="max_estsales" id="max_estsales" class="form-control"></td>
                        <td><div class="checkbox">
                                <label>
                                    <input name="estsales_default" id="estsales_default" type="checkbox">
                                    High </label>
                            </div></td>
                    </tr>
                    <tr>
                        <td>Est.Rev:</td>
                        <td><input type="text" name="min_estrev" id="min_estrev" class="form-control"></td>
                        <td><input type="text" name="max_estrev" id="max_estrev" class="form-control"></td>
                        <td><div class="checkbox">
                                <label>
                                    <input name="estrev_default" id="estrev_default" type="checkbox">
                                    Any </label>
                            </div></td>
                    </tr>
                    <tr>
                        <td>Rating:</td>
                        <td><input type="text" name="min_rating" id="min_rating" class="form-control"></td>
                        <td><input type="text" name="max_rating" id="max_rating" class="form-control"></td>
                        <td><div class="checkbox">
                                <label>
                                    <input name="rating_default" id="rating_default" type="checkbox">
                                    Any </label>
                            </div></td>
                    </tr>
                    <tr>
                        <td>Reviews:</td>
                        <td><input type="text" name="min_review" id="min_review" class="form-control"></td>
                        <td><input type="text" name="max_review" id="max_review" class="form-control"></td>
                        <td><div class="checkbox">
                                <label>
                                    <input name="review_default" id="review_default" type="checkbox">
                                    Any </label>
                            </div></td>
                    </tr>

                    </tbody></table>
                <div class="row err" style="display:none;">
                    <div class="col-sm-12 err-inner " style="text-align:center; color: red;">
                        dddd
                    </div>
                </div>

                <br>
                <!--            <div class="row">
                                <div class="col-md-12 text-right">
                                    <input class="btn btn-primary" type="button" id="submit" value="Submit">
                                </div>
                            </div>-->
            </div>
            <input type="button" class="btn btn-primary" id="submit" value="Submit">&nbsp;&nbsp;<img id="loading-img" style="display:none;" src="assets/images/spinner.gif" />
        </div>

        <hr>

        <div class="row">

            <div class="col-md-12">

                <section class="scrollable padder">

                    <table id="example1" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Category</th>
                            <th>Price</th>
                            <th>Rank</th>
                            <th>Est. Sale</th>
                            <th>Est. Revenue</th>
                            <th>Reviews</th>
                            <th>Rating</th>
                        </tr>
                        </thead>

                    </table>


                    <div class="pagi"></div>

                </section>
            </div>

        </div>
        <input type="button" class="btn btn-primary" id="save-search" value="Save Search">
    </form>


    <script>

        $(document).ready(function () {

            $('#keywords').tooltip({ /*or use any other selector, class, ID*/
                placement: "right",
                trigger: "focus"
            });

            saveSearchNow('');

            //$("#allCats").prop('checked', 'checked');
            $(".cats[value=2619526011]").prop('checked', 'checked');
            $("#allCats").change(function(){
                if($(this).is(":checked")){
                    $(".cats").prop('disabled', 'disabled').removeAttr('checked');
                }else{
                    $(".cats").removeAttr('disabled');
                }
            })


            var Qstring = '';
            marketplace = $("#marketplace").val();
            defaultvar = '';
            Qstring = "marketplace=" + marketplace + "&defaultvar=&rank_default=on&flag=true&category=2619526011";

            $("#submit").click(function () {

                urlString = getRecords(true);
                oTable.ajax.url(urlString).load();
                $("#loading-img").show();

                $("#strip-collaps").slideDown('slow');
                $("#frm-wrap").slideUp('slow');

            })

            $("#strip-collaps").click(function(){
                $("#strip-collaps").slideUp('slow');
                $("#frm-wrap").slideDown('slow');
            });

            $("#example1 th").on("click", function () {
                urlString = getRecords(false);
                oTable.ajax.url(urlString).load();
                $("#loading-img").show();
            });

            var oTable = $('#example1').DataTable({
                "aoColumnDefs": [{ "sClass": "numericCol", "aTargets": [2,3,4,5,6,7] }],
                "processing": true,
                "serverSide": true,
                "pagingType": "full_numbers",
                "scrollX": true,
                oLanguage: {
                    sProcessing: "<img src='assets/images/spinner.gif'>"
                },
                ajax: {
                    url: "<?php echo url('opportunityfinder/opportunity_ajax?'); ?>" + Qstring,
                }
            });

            $('#example1').on('draw.dt', function () {
                $("#loading-img").hide();
                $('html, body').animate({
                    scrollTop: $("#example1").offset().top
                }, 2000);

                $('[data-toggle="tooltip"]').tooltip();

            });

            $("#save-search").click(function () {
                urlString = getRecords(true);
                title = $("#keywords").val();
                if (title == '') {
                    var strTitle = prompt("Please enter your search title", "");
                    if (strTitle == '') {
                        alert("Search could not be saved without title.");
                    } else {
                        saveSearchNow(strTitle);
                    }
                } else {
                    var strTitle = prompt("Please verify the search title", title);
                    if (strTitle == '') {
                        alert("Search could not be saved without title.");
                    } else {
                        saveSearchNow(strTitle);
                    }
                }

            })

            $("body").on('click', '.saved-search', function (e) {
                e.preventDefault();
                var str = $(this).attr('href');
                var decodedStr = makeBase64(str);

                var obj = (getQueryParams(decodedStr));

                //(obj['top_seller'] == 'on') ? $("#top_seller").attr("checked") : $("#top_seller").removeAttr("checked")
                if (obj['marketplace'] == 1) {
                    $("#marketplace").val(1);
                    $("#marketplace").trigger('change');
                } else {
                    $("#marketplace").val(2);
                    $("#marketplace").trigger('change');
                }



                if (obj['category'] != 0) {
                    $("#allCats").removeAttr('disabled');
                    $(".cats").removeAttr('disabled');
                    $('input[name="category[]"]').removeAttr("checked");
                    var catStr = obj['category'];
                    var catStrAr = catStr.split("_");
                    for (var i = 0; i < catStrAr.length; i++) {
                        chkVal = catStrAr[i];
                        $('input[value="' + chkVal + '"]').prop("checked", true);
                    }
                }else{
                    $("#allCats").prop('checked', 'checked');
                    $(".cats").prop('disabled', 'disabled');
                }
                if (obj['keywords'] != '') {
                    var keywords = obj['keywords'];
                    var keywords = keywords.replace("+", " ");
                    $("#keywords").val(keywords);
                } else {
                    $("#keywords").val('');
                }

                if (obj['defaultvar'] != '') {
                    $('#default').prop("checked", true);
                    $('#default').trigger('change');
                } else {
                    $('#default').removeAttr("checked");
                    $('#default').trigger('change');

                    if (obj['price_default'] != '') {
                        $('#price_default').prop("checked", true);
                        $('#price_default').trigger('change');
                    } else {
                        $('#price_default').removeAttr("checked");
                        $('#price_default').trigger('change');
                    }
                    if (obj['rank_default'] != '') {
                        $('#rank_default').prop("checked", true);
                        $('#rank_default').trigger('change');
                    } else {
                        $('#rank_default').removeAttr("checked");
                        $('#rank_default').trigger('change');
                    }
                    if (obj['estsales_default'] != '') {
                        $('#estsales_default').prop("checked", true);
                        $('#estsales_default').trigger('change');
                    } else {
                        $('#estsales_default').removeAttr("checked");
                        $('#estsales_default').trigger('change');
                    }
                    if (obj['estrev_default'] != '') {
                        $('#estrev_default').prop("checked", true);
                        $('#estrev_default').trigger('change');
                    } else {
                        $('#estrev_default').removeAttr("checked");
                        $('#estrev_default').trigger('change');
                    }
                    if (obj['rating_default'] != '') {
                        $('#rating_default').prop("checked", true);
                        $('#rating_default').trigger('change');
                    } else {
                        $('#rating_default').removeAttr("checked");
                        $('#rating_default').trigger('change');
                    }
                    if (obj['review_default'] != '') {
                        $('#review_default').prop("checked", true);
                        $('#review_default').trigger('change');
                    } else {
                        $('#review_default').removeAttr("checked");
                        $('#review_default').trigger('change');
                    }
                }

                (obj['min_estrev'] != '') ? $('#min_estrev').val(obj['min_estrev']) : $('#min_estrev').val('');
                (obj['max_estrev'] != '') ? $('#max_estrev').val(obj['max_estrev']) : $('#max_estrev').val('');
                (obj['min_price'] != '') ? $('#min_price').val(obj['min_price']) : $('#min_price').val('');
                (obj['max_price'] != '') ? $('#max_price').val(obj['max_price']) : $('#max_price').val('');
                (obj['min_rank'] != '') ? $('#min_rank').val(obj['min_rank']) : $('#min_rank').val('');
                (obj['max_rank'] != '') ? $('#max_rank').val(obj['max_rank']) : $('#max_rank').val('');
                (obj['min_estsales'] != '') ? $('#min_estsales').val(obj['min_estsales']) : $('#min_estsales').val('');
                (obj['max_estsales'] != '') ? $('#max_estsales').val(obj['max_estsales']) : $('#max_estsales').val('');
                (obj['min_rating'] != '') ? $('#min_rating').val(obj['min_rating']) : $('#min_rating').val('');
                (obj['max_rating'] != '') ? $('#max_rating').val(obj['max_rating']) : $('#max_rating').val('');
                (obj['min_review'] != '') ? $('#min_review').val(obj['min_review']) : $('#min_review').val('');
                (obj['max_review'] != '') ? $('#max_review').val(obj['max_review']) : $('#max_review').val('');
                oTable.ajax.url(decodedStr).load();
                $("#loading-img").show();
            })

            $("body").on('click', '.removeSavedSearch', function (e) {
                e.preventDefault();
                var id = $(this).attr('id');
                $("#"+id).parent().remove();
                $.ajax({
                    type: 'GET',
                    url: '<?php echo url(); ?>/opportunityfinder/remove_saved_search',
                    data: {
                        id: id
                    },
                    success: function (data) {
                        //saveSearchNow('');
                    }
                });

            })

        });




        function getQueryParams(url) {

            var qparams = {},
                    parts = (url || '').split('?'),
                    qparts, qpart,
                    i = 0;

            if (parts.length <= 1) {
                return qparams;
            } else {
                qparts = parts[1].split('&');
                for (i in qparts) {

                    qpart = qparts[i].split('=');
                    qparams[decodeURIComponent(qpart[0])] =
                            decodeURIComponent(qpart[1] || '');
                }
            }

            return qparams;
        };


        function saveSearchNow(title) {
            var base64Str = '';
            if (title != '') {
                var base64Str = makeBase64(urlString, 'encode');
            }
            $.ajax({
                type: 'GET',
                url: '<?php echo url(); ?>/opportunityfinder/saved-search',
                data: {
                    str: base64Str,
                    title: title
                },
                success: function (data) {
                    $("#accordion1 div").html(data)
                }
            });
        }



        function getRecords(flag) {
            catstr = '';
            $('input[name="category[]"]:checked:enabled').each(function () {
                catstr += $(this).val() + '_';
            });
            var strLen = catstr.length;
            catstr = catstr.slice(0, strLen - 1);

            var obj = {
                marketplace: $("#marketplace").val(),
                defaultvar: ($("#default").is(":checked")) ? 'on' : '',
                //top_seller: ($("#top_seller").is(":checked")) ? 'on' : '',
                price_default: ($("#default").is(":checked")) ? '' : (($("#price_default").is(":checked")) ? 'on' : ''),
                rank_default: ($("#default").is(":checked")) ? '' : (($("#rank_default").is(":checked")) ? 'on' : ''),
                estsales_default: ($("#default").is(":checked")) ? '' : (($("#estsales_default").is(":checked")) ? 'on' : ''),
                estrev_default: ($("#default").is(":checked")) ? '' : (($("#estrev_default").is(":checked")) ? 'on' : ''),
                rating_default: ($("#default").is(":checked")) ? '' : (($("#rating_default").is(":checked")) ? 'on' : ''),
                keywords: $("#keywords").val(),
                min_price: ($("#min_price").val() != '') ? $("#min_price").val() : '',
                max_price: ($("#max_price").val() != '') ? $("#max_price").val() : '',
                min_rank: ($("#min_rank").val() != '') ? $("#min_rank").val() : '',
                max_rank: ($("#max_rank").val() != '') ? $("#max_rank").val() : '',
                min_estsales: ($("#min_estsales").val() != '') ? $("#min_estsales").val() : '',
                max_estsales: ($("#max_estsales").val() != '') ? $("#max_estsales").val() : '',
                min_estrev: ($("#min_estrev").val() != '') ? $("#min_estrev").val() : '',
                max_estrev: ($("#max_estrev").val() != '') ? $("#max_estrev").val() : '',
                min_rating: ($("#min_rating").val() != '') ? $("#min_rating").val() : '',
                max_rating: ($("#max_rating").val() != '') ? $("#max_rating").val() : '',
                min_review: ($("#min_review").val() != '') ? $("#min_review").val() : '',
                max_review: ($("#max_review").val() != '') ? $("#max_review").val() : '',
                category: catstr,
                flag: flag
            }

            Qstring = $.param(obj);
            urlString = "<?php echo url('opportunityfinder/opportunity_ajax?'); ?>" + Qstring;
            //var str = (onlyQstring == true) ? Qstring : urlString;
            return urlString;

        }


        $(function () {

            $('#default').change(function () {
                if ($(this).is(":checked")) {
                    $(".default-wrap input[type=text]").val('').attr('disabled', 'disabled');
                    $(".default-wrap input[type=checkbox]").prop('checked', 'checked').attr('disabled', 'disabled');
                    $("#default").removeAttr('disabled');
                } else {
                    $(".default-wrap input[type=text]").val('').removeAttr('disabled');
                    $(".default-wrap input[type=checkbox]").removeAttr('checked').removeAttr('disabled');
                }
                checkNumeric();
            });

            $('#price_default').change(function () {
                if ($(this).is(":checked")) {
                    $("#min_price, #max_price").val('').attr('disabled', 'disabled');
                } else {
                    $("#min_price, #max_price").val('').removeAttr('disabled');
                }
                checkNumeric();
            });

            $("#min_rank, #max_rank").val('').attr('disabled', 'disabled');
            $('#rank_default').change(function () {
                if ($(this).is(":checked")) {
                    $("#min_rank, #max_rank").val('').attr('disabled', 'disabled');
                } else {
                    $("#min_rank, #max_rank").val('').removeAttr('disabled');
                }
                checkNumeric();
            });
            $('#estsales_default').change(function () {
                if ($(this).is(":checked")) {
                    $("#min_estsales, #max_estsales").val('').attr('disabled', 'disabled');
                } else {
                    $("#min_estsales, #max_estsales").val('').removeAttr('disabled');
                }
                checkNumeric();
            });
            $('#estrev_default').change(function () {
                if ($(this).is(":checked")) {
                    $("#min_estrev, #max_estrev").val('').attr('disabled', 'disabled');
                } else {
                    $("#min_estrev, #max_estrev").val('').removeAttr('disabled');
                }
                checkNumeric();
            });
            $('#rating_default').change(function () {
                if ($(this).is(":checked")) {
                    $("#min_rating, #max_rating").val('').attr('disabled', 'disabled');
                } else {
                    $("#min_rating, #max_rating").val('').removeAttr('disabled');
                }
                checkNumeric();
            });
            $('#review_default').change(function () {
                if ($(this).is(":checked")) {
                    $("#min_review, #max_review").val('').attr('disabled', 'disabled');
                } else {
                    $("#min_review, #max_review").val('').removeAttr('disabled');
                }
                checkNumeric();
            });

            $('#marketplace').val(1);
            $('.cls-2').hide();
            $('.cls-1').show();

            $('#marketplace').change(function () {

                if ($(this).val() == 1) {
                    $('.cls-2').hide();
                    $('.cls-1').show();
                } else if ($(this).val() == 2) {
                    $('.cls-2').show();
                    $('.cls-1').hide();
                }
            });

            $(".default-inputs [type=text]").keyup(function () {

                if ($(this).val().length > 0) {
                    if (!$.isNumeric($(this).val())) {
                        $("#submit").attr('disabled', 'disabled');
                        $(".err").show();
                        $(".err-inner").html("Please insert numeric value");
                    }
                }

                checkNumeric();

            })

            function checkNumeric() {
                $(".default-inputs [type=text]").each(function () {
                    if (this.value.length > 0) {
                        if (!$.isNumeric(this.value)) {
                            $("#submit").attr('disabled', 'disabled');
                            $(".err").show();
                            $(".err-inner").html("Please insert numeric value");
                            return false;
                        } else {


                            {
                                var id      =   this.id;
                                var value   =   this.value;

                                switch(id)
                                {
                                    case "min_price":
                                    case "max_price":
                                        if(parseInt(value)>9999) {
                                            $("#submit").attr('disabled', 'disabled');
                                            $(".err").show();
                                            $(".err-inner").html("Please input a value less than 9999");
                                            return FALSE;
                                        }
                                        break;
                                    case "min_rank":
                                    case "max_rank":
                                        if((value   != Math.floor(value)) || parseInt(value)>9999){
                                            $("#submit").attr('disabled', 'disabled');
                                            $(".err").show();
                                            $(".err-inner").html("Please input a positve integer value less than 9999");
                                            return FALSE;
                                        }


                                        break;
                                    case "min_estsales":
                                    case "max_estsales":
                                        if(parseInt(value)>9999999 || (value   != Math.floor(value))) {
                                            $("#submit").attr('disabled', 'disabled');
                                            $(".err").show();
                                            $(".err-inner").html("Please input a positve integer value less than 9999999");
                                            return FALSE;
                                        }
                                        break;
                                    case "min_estrev":
                                    case "max_estrev":
                                        if(parseInt(value)>9999999) {
                                            $("#submit").attr('disabled', 'disabled');
                                            $(".err").show();
                                            $(".err-inner").html("Please input a value less than 9999999");
                                            return FALSE;
                                        }
                                        break;
                                    case "min_rating":
                                    case "max_rating":
                                        if(parseFloat(value)>5) {
                                            $("#submit").attr('disabled', 'disabled');
                                            $(".err").show();
                                            $(".err-inner").html("Please input a value less than 5");
                                            return FALSE;
                                        }
                                        var decimal_val =   String(value).split('.');
                                        if(typeof decimal_val[1] != "undefined")
                                        {
                                            if(parseInt(decimal_val[1])>9)
                                            {
                                                $("#submit").attr('disabled', 'disabled');
                                                $(".err").show();
                                                $(".err-inner").html("Only One digit after decimal is allowed");
                                                return FALSE;
                                            }
                                            if(typeof decimal_val[2] != "undefined")
                                            {
                                                $("#submit").attr('disabled', 'disabled');
                                                $(".err").show();
                                                $(".err-inner").html("Only One decimal point is allowed");
                                                return FALSE;
                                            }
                                        }


                                        break;
                                    case min_review:
                                    case max_review:
                                        if(parseInt(value)>9999 || (value   != Math.floor(value))) {
                                            $("#submit").attr('disabled', 'disabled');
                                            $(".err").show();
                                            $(".err-inner").html("Please input a positve integer value less than 9999");
                                            return FALSE;
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }

                            $(".err").hide();
                            $(".err-inner").html("");
                            $("#submit").removeAttr('disabled');
                        }
                    } else {
                        $(".err").hide();
                        $(".err-inner").html("");
                        $("#submit").removeAttr('disabled');
                    }
                });
            }




        })

        function makeBase64(str, type) {
            var Base64 = {_keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function (e) {
                var t = "";
                var n, r, i, s, o, u, a;
                var f = 0;
                e = Base64._utf8_encode(e);
                while (f < e.length) {
                    n = e.charCodeAt(f++);
                    r = e.charCodeAt(f++);
                    i = e.charCodeAt(f++);
                    s = n >> 2;
                    o = (n & 3) << 4 | r >> 4;
                    u = (r & 15) << 2 | i >> 6;
                    a = i & 63;
                    if (isNaN(r)) {
                        u = a = 64
                    } else if (isNaN(i)) {
                        a = 64
                    }
                    t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
                }
                return t
            }, decode: function (e) {
                var t = "";
                var n, r, i;
                var s, o, u, a;
                var f = 0;
                e = e.replace(/[^A-Za-z0-9\+\/\=]/g, "");
                while (f < e.length) {
                    s = this._keyStr.indexOf(e.charAt(f++));
                    o = this._keyStr.indexOf(e.charAt(f++));
                    u = this._keyStr.indexOf(e.charAt(f++));
                    a = this._keyStr.indexOf(e.charAt(f++));
                    n = s << 2 | o >> 4;
                    r = (o & 15) << 4 | u >> 2;
                    i = (u & 3) << 6 | a;
                    t = t + String.fromCharCode(n);
                    if (u != 64) {
                        t = t + String.fromCharCode(r)
                    }
                    if (a != 64) {
                        t = t + String.fromCharCode(i)
                    }
                }
                t = Base64._utf8_decode(t);
                return t
            }, _utf8_encode: function (e) {
                e = e.replace(/\r\n/g, "\n");
                var t = "";
                for (var n = 0; n < e.length; n++) {
                    var r = e.charCodeAt(n);
                    if (r < 128) {
                        t += String.fromCharCode(r)
                    } else if (r > 127 && r < 2048) {
                        t += String.fromCharCode(r >> 6 | 192);
                        t += String.fromCharCode(r & 63 | 128)
                    } else {
                        t += String.fromCharCode(r >> 12 | 224);
                        t += String.fromCharCode(r >> 6 & 63 | 128);
                        t += String.fromCharCode(r & 63 | 128)
                    }
                }
                return t
            }, _utf8_decode: function (e) {
                var t = "";
                var n = 0;
                var r = c1 = c2 = 0;
                while (n < e.length) {
                    r = e.charCodeAt(n);
                    if (r < 128) {
                        t += String.fromCharCode(r);
                        n++
                    } else if (r > 191 && r < 224) {
                        c2 = e.charCodeAt(n + 1);
                        t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                        n += 2
                    } else {
                        c2 = e.charCodeAt(n + 1);
                        c3 = e.charCodeAt(n + 2);
                        t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                        n += 3
                    }
                }
                return t
            }}
            var string = (type == 'encode') ? Base64.encode(str) : Base64.decode(str);
            return string;
//        var decodedString = Base64.decode(encodedString);
        }



    </script>

@stop