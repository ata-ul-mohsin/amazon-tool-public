
Dear {{ $user['email'] }} you have received a feedback with low rating.

{{ $feedback['rater'] }} has given {{ $feedback['rating'] }} with comment {{ $feedback['rating_text'] }} on {{ $feedback['date'] }}
