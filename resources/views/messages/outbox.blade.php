@extends('main_template')

@section ("title")

    Outbox Messages List

@stop


@section("content")
    <div class="row">
        <div class="col-md-12">
           <div class="table-responsive">
                <table class="table table-bordered table-striped data_table" data-ride="datatables">
                    <thead>
                    <tr bgcolor="#f4f4f4">
                        <th width="25%">Message</th>
                        <th width="25%">Order</th>
                        <th width="35%">Due Date</th>
                        <th width="15%">Action</th>

                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($messages) && is_array($messages))
                        @foreach($messages as $message)
                            <?php $message =  (array)$message; ?>
                            <tr>
                                <td> {{{ $message['title'] }}}</td>

                                <td> {{{ $message['order_id'] }}}</td>

                                <td>
                                 <?php
                                    $due_date  =   \Carbon\Carbon::createFromTimeStamp(strtotime($message['due_date']))
                                                    ->diffForHumans();
                                    echo $due_date;
                                    ?>

                                   </td>
                                <td nowrap>
                                    <a  data-href=" {!!  url('messages/outbox/delete/'.$message['id']) !!}" class="btn btn-default   confirm_delete confirm_delete_other"><i class="fa fa-trash-o text"></i></a>
                                </td>
                            </tr>
                        @endforeach

                    @endif

                    </tbody>

                </table>

            </div>

        </div>

    </div>
@stop