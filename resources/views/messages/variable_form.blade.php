@extends('main_template');
@section('content')
    <section class="scrollable padder">
        <div class="m-b-md">
            <h3 class="m-b-none">Message Variables</h3>
        </div>
        <div class="row">

            <div class="col-sm-12">
                <section class="panel panel-default">
                    <header class="panel-heading font-bold"><?php if(!empty($variable['id'])) echo "Edit"; else echo "Add"; ?> Message Variables</header>
                    <div class="panel-body">
                        <form action="{!! url('messages/save_variable') !!}" method="post" class="bs-example form-horizontal" enctype="multipart/form-data" id="consumer_form"  data-validate="parsley" >
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Variable Name</label>
                                <div class="col-lg-4">
                                    <input type="text" placeholder="Enter Variable Name" class="form-control" name="name" id="name" value="<?php echo (isset($variable['name'])?$variable['name']:''); ?>" data-required="true" data-parsley-trigger="keyup">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Variable Value</label>
                                <div class="col-lg-4">
                                    <input type="text" placeholder="Enter Variable Value" class="form-control" name="value" id="value" value="<?php echo isset($variable['value'])?$variable['value']:''; ?>"  data-required="true" data-parsley-trigger="keyup">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label"></label>
                                <div class="col-lg-4">
                                    <input type="hidden" id="variable_id" name="variable_id" value="<?php echo isset($variable['id'])?$variable['id']:''; ?>" />
                                    <button class="btn btn-sm btn-success" type="submit" >Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </section>
@stop