@extends('main_template')

@section ("title")

    Orders List

@stop


@section("content")
    <div class="wrapper-inline-form">

        <div class="row">

            <div class="col-md-4">

                {!! csrf_field() !!}





                <div class="form-group">

                    <div  name="marketplace_select" id="marketplace_select">

                    </div>

                </div>



                <div class="form-group">

                    <?php /*      <label class="col-lg-4 control-label">Status :</label> */ ?>



                </div>

            </div>

        </div>

    </div>
    <div class="row">

        <div class="col-md-12">



            <div class="table-responsive">

                <table class="table table-bordered table-striped" data-ride="datatables" id="server_sisde_dtable">
                    <thead>
                        <tr bgcolor="#f4f4f4">
                            <th>Sr</th>
                            <th>Order Id</th>
                            <th>Buyer Name</th>
                            <th>Buyer Email</th>
                            <th>Fulfillment Channel</th>
                            <th>Status</th>
                            <th>Shipping Country</th>
                        </tr>

                    </thead>
                    <tbody>

                    </tbody>

                </table>

            </div>

        </div>

    </div>

@stop
@section('additional_js')

    <script>

        $(function() {

            var table   =   $('#server_sisde_dtable').DataTable( {

                "processing": true,

                "serverSide": true,

                "ajax": "<?php echo url("list-order-data"); ?>",

                'drawCallback':function() {



                },

                fnInitComplete : function() {

                    this.columnFilter({

                        sPlaceHolder: "head:after",

                        aoColumns: [
                            { sSelector: "#marketplace_select", type: "select", values: [<?php echo $setting_str; ?>]},

                            null,
                            null,
                            null,
                            null,
                            null,
                            null

                        ]

                    });

                }

            });


        } );

    </script>
@stop