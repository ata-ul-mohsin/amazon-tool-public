@extends("main_template")
@section("content")
    <div class="table-responsive">
        <table class="table table-striped m-b-none sortable data-table" data-ride="datatables">
            <thead>
            <tr>
                <th width="20%">Name</th>
                <th width="20%">Value</th>
                <th width="10%">Action</th>
            </tr>
            </thead>
            <tbody>
           @if(!empty($variables) && is_array($variables))
                @foreach($variables as $variable)
                <tr>
                    <td> {{{ $variable['name'] }}}</td>
                    <td> {{{ $variable['value'] }}}</td>
                    <td nowrap>
                        <a href="{{{ url('messages/edit-variable/'.$variable['id']) }}}"class="btn btn-default"><i class="fa fa-pencil text"></i></a>&nbsp;
                        <a data-href="{{{ url('messages/delete-variable/'.$variable['id']) }}}" class="btn btn-default confirm_delete"><i class="fa fa-trash-o text"></i></a>
                    </td>

                </tr>
                @endforeach
              @endif
            </tbody>
        </table>
    </div>
    <p class="small">** These fields are only available for FBA orders, or Merchant Fulfilled order where a tracking number has been uploaded into FeedbackGenius</p>

    <h3>URLs Only</h3>
    <p>You can change the 5-characters '-link' on the end of any of these link variables to '-url' to render the URL only, without the surrounding &lt;a&gt; anchor tags.</p>

    <h3>Link Text</h3>
    <p>For the link variables, you can specify the link text by appending a colon and the link text that you'd like.  For example,
    <pre>
[[order-link:View Order on Amazon.com]]
</pre>
    Will be rendered as:<br />
    <a href="#">View Order on Amazon.com</a>

@stop
