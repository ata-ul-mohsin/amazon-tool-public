@extends('main_template')
@section ("title")
    Message Settings
@stop
@section('content')
        <div class="col-sm-12">
                    <form action="{!! url('messages/settings') !!}" method="post" class="bs-example form-horizontal" enctype="multipart/form-data" id="consumer_form"  data-validate="parsley" >
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label class="col-lg-4 control-label">Account Name</label>
                            <div class="col-lg-4">
                                <input type="text" placeholder="Enter Account Name" class="form-control" name="account_name" id="account_name" value="<?php echo (isset($settings['account_name'])?$settings['account_name']:''); ?>" data-type="email" data-required="true" data-parsley-trigger="keyup">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label">Seller Central Email</label>
                            <div class="col-lg-4">
                                <input type="text" placeholder="Enter Seller Central email" class="form-control" name="seller_central_email" id="seller_central_email" value="<?php echo isset($settings['seller_central_email'])?$settings['seller_central_email']:''; ?>" data-type="email" data-required="true" data-parsley-trigger="keyup">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label">Testing To Email</label>
                            <div class="col-lg-4">
                                <input type="text" placeholder="Enter Testing To email" class="form-control" name="testing_to_email" id="testing_to_email" value="<?php echo isset($settings['testing_to_email'])?$settings['testing_to_email']:''; ?>" data-type="email" data-required="true" data-parsley-trigger="keyup">
                            </div>
                        </div>

                        <div class="form-group">
                            <label  class="col-lg-4 control-label">Time Zone</label>
                            <div class="col-lg-4">
                                <select class="form-control" data-required="true" data-parsley-trigger="change" name="time_zone" id="time_zone">
                                    <option value="">Select Time Zone</option>
                                    <?php

                                    if(isset($timezones))
                                        foreach($timezones as $timezone)
                                        {
                                    ?>
                                    <option value="<?php echo $timezone['id']; ?>" <?php if(isset($settings['time_zone']) && $settings['time_zone'] == $timezone['id'])echo 'selected'; ?>   > <?php echo $timezone['name']; ?></option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"></label>
                            <div class="col-lg-4">
                                <button class="btn btn-sm btn-primary" type="submit" >Update</button>
                            </div>
                        </div>
                      </form>

                </div>

@stop