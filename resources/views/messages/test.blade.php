<?php

    public function saveSetting()
    {
    $update_id = Input::get("id");
    if (Request::isMethod('post')) {
    /*  echo "<pre>";
                  print_r(Input::all());
                  echo "</pre>";
    exit;*/
    $result = Setting::saveToDb(Input::all());
    /*    echo "<pre>";
                  print_r($result);
                  echo "</pre>";
    exit;*/

    if ($result['status'] == '1') {
    return Redirect::route('settings')->with('setting', 'success= ' . $result['setting']);
    } else if ($result['status'] == '2') {
    return Redirect::back()->withErrors($result['validator']);
    } else if ($result['status'] == '3') {
    return Redirect::route('settings')->with('setting', 'danger= ' . $result['setting']);
    }
    } else if ($update_id) {
    if (is_numeric($update_id) && $update_id > 0) {
    $setting = Setting::find($update_id);
    if (!empty($setting)) {

    return View::make('settings.form', array("setting"=>$setting,"countries"=>$this->countries));
    } else {
    return Redirect::route('settings')->with('setting', 'danger= Setting not found in system!');
    }
    } else {
    return Redirect::route('settings')->with('setting', 'danger= Setting not found in system!');
    }
    } else {
    $setting = new Setting();
    return View::make('settings.form', array("setting"=>$setting,"countries"=>$this->countries));

    }
    }