@extends('main_template')
@section ("title")
   Sent Messages List
@stop

@section("content")
    <div class="row">
        <div class="col-md-12">

            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th>Sr</th>
                        <th>Message</th>
                        <th>Order</th>
                    </tr>
                    @if(!empty($sent_messages))
                        @foreach($sent_messages as $message)
                            <tr>
                                <td>{{ $count++    }}</td>
                                <td>{{ $message['content']  }}</td>
                                <td>{{ $message['order']['order_id'] }}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3" align="center"><b>No Message Sent</b></a> </td>

                        </tr>
                    @endif


                </table>
            </div>
        </div>
    </div>
@stop
