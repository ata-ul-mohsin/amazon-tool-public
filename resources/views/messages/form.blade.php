<?php

$marketplace_val                    =       Input::old('marketplace');
$statuses_val                       =       Input::old('statuses');
$when_send_to_buyer_val             =       Input::old('when_send_to_buyer');
$send_after_val                     =       Input::old('send_after');
$fulfillment_channel_val            =       Input::old('fulfillment_channel');
$shipping_country_val               =       Input::old('shipping_country');
$shipping_country_is_not_val        =       Input::old('shipping_country_is_not');
$id                                 =       Input::old('id');

?>
@extends('main_template')
@section ("title")
    Message Definition
@stop
@section("content")
    @if(isset($message))
        {!! Form::model($message, ['route' => ['messages.update', $message->id], 'method' => 'patch', 'data-parsley-validate' => '', 'enctype' => "multipart/form-data" ]) !!}
        <?php
        if((count($errors) == 0))
        {


         if(empty($marketplace_val))
            $marketplace_val                    =       !empty($message->marketplace)?$message->marketplace:"";
        if(empty($statuses_val))
            $statuses_val                    =       !empty($message->status)?$message->status:"";
        if(empty($when_send_to_buyer_val))
            $when_send_to_buyer_val                    =       !empty($message->when_send_to_buyer)?$message->when_send_to_buyer:"";
        if(empty($send_after_val))
            $send_after_val                    =       !empty($message->send_after)?$message->send_after:"";
        if(empty($fulfillment_channel_val))
            $fulfillment_channel_val                    =       !empty($message->fulfillment_channel)?$message->fulfillment_channel:"";
        if(empty($shipping_country_val))
            $shipping_country_val                    =       !empty($message->shipping_country)?explode(",",$message->shipping_country):"";
        if(empty($shipping_country_is_not_val))
            $shipping_country_is_not_val                    =       !empty($message->shipping_country_is_not)?explode(",",$message->shipping_country_is_not):"";
        if(empty($id))
            $id                    =       !empty($message->id)?$message->id:"";
   }


        ?>
    @else
        {!! Form::open(['route' => 'messages.store', 'data-parsley-validate' => " ", 'enctype' => "multipart/form-data" ]) !!}
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="tab-panel-container">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#new_email_details" aria-controls="home" role="tab" data-toggle="tab">Details</a></li>
                    <li role="presentation"><a href="#new_email_body" aria-controls="profile" role="tab" data-toggle="tab">Body</a></li>
                    <li role="presentation"><a href="#new_email_when" aria-controls="messages" role="tab" data-toggle="tab">When</a></li>
                    <li role="presentation"><a href="#new_email_filters" aria-controls="settings" role="tab" data-toggle="tab">Filters</a></li>
                    <li role="presentation"><a href="#new_email_attachments" aria-controls="settings" role="tab" data-toggle="tab">Attachments</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="new_email_details">
                        <div class="panel-head">
                            <h4>Details</h4>
                        </div>
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Title:</label>
                                <div class="col-sm-4">
                                    {!! Form::text('title', Input::old('title'), ['class' => 'form-control','data-parsley-trigger' => 'change','required' => '']) !!}

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Subject:</label>
                                <div class="col-sm-4">
                                    {!! Form::text('subject', Input::old('subject'), ['class' => 'form-control','data-parsley-trigger' => 'change','required' => '']) !!}

                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Market Place</label>
                                <div class="col-sm-4">
                                    {!! Form::select('marketplace', $marketplaces, $marketplace_val, ['id' => 'marketplace', 'class' => 'form-control chosen_select']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-4">
                                    {!! Form::select('status', $statuses, $statuses_val, ['id' => 'status', 'class' => 'form-control chosen_select']) !!}

                                </div>
                            </div>
                        </div>
                    </div>

                    <!--- BODY PANEL -->
                    <div role="tabpanel" class="tab-pane" id="new_email_body">
                        <div class="panel-head">
                            <h4>Content</h4>
                        </div>
                        <div class="form-horizontal">
                            <div class="form-group">

                                <div class="col-sm-12">
                                    {!! Form::textarea('content', Input::old('content'), ['id' => 'marketplace', 'rows' => '5','class' => 'form-control ckeditor','data-parsley-trigger' => 'change','required' => '']) !!}
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- END BODY PANEL -->




                    <!-- WHEN PANEL -->
                    <div role="tabpanel" class="tab-pane" id="new_email_when">
                        <div class="panel-head">
                            <h4>When</h4>
                        </div>
                        <div class="form-horizontal">

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Send On:</label>
                                <div class="col-sm-4">
                                    {!! Form::select('when_send_to_buyer', $when_send_to_buyer, $when_send_to_buyer_val, ['id' => 'when_send_to_buyer', 'class' => 'form-control chosen_select']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">After the Order:</label>
                                <div class="col-sm-4">
                                    {!! Form::select('send_after', $send_after, $send_after_val, ['id' => 'send_after', 'class' => 'form-control chosen_select']) !!}

                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-2 control-label">Priority:</label>
                                <div class="col-sm-4">
                                    {!! Form::text('priority', Input::old('priority'), ['class' => 'form-control']) !!}

                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-2 control-label">Order After:</label>
                                <div class="col-sm-4">
                                    <div class="input-group">
                                        {!! Form::text('order_placed_on_or_after', Input::old('order_placed_on_or_after'), ['class' => 'form-control form-control datepicker']) !!}

                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-2 control-label">Order Before:</label>
                                <div class="col-sm-4">
                                    <div class="input-group">
                                        {!! Form::text('order_placed_before', Input::old('order_placed_before'), ['class' => 'form-control form-control datepicker']) !!}
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>
                    <!-- END WHEN PANEL -->

                    <!-- FILTER PANEL -->
                    <div role="tabpanel" class="tab-pane" id="new_email_filters">
                        <div class="panel-head">
                            <h4>Filter</h4>
                        </div>

                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">SKU is:</label>
                                <div class="col-sm-4">
                                    {!! Form::text('sku_filter', Input::old('sku_filter'), ['class' => 'form-control']) !!}
                                    <span class="help-block">(Comma-separated list. No spaces)</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">ASIN:</label>
                                <div class="col-sm-4">
                                    {!! Form::text('asin_filter', Input::old('asin_filter'), ['class' => 'form-control']) !!}
                                    <span class="help-block">(Comma-separated list. No spaces)</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Fulfillment Channel</label>
                                <div class="col-sm-4">
                                    {!! Form::select('fulfillment_channel', $fulfillment_channel, $fulfillment_channel_val, ['id' => 'fulfillment_channel', 'class' => 'form-control chosen_select']) !!}

                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Shipping Country IS:</label>
                                <div class="col-sm-4">
                                    {!! Form::select('shipping_country', $countries, $shipping_country_val, ['id' => 'shipping_country', 'name'=>'shipping_country[]', 'class' => 'form-control chosen_select', 'multiple' => 'multiple']) !!}

                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Shipping Country IS NOT:</label>
                                <div class="col-sm-4">
                                    {!! Form::select('shipping_country_is_not', $countries , $shipping_country_is_not_val, ['id' => 'shipping_country_is_not', 'name'=>'shipping_country_is_not[]', 'class' => 'form-control chosen_select', 'multiple' => 'multiple']) !!}

                                </div>
                            </div>

                        </div>

                    </div>
                    <!-- END FILTER PANEL -->

                    <!-- ATTACHMENT PANEL -->
                    <div role="tabpanel" class="tab-pane" id="new_email_attachments">
                        <div class="panel-head">
                            <h4>Attachment</h4>
                        </div>

                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Attachment:</label>
                                <div class="col-sm-4">
                                    <div class="input-group">

                                        <input type="text" class="form-control" readonly>
                <span class="input-group-btn"> <span class="btn btn-default btn-file"> Browse&hellip;
                        <input type="file" name="message_file" />
                </span> </span> </div>
                                </div>
                            </div>
                        </div>
                   </div>
                    <!-- END ATTACHMENTY PANEL -->
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">&nbsp;</label>
            <div class="col-sm-4">
                {!! Form::hidden('id',$id, ['name' => 'id']) !!}
                {!! Form::submit('  Save  ', ['name' => 'save','class' => 'btn btn-primary', 'type' => "button"]) !!}
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop