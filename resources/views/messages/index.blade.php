@extends('main_template')

@section ("title")

    Message List

@stop

@section("right_header")
    <style>
        .page-header .add_link a {
            font-size: 12px !important;
            white-space: nowrap  !important;
        }
    </style>
    <div class="col-sm-6" xmlns="http://www.w3.org/1999/html">

        <div class="add_link col-sm-3">
            <a href="<?php echo url('messages/settings'); ?>"><span class="fa fa-gear"></span>&nbsp;Message Settings</a>
        </div>
        <div class="add_link col-sm-3" >
            <a href="<?php echo url('messages/sent'); ?>"><span class="fa fa-envelope"></span>&nbsp;Sent Messages</a>
        </div>
        <div class="add_link col-sm-3" >
            <a href="<?php echo url('messages/outbox'); ?>"><span class="fa fa-envelope"></span>&nbsp;Outbox Messages</a>
        </div>
        <div class="add_link col-sm-3" >
            <a href="<?php echo url('messages/create'); ?>"><span class="fa fa-plus"></span>&nbsp;Add New</a>
        </div>
    </div>
@stop

@section("content")

    <div class="row">

        <div class="col-md-12">



            <div class="table-responsive">

                <table class="table table-bordered table-striped data_table" data-ride="datatables">

                    <thead>

                    <tr bgcolor="#f4f4f4">

                        <th width="25%">Name</th>

                        <th width="25%">When</th>

                        <th width="35%">Subject</th>

                        <th width="15%">Status</th>

                        <th>Action</th>

                    </tr>

                    </thead>

                    <tbody>

                    @if(!empty($messages) && is_array($messages))

                        @foreach($messages as $message)

                            <tr>

                                <td> {{{ $message['title'] }}}</td>

                                <td> {{{ $message['when_send_to_buyer'] }}}</td>

                                <td> {{{ $message['subject'] }}}</td>

                                <td> {{{ $message['status'] }}}</td>

                                <td nowrap>

                                    <a href=" {{{ route('messages.edit',array("id"=>$message['id'])) }}}"class="btn btn-default"><i class="fa fa-pencil text"></i></a>&nbsp;

                                    <a data-href=" {!! route('messages.destroy',array('id'=>$message['id'])) !!}" class="btn btn-default  confirm_delete"><i class="fa fa-trash-o text"></i></a>
                                </td>

                            </tr>

                        @endforeach

                    @endif

                    </tbody>

                </table>

            </div>

        </div>

    </div>
@stop