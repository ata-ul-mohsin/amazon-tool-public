@extends('main_template')
@section ("title")
    Blacklist Email Definition
@stop
@section("content")
    @if(isset($blacklist_email))
        {!! Form::model($blacklist_email, ['route' => ['blacklist-emails.update', $blacklist_email->id], 'method' => 'patch', 'data-parsley-validate' => '', 'enctype' => "multipart/form-data" ]) !!}
    @else
        {!! Form::open(['route' => 'blacklist-emails.store', 'data-parsley-validate' => '', 'enctype' => "multipart/form-data" ]) !!}
    @endif
    <div class="row">
        <div class="col-md-6">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Blacklist Email:</label>
                    <div class="col-sm-7">
                        {!! Form::text('email', Input::old('email'), ['class' => 'form-control','data-parsley-trigger' => 'change','required' => '','data-parsley-type'=>'email']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Account Name:</label>
                    <div class="col-sm-7">
                        {!! Form::select('setting_id', $amazon_settings, Input::old('setting_id'), ['id' => 'setting_id', 'class' => 'form-control chosen_select','data-parsley-trigger' => 'change','required' => '']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Blacklist Email:</label>
                    <div class="col-sm-7">
                        {!! Form::hidden('id',Input::old('id'), ['name' => 'id']) !!}
                        {!! Form::submit('Save', ['name' => 'save','class' => 'btn btn-primary']) !!}
                        {!! Form::submit('Save &amp; Add New', ['name' => 'save_add','class' => 'btn btn-black']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! Form::close() !!}
@stop