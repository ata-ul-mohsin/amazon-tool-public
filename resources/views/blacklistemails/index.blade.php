<?php
        $i=1;
?>
@extends('main_template')
@section ("title")
    BlacklistEmails List
@stop
@section("right_header")
    <div class="col-sm-6" xmlns="http://www.w3.org/1999/html">
        <div class="add_link">
            <a href="<?php echo url('blacklist-emails/create'); ?>"><span class="fa fa-plus"></span> Add New</a>
        </div>
    </div>
@stop
@section("content")
    <div class="row">
        <div class="col-md-12">

            <div class="table-responsive">
                <table class="table table-bordered table-striped data_table" data-ride="datatables">
                    <thead>
                    <tr bgcolor="#f4f4f4">
                        <th width="10%">Sr</th>
                        <th width="30%">Email</th>
                        <th width="30%">Account Name</th>
                        <th width="30%">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($blacklist_emails) && is_array($blacklist_emails))
                        @foreach($blacklist_emails as $blacklist_email)
                            <tr>
                                <td> {!! $i++ !!}</td>
                                <td> {{{ $blacklist_email['email'] }}}</td>
                                <td> {{{ $blacklist_email['settings']['account_name'] }}}</td>
                                <td nowrap>
                                    <a href=" {{{ route('blacklist-emails.edit',array("id"=>$blacklist_email['id'])) }}}"class="btn btn-default"><i class="fa fa-pencil text"></i></a>&nbsp;
                                    <a data-href="{!! route('blacklist-emails.destroy',array('id'=>$blacklist_email['id'])) !!}" class="btn btn-default  confirm_delete"><i class="fa fa-trash-o text"></i></a>

                                </td>

                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
