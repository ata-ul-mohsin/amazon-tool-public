




!function(e) {
    e.fn.columnFilter = function(t) {
        function n(e, t, n, a, r) {
            if ("undefined" == typeof t) return new Array;
            "undefined" == typeof n && (n = !0), "undefined" == typeof a && (a = !0), "undefined" == typeof r && (r = !0);
            var l;
            l = 1 == a ? e.aiDisplay : e.aiDisplayMaster;
            for (var i = new Array, s = 0, o = l.length; o > s; s++) {
                var c = l[s],
                    u = x.fnGetData(c),
                    f = u[t];
                (1 != r || 0 != f.length) && (1 == n && jQuery.inArray(f, i) > -1 || i.push(f))
            }
            return i.sort()
        }

        function a(e) {
            return F.bUseColVis ? e : x.fnSettings().oApi._fnVisibleToColumnIndex(x.fnSettings(), e)
        }

        function r(t, n, r, l, i, s) {
            var o = "text_filter form-control";
            l && (o = "number_filter form-control"), g = g.replace(/(^\s*)|(\s*$)/g, "");
            var c = t.fnSettings().aoPreSearchCols[h].sSearch,
                u = "search_init ",
                f = g;
            "" != c && "^" != c && (f = l && "^" == c.charAt(0) ? c.substr(1) : c, u = "");
            var d = e('<input type="text" class="' + u + o + '" value="' + f + '" rel="' + h + '"/>');
            void 0 != s && -1 != s && d.attr("maxlength", s), v.html(d), l ? v.wrapInner('<span class="filter_column filter_number" />') : v.wrapInner('<span class="filter_column filter_text" />'), p[h] = g;
            var m = h;
            l && !t.fnSettings().oFeatures.bServerSide ? d.keyup(function() {
                t.fnFilter("^" + this.value, a(m), !0, !1), S()
            }) : d.keyup(function() {
                if (t.fnSettings().oFeatures.bServerSide && 0 != i) {
                    var l = (t.fnSettings().aoPreSearchCols[m].sSearch, e(this).data("dt-iLastFilterLength"));
                    "undefined" == typeof l && (l = 0);
                    var s = this.value.length;
                    if (Math.abs(s - l) < i) return;
                    e(this).data("dt-iLastFilterLength", s)
                }
                t.fnFilter(this.value, a(m), n, r), S()
            }), d.focus(function() {
                e(this).hasClass("search_init") && (e(this).removeClass("search_init"), this.value = "")
            }), d.blur(function() {
                "" == this.value && (e(this).addClass("search_init"), this.value = p[m])
            })
        }

        function l(t) {
            v.html(d(0));
            var n = t.attr("id") + "_range_from_" + h,
                r = e('<input type="text" class="number_range_filter form-control" id="' + n + '" rel="' + h + '"/>');
            v.append(r), v.append(d(1));
            var l = t.attr("id") + "_range_to_" + h,
                i = e('<input type="text" class="number_range_filter form-control" id="' + l + '" rel="' + h + '"/>');
            v.append(i), v.append(d(2)), v.wrapInner('<span class="filter_column filter_number_range form-control" />');
            var s = h;
            _.push(h), t.dataTableExt.afnFiltering.push(function(e, r, i) {
                if (t.attr("id") != e.sTableId) return !0;
                if (null == document.getElementById(n)) return !0;
                var o = 1 * document.getElementById(n).value,
                    c = 1 * document.getElementById(l).value,
                    u = "-" == r[a(s)] ? 0 : 1 * r[a(s)];
                return "" == o && "" == c ? !0 : "" == o && c >= u ? !0 : u >= o && "" == c ? !0 : u >= o && c >= u ? !0 : !1
            }), e("#" + n + ",#" + l, v).keyup(function() {
                var e = 1 * document.getElementById(n).value,
                    a = 1 * document.getElementById(l).value;
                0 != e && 0 != a && e > a || (t.fnDraw(), S())
            })
        }

        function i(t) {
            var n = m.split(/[}{]/);
            v.html("");
            var r = t.attr("id") + "_range_from_" + h,
                l = e('<input type="text" class="date_range_filter form-control" id="' + r + '" rel="' + h + '"/>');
            l.datepicker();
            var i = t.attr("id") + "_range_to_" + h,
                s = e('<input type="text" class="date_range_filter form-control" id="' + i + '" rel="' + h + '"/>');
            for (ti = 0; ti < n.length; ti++) n[ti] == F.sDateFromToken ? v.append(l) : n[ti] == F.sDateToToken ? v.append(s) : v.append(n[ti]);
            v.wrapInner('<span class="filter_column filter_date_range" />'), s.datepicker();
            var o = h;
            _.push(h), t.dataTableExt.afnFiltering.push(function(n, r, i) {
                if (t.attr("id") != n.sTableId) return !0;
                var c = l.datepicker("getDate"),
                    u = s.datepicker("getDate");
                if (null == c && null == u) return !0;
                var f = null;
                try {
                    if (null == r[a(o)] || "" == r[a(o)]) return !1;
                    f = e.datepicker.parseDate(e.datepicker.regional[""].dateFormat, r[a(o)])
                } catch (d) {
                    return !1
                }
                return null == f ? !1 : null == c && u >= f ? !0 : f >= c && null == u ? !0 : f >= c && u >= f ? !0 : !1
            }), e("#" + r + ",#" + i, v).change(function() {
                t.fnDraw(), S()
            })
        }

        function s(t, a, r, l, i, s, o, u) {
            var acc_id    =   String(l.selector).slice(1);
            var account_select  =  acc_id.match('marketplace_select');
            var brand_select  =  acc_id.match('brand_select');
            var f = i,
                d = String(l.selector).slice(1) + "_filter";
            f.match("Sr") && (f = "Account"), f.match("ASIN") && (f = "Brand"), null == a && (a = n(t.fnSettings(), r, !0, !1, !0));
            var p = r,
                g = t.fnSettings().aoPreSearchCols[h].sSearch;
            (null == g || "" == g) && (g = o);

            if(account_select != null || brand_select != null)
                var v = '<select name="' + d + '"  id="' + d + '" class="search_init select_filter form-control chosen_select" rel="' + h + '">"';
            else
                var v = '<select name="' + d + '"  id="' + d + '" class="search_init select_filter form-control chosen_select" rel="' + h + '"><option value="" class="search_init">' + f + "</option>";

            u && (v = '<select name="' + d + '"  id="' + d + '"  class="search_init select_filter form-control chosen_select" rel="' + h + '" multiple>');
            var m = 0,
                b = a.length;
            for (m = 0; b > m; m++)
                if ("object" != typeof a[m]) {
                    var _ = String(a[m]).split(":::"),
                        x = _[0],
                        k = _[0];
                    "undefined" != typeof _[1] && "" != _[1] && (k = _[1]);
                    var y = "";
                    (escape(a[m]) == g || escape(a[m]) == escape(g)) && (y = "selected "), v += "<option " + y + ' value="' + escape(x) + '">' + k + "</option>"
                } else {
                    var y = "";
                    s ? (a[m].value == g && (y = "selected "), v += "<option " + y + 'value="' + a[m].value + '">' + a[m].label + "</option>") : (escape(a[m].value) == g && (y = "selected "), v += "<option " + y + 'value="' + escape(a[m].value) + '">' + a[m].label + "</option>")
                }
            var F = e(v + "</select>");
            l.html(F), l.wrapInner('<span class="filter_column filter_select" />'), u ? F.change(function() {
                "" != e(this).val() ? e(this).removeClass("search_init") : e(this).addClass("search_init");
                var n = e(this).val(),
                    a = [];
                if (null == n || n == []) var r = "^(.*)$";
                else {
                    e.each(n, function(e, t) {
                        a.push(c(t))
                    });
                    var r = "^(" + a.join("|") + ")$"
                }
                t.fnFilter(r, p, !0, !1)
            }) : (F.change(function() {
                "" != e(this).val() ? e(this).removeClass("search_init") : e(this).addClass("search_init"), s ? t.fnFilter(e(this).val(), r, s) : t.fnFilter(unescape(e(this).val()), r), S()
            }), null != g && "" != g && t.fnFilter(unescape(g), r))
            $(".chosen_select").chosen({
                width: '100%'
            });
        }

        function o(e, t, n, r, l) {
            var i = e.fnSettings();
            null != t && "function" != typeof t || "" == i.sAjaxSource || i.oFeatures.bServerSide || i.aoDrawCallback.push({
                fn: function(i, o, c) {
                    return function(u) {
                        return 2 != u.iDraw || null == u.sAjaxSource || "" == u.sAjaxSource || u.oFeatures.bServerSide ? void 0 : s(e, t && t(u.aoData, u), a(i), o, c, n, r, l)
                    }
                }(h, v, g),
                sName: "column_filter_" + h
            }), s(e, "function" == typeof t ? null : t, a(h), v, g, n, r, l)
        }

        function c(e) {
            return e.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&")
        }

        function u(t) {
            var n, a = h,
                r = '<div class="dropdown select_filter form-control"><a class="dropdown-toggle" data-toggle="dropdown" href="#">' + g + '<b class="caret"></b></a><ul class="dropdown-menu" role="menu"><li data-value=""><a>Show All</a></li>',
                l = t.length;
            for (n = 0; l > n; n++) r += '<li data-value="' + t[n] + '"><a>' + t[n] + "</a></li>";
            var i = e(r + "</ul></div>");
            v.html(i), v.wrapInner('<span class="filterColumn filter_select" />'), i.find("li").click(function() {
                x.fnFilter(e(this).data("value"), a)
            })
        }

        function f(t, a) {
            null == a && (a = n(t.fnSettings(), h, !0, !0, !0));
            var r, l = h,
                i = "",
                s = a.length,
                o = g.replace("%", "Perc").replace("&", "AND").replace("$", "DOL").replace("£", "STERL").replace("@", "AT").replace(/\s/g, "_");
            o = o.replace(/[^a-zA-Z 0-9]+/g, "");
            var c = g;
            (null != F.sFilterButtonText || void 0 != F.sFilterButtonText) && (c = F.sFilterButtonText);
            var u = 10,
                f = 12,
                d = Math.floor(s / f);
            s % f > 0 && (d += 1);
            var p = 100 / d - 2,
                m = u * d;
            1 == d && (p = 20);
            var b = '<div style="float:left; min-width: ' + p + '%; " >',
                _ = "</div>",
                x = t.attr("id") + o,
                k = "chkBtnOpen" + x,
                y = x + "-flt-toggle";
            for (i += '<button id="' + k + '" class="checkbox_filter btn btn-default" > ' + c + "</button>", i += '<div id="' + y + '" title="' + g + '" rel="' + h + '" class="toggle-check ui-widget-content ui-corner-all"  style="width: ' + m + '%; " >', i += b, r = 0; s > r; r++) {
                r % f == 0 && 0 != r && (i += _ + b);
                var w = a[r],
                    T = a[r];
                "object" == typeof a[r] && (w = a[r].label, T = a[r].value), i += '<input class="search_init checkbox_filter btn btn-default" type="checkbox" id= "' + x + "_cb_" + T + '" name= "' + o + '" value="' + T + '" >' + w + "<br/>";
                var C = e(i);
                v.html(C), v.wrapInner('<span class="filter_column filter_checkbox" />'), C.change(function() {
                    var n = "",
                        a = "|",
                        r = e('input:checkbox[name="' + o + '"]:checked').size();
                    e('input:checkbox[name="' + o + '"]:checked').each(function(t) {
                        (0 == t && 1 == r || 0 != t && t == r - 1) && (a = ""), n = n.replace(/^\s+|\s+$/g, ""), n = n + e(this).val() + a, a = "|"
                    }), "" != n ? e('input:checkbox[name="' + o + '"]').removeClass("search_init") : e('input:checkbox[name="' + o + '"]').addClass("search_init"), t.fnFilter(n, l, !0, !1), S()
                })
            }
            e("#" + k).button(), e("#" + y).dialog({
                autoOpen: !1,
                hide: "blind",
                buttons: [{
                    text: "Reset",
                    click: function() {
                        return e('input:checkbox[name="' + o + '"]:checked').each(function(t) {
                            e(this).attr("checked", !1), e(this).addClass("search_init")
                        }), t.fnFilter("", l, !0, !1), S(), !1
                    }
                }, {
                    text: "Close",
                    click: function() {
                        e(this).dialog("close")
                    }
                }]
            }), e("#" + k).click(function() {
                e("#" + y).dialog("open");
                var t = e(this);
                return e("#" + y).dialog("widget").position({
                    my: "top",
                    at: "bottom",
                    of: t
                }), !1
            });
            var j = S;
            S = function() {
                var t = e("#" + k);
                e("#" + y).dialog("widget").position({
                    my: "top",
                    at: "bottom",
                    of: t
                }), j()
            }
        }

        function d(e) {
            switch (e) {
                case 0:
                    return m.substring(0, m.indexOf("{from}"));
                case 1:
                    return m.substring(m.indexOf("{from}") + 6, m.indexOf("{to}"));
                default:
                    return m.substring(m.indexOf("{to}") + 4)
            }
        }
        var p, h, g, v, m = "From {from} to {to}",
            b = new Array,
            _ = new Array,
            S = function() {},
            x = this,
            y = {
                sPlaceHolder: "foot",
                sRangeSeparator: "~",
                iFilteringDelay: 500,
                aoColumns: null,
                sRangeFormat: "From {from} to {to}",
                sDateFromToken: "from",
                sDateToToken: "to"
            },
            F = e.extend(y, t);
        return this.each(function() {
            if (x.fnSettings().oFeatures.bFilter) {
                p = new Array;
                var t = x.fnSettings().aoFooter[0],
                    n = x.fnSettings().nTFoot,
                    a = "tr";
                if ("head:after" == F.sPlaceHolder) {
                    var s = e("tr:first", x.fnSettings().nTHead).detach();
                    x.fnSettings().bSortCellsTop ? (s.prependTo(e(x.fnSettings().nTHead)), t = x.fnSettings().aoHeader[1]) : (s.appendTo(e(x.fnSettings().nTHead)), t = x.fnSettings().aoHeader[0]), a = "tr:last", n = x.fnSettings().nTHead
                } else if ("head:before" == F.sPlaceHolder) {
                    if (x.fnSettings().bSortCellsTop) {
                        var s = e("tr:first", x.fnSettings().nTHead).detach();
                        s.appendTo(e(x.fnSettings().nTHead)), t = x.fnSettings().aoHeader[1]
                    } else t = x.fnSettings().aoHeader[0];
                    a = "tr:first", n = x.fnSettings().nTHead
                }
                for (e(t).each(function(t) {
                    h = t;
                    var n = {
                        type: "text",
                        bRegex: !1,
                        bSmart: !0,
                        iMaxLenght: -1,
                        iFilterLength: 0
                    };
                    if (null != F.aoColumns) {
                        if (F.aoColumns.length < h || null == F.aoColumns[h]) return;
                        n = F.aoColumns[h]
                    }
                    if (g = e(e(this)[0].cell).text(), null == n.sSelector ? v = e(e(this)[0].cell) : (v = e(n.sSelector), 0 == v.length && (v = e(e(this)[0].cell))), null != n) switch (m = null != n.sRangeFormat ? n.sRangeFormat : F.sRangeFormat, n.type) {
                        case "null":
                            break;
                        case "number":
                            r(x, !0, !1, !0, n.iFilterLength, n.iMaxLenght);
                            break;
                        case "select":
                            1 != n.bRegex && (n.bRegex = !1), o(x, n.values, n.bRegex, n.selected, n.multiple);
                            break;
                        case "number-range":
                            l(x);
                            break;
                        case "date-range":
                            i(x);
                            break;
                        case "checkbox":
                            f(x, n.values);
                            break;
                        case "twitter-dropdown":
                        case "dropdown":
                            u(n.values);
                            break;
                        case "text":
                        default:
                            bRegex = null == n.bRegex ? !1 : n.bRegex, bSmart = null == n.bSmart ? !1 : n.bSmart, r(x, bRegex, bSmart, !1, n.iFilterLength, n.iMaxLenght)
                    }
                }), j = 0; j < _.length; j++) {
                    var c = function() {
                        var t = x.attr("id");
                        return e("#" + t + "_range_from_" + _[j]).val() + F.sRangeSeparator + e("#" + t + "_range_to_" + _[j]).val()
                    };
                    b.push(c)
                }
                if (x.api().settings().context[0].bAjaxDataGet) {
                    var d = x.api().settings().ajax;
                    x.api().settings().ajax = function(t, n, a) {
                        for (j = 0; j < _.length; j++)
                            for (_[j], k = 0; k < t.length; k++) t[k][searchable] === !0 && (t[k][search][value] = b[j]());
                        if (null != d) try {
                            d(t, n, a, x.fnSettings())
                        } catch (r) {
                            d(t, n, a)
                        } else e.getJSON(sSource, aoData, function(e) {
                            fnCallback(e)
                        })
                    }
                }
            }
        })
    }

}(jQuery);