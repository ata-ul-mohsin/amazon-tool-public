<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Validator;
use Session;
use Illuminate\Support\Facades\DB;
class UserAmazonSetting extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected static $marketplace_validation_rules =
        array(
            'region'        => "required",
            'account_name'  => "required",
            "access_key_id" => "required",
            "secret_key"    => "required",
            "seller_id"     => "required",
            "setting_type"  => "required",
            "profile_id"    => "required"
        );

    protected static $prodadv_validation_rules =
        array(
            'region'        => "required",
            'account_name'  => "required",
            "access_key_id" => "required",
            "secret_key"    => "required",
            "associate_tag" => "required",
            "setting_type"  => "required",
            "profile_id"    => "required"

        );

    private static $validator;
    public static function validateSetting($data = array(), $type)
    {
        if(!empty($type))
        {
            if(strcmp($type,"prod_adv")==0)
            {
                $rules = UserAmazonSetting::$prodadv_validation_rules;
            }
            else if(strcmp($type,"marketplace")==0)
            {
                $rules = UserAmazonSetting::$marketplace_validation_rules;
            }
            if(!empty($rules))
            {
                $validator = Validator::make($data, $rules);
                self::$validator = $validator;
                return $validator->passes();
            }
        }
        return FALSE;
    }
    public static function saveToDb($data = array(), $type="")
    {
        $result = array("status" => '3', "message" => " Error in saving data! ");

        $user   =   Session::get('user');
        $profile_id =   $user['profile_id'];

        if(!empty($type) &&  empty($data['user_setting_id']))
        {
            $single_setting =    UserAmazonSetting::where(array("profile_id"=>$profile_id,"setting_type"=>$type))->get();
            if(!$single_setting->isEmpty())
            {
                $result['message']  =   "Only one account allowed for the time being. Please try again later to add additional accounts.";
                return $result;
            }
        }

        if (!empty($data) && !empty($type))
        {

            $setting_where      =   array();
            if(!empty($data['account_name']))
                $setting_where['account_name']    =   $data['account_name'];
            if(!empty($data['access_key_id']))
                $setting_where['access_key_id']   =   $data['access_key_id'];
            if(!empty($data['secret_key']))
                $setting_where['secret_key']      =    $data['secret_key'];
            if(!empty($data['seller_id']))
                $setting_where['seller_id']       =   $data['seller_id'];


            $setting_exists =   UserAmazonSetting::where(function($query) use($setting_where){

                                                    foreach($setting_where as $key=>$or_where)
                                                    {
                                                        $query->orWhere($key,"=",$or_where);
                                                    }
                                                })
                                                ->where("setting_type","=",$type);
            if(!empty($data['user_setting_id']))
                $setting_exists     =       $setting_exists->where("id","<>",$data['user_setting_id']);

            $setting_exists     =       $setting_exists->get();

            if(!$setting_exists->isEmpty())
            {
                $result['message'] =    "Credentials already in the system. Please try some different credentials. ";
                return $result;
            }
            $oldLogo = "";
            if (UserAmazonSetting::validateSetting($data,$type))
            {

                if(!empty($data['user_setting_id']))
                {
                    $where = array(
                        'id'            => $data['user_setting_id'],
                        'profile_id'    => $data['profile_id'],
                        'setting_type'  => $type
                    );

                    $setting = UserAmazonSetting::where($where)->get();

                    if ($setting->isEmpty())
                    {
                        $result['message']  =   " Setting not found in the system. ";
                        return $result;
                    }
                    $setting    =   $setting->first();
                }
                else
                {
                    $setting = new UserAmazonSetting();
                    $setting->created_at = date("Y-m-d h:i:s");
                }

                $setting->updated_at = date("Y-m-d h:i:s");

                if (isset($data['region']))
                   $setting->region = $data['region'];

                if (isset($data['account_name']))
                   $setting->account_name = $data['account_name'];

                if (isset($data['access_key_id']))
                   $setting->access_key_id = $data['access_key_id'];

                if (isset($data['secret_key']))
                    $setting->secret_key = $data['secret_key'];

                if (isset($data['profile_id']))
                    $setting->profile_id = $data['profile_id'];

                if(strcmp($type,"prod_adv")==0)
                {
                    $setting->setting_type = "prod_adv";
                    if (isset($data['associate_tag']))
                        $setting->associate_tag = $data['associate_tag'];
                }
                else if(strcmp($type,"marketplace")==0)
                {
                    if (isset($data['seller_id']))
                        $setting->seller_id = $data['seller_id'];
                    $setting->setting_type = "marketplace";
                }
                if ($setting->save())
                {
                    $result ["status"] = '1';
                    $result ["message"] = " Successfully saved to database. ";
                }

            }
            else
            {
                $result['validator'] = self::$validator;
                self::$validator = "";
                $result ["status"] = '2';
                $result['message'] = " Validation Failed. ";
            }
            return $result;
        }
    }
    public static function getUserAmazonSettings($type="",$profile_id="")
    {
        $settings   =   UserAmazonSetting::where(array("setting_type"=>$type,"profile_id"=>$profile_id))
            ->with('region')
            ->orderBy("id","desc")
            ->get();

        if($settings)
            $settings = \CommonHelper::objToArray($settings);
        return $settings;
    }
    public static function getUsersDefaultSettingsIds($type="",$profile_id="",$get_full="",$with="")
    {
        $result =   array();
        try
        {
            $result = FALSE;
            if (!empty($type))
                switch ($type)
                {
                    case "marketplace":
                    case "prod_adv":
                        $where  =   array("setting_type" => $type, "is_default" => "TRUE");
                        if(!empty($profile_id))
                            $where["users.profile_id"]  = $profile_id;
                        $where["users.status"]  = "ACTIVE";
                        $result = UserAmazonSetting::where($where)
                            ->join('users', 'users.profile_id', '=', 'user_amazon_settings.profile_id', 'inner');

                        if(!empty($with))
                        {
                            switch($with)
                            {
                                case "request_orders_report":
                                    $result     =   $result->withTodayReportNotFound();
                                    break;
                                case "request_list_report":
                                    $result     =   $result->withTodayRequestedReport();
                                    $result     =   $result->groupBy('user_amazon_settings.id');
                                    break;
                                case "request_get_report":
                                    $result     =   $result->withTodayReportsDone();
                                    $result     =   $result->groupBy('user_amazon_settings.id');
                                    break;
                                default:
                                    break;
                            }
                        }

                    if(empty($get_full))
                            $result = $result->get(["user_amazon_settings.id"]);
                        else
                            $result = $result->get(["user_amazon_settings.*"]);

                    if(empty($with))
                        if ($result)
                            $result = $result->toArray();
                        break;
                    default:
                        break;
                }
        }
        catch(Exception $exp)
        {
            $result =   array();
        }
        return $result;
    }
    public function scopeWithTodayReportNotFound($query)
    {
        $query =   $query->leftJoin('amazon_report_requests', function($join)
        {
            $join->on('amazon_report_requests.setting_id','=',"user_amazon_settings.id");
            $join->on('amazon_report_requests.profile_id','=',"user_amazon_settings.profile_id");
            $join->on('amazon_report_requests.type','=',DB::raw("'_GET_XML_ALL_ORDERS_DATA_BY_ORDER_DATE_'"));
            $join->on(DB::raw('DATE(amazon_report_requests.created_at)'),'=',DB::raw("'".date("Y-m-d")."'"));
        });
        $query =   $query->whereNull('amazon_report_requests.id');
        return $query;
    }
    public function scopeWithTodayRequestedReport($query)
    {
        $query =   $query->join('amazon_report_requests', function($join)
        {
            $join->on('amazon_report_requests.setting_id','=',"user_amazon_settings.id");
            $join->on('amazon_report_requests.profile_id','=',"user_amazon_settings.profile_id");
            $join->on('amazon_report_requests.type','=',DB::raw("'_GET_XML_ALL_ORDERS_DATA_BY_ORDER_DATE_'"));
            $join->on(DB::raw('DATE(amazon_report_requests.created_at)'),'=',DB::raw("'".date("Y-m-d")."'"));
        });
        $query =   $query->addSelect("request_id");
        $query =   $query->addSelect("user_amazon_settings.*");
        $query =   $query->addSelect(DB::raw("amazon_report_requests.id as request"));

        return $query;
    }
    public function scopeWithTodayReportsDone($query)
    {
        $query =   $query->join('amazon_report_requests', function($join)
        {
            $join->on('amazon_report_requests.setting_id','=',"user_amazon_settings.id");
            $join->on('amazon_report_requests.profile_id','=',"user_amazon_settings.profile_id");
            $join->on('amazon_report_requests.processing_status','=',DB::raw("'_DONE_'"));
            $join->on('amazon_report_requests.type','=',DB::raw("'_GET_XML_ALL_ORDERS_DATA_BY_ORDER_DATE_'"));
            $join->on(DB::raw('DATE(amazon_report_requests.created_at)'),'=',DB::raw("'".date("Y-m-d")."'"));
        });
        $query =   $query->addSelect("request_id");
        $query =   $query->addSelect("generated_report_id");
        $query =   $query->addSelect("user_amazon_settings.*");
        $query =   $query->addSelect(DB::raw("amazon_report_requests.id as request"));

        return $query;
    }
    public static function chooseDefaultSetting($setting_id="")
    {
        $user   =   Session::get('user');
        $result =   array("status"=>"2","message"=>" Could not change the status");
        if(!empty($user['profile_id']))
        {
            $setting    =   UserAmazonSetting::where(array("profile_id"=>$user['profile_id'],"id"=>$setting_id))
                            ->get()
                            ->first();
            if($setting)
            {
                $all_settings   =   UserAmazonSetting::where(array("setting_type"=>$setting['setting_type'],"profile_id"=>$user['profile_id']))
                ->get();

                if($all_settings)
                    foreach($all_settings as $current_setting)
                    {
                        $current_setting->is_default    =   "FALSE";
                        $current_setting->save();
                    }

                $setting->is_default =   "TRUE";
                $setting->save();

                $result ["status"]      =   "1";
                $result ["message"]     =   " Default Setting changed";
            }
            else
            {
                $result ["message"]     =   " Setting not found in the system";
            }
        }
        else
        {
            $result ["message"]     =   " Current user not found";
        }
        return $result;
    }
    public static function deleteUserAmazonSetting($type="",$id="")
    {

        try
        {

            if (!empty($type))
                switch ($type)
                {
                    case "marketplace":
                    case "prod_adv":
                        $user_setting = UserAmazonSetting
                            ::where(array("setting_type" => $type, "id" => $id))
                            ->get();

                        if (!$user_setting->isEmpty())
                        {
                            $user_setting = $user_setting->first();
                            $user_setting->delete();
                            return TRUE;
                        }
                        break;
                    default:
                        break;
                }


        }
        catch (\Exception $ex)
        {
        }
        return FALSE;
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'profile_id', 'profile_id');
    }
    public function region()
    {
        return $this->belongsTo('App\Region', 'region', 'id');
    }
    public function order()
    {
        return $this->hasMany('App\Marketplace', 'setting_id', 'id');
    }
    public function reports()
    {
        return $this->hasMany('App\AmazonReportRequest', 'setting_id', 'id');
    }
    public function blacklist_emails()
    {
        return $this->hasMany("App\BlacklistEmail","setting_id","id");
    }
    public static function getSettingForMWSRequest($profile_id,$setting_id)
    {
        $mws_settings = UserAmazonSetting::where(array("profile_id" => $profile_id, "id" => $setting_id))->get();
        if (!$mws_settings->isEmpty())
        {
            $mws_settings = $mws_settings->first();

            if ($mws_settings)
            {
                $mws_settings->user = User::where(array("profile_id" => $mws_settings["profile_id"]))->get();
                $mws_settings->region = Region::where(array("id" => $mws_settings["region"]))->get();
                if ($mws_settings->region)
                {
                    $mws_settings->region = $mws_settings->region->first();

                    $marketplace = Marketplace::where(array("region" => $mws_settings->region->code))->get();

                    if ($marketplace)
                    {
                        $mws_settings->marketplace = $marketplace->first();
                    }
                }
                if ($mws_settings->user)
                {
                    $mws_settings->user = $mws_settings->user->first();
                }
                $mws_settings = \CommonHelper::objToArray($mws_settings);

            }
        }
        return $mws_settings;
    }
}