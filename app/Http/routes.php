<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get("test_queue/{id}",array("uses"=>"TestController@sendReminderEmail"));
//Route::get("local_testing",array("uses"=>"TestController@local_testing"));
//Route::get("just_test",array("uses"=>"TestController@just_test"));
//Route::get("test_multiple_tables",array("uses"=>"TestController@test_multiple_tables"));
Route::get("test_fetch_products",array("uses"=>"TestController@test_fetch_products"));
//Route::get('test_fetch_products',array("uses"=>"TestController@test_fetch_products"));

Route::get("testing",array("uses"=>"TestController@testing"));
Route::get("test-compose-message",array("uses"=>"TestController@test_compose_message"));


Route::match(array("GET","POST"),"user/login",array('uses'=>'UserController@login'));
Route::get("enqueue-fetch-orders",array("uses"=>"JobsController@enqueueFetchOrderJob"));
Route::get("enqueue-send-emails",array("uses"=>"JobsController@enqueueSendEmailsJob"));
Route::get("enqueue-fetch-products",array("uses"=>"JobsController@enqueueFetchProductsJob"));
Route::get("enqueue-fetch-feedbacks",array("uses"=>"JobsController@enqueueFetchFeedbacks"));
Route::get("enqueue-fetch-reviews",array("uses"=>"JobsController@enqueueFetchReviewsAndRatings"));
Route::get("request-orders-reports",array("uses"=>"JobsController@requestOrdersReports"));
Route::get("unsubscribe/{email}",'BlacklistEmailController@unsubscribe');
Route::get("general-message",'BlacklistEmailController@general_message');

//  Route::get("test_feedback",array("uses"=>"TestController@test_feedback"));
//  Route::get("test_brands",array("uses"=>"TestController@assignBrands"));
//Route::get("test_reviews",array("uses"=>"TestController@test_reviews"));





/*
|--------------------------------------------------------------------------
| Basic Authenticated Routes
|--------------------------------------------------------------------------
*/









Route::group(array("middleware"=>'logged_in'),function(){
//

    Route::get("user/choose-setting/{id}",array('uses'=>'UserController@chooseDefaultSetting'));
    Route::get("user/logout",array("uses"=>'UserController@logout'));

    Route::group(array("prefix"=>'opportunityfinder'),function(){
        Route::get('/', array('as'=>'opportunityfinder','uses' => 'OpportunityController@index'));
        Route::match(array('get', 'post'),"find_opportunity",array("as"=>'find_opportunity','uses'=>'OpportunityController@findOpportunity'));
        Route::get("opportunity_ajax",array("as"=>'opportunity_ajax','uses'=>'OpportunityController@opportunityAjax'));
        Route::get("opportunity_cron",array("as"=>'opportunity_cron','uses'=>'OpportunityController@opportunityCron'));
        Route::get("make_asin",array("as"=>'make_asin','uses'=>'OpportunityController@makeAsin'));
        Route::get("populate_products",array("as"=>'populate_products','uses'=>'OpportunityController@populateProducts'));
        Route::get("populate_general_products",array("as"=>'populate_general_products','uses'=>'OpportunityController@populateGeneralProducts'));
        Route::get("get_review_rating",array("as"=>'get_review_rating','uses'=>'OpportunityController@getReviewRating'));
        Route::get("set_price",array("as"=>'set_price','uses'=>'OpportunityController@gePrice'));
        Route::get("saved-search",array('uses'=>'OpportunityController@savedSearch'));
        Route::get("remove_saved_search",array('uses'=>'OpportunityController@removeSavedSearch'));
        Route::match(array('get', 'post'),"scrap_top_sellers",array("as"=>'scrap_top_sellers','uses'=>'OpportunityController@scrap_top_sellers'));
        Route::match(array('get', 'post'),"del_pro",array("as"=>'del_pro','uses'=>'OpportunityController@del_pro'));
    });

    Route::group(array("middleware"=>'check_user_settings'),function()
    {

        Route::get("orders",array("as"=>'orders','uses'=>'BrandController@orders'));

        Route::get("products",array("as"=>"products","uses"=>"UserController@products"));
        Route::get('/',array("uses"=>"UserController@index"));
        Route::resource("brands",'BrandController');
        Route::match(array("GET","POST"),"messages/settings",array("as"=>'settings','uses'=>'MessageController@saveSetting'));
        Route::get("messages/sent",array("as"=>'settings','uses'=>'MessageController@sent'));
        Route::get("messages/outbox",array("as"=>'outbox','uses'=>'MessageController@outbox'));
        Route::get("messages/outbox/delete/{id}",array('uses'=>'MessageController@deleteOutbox'));


        Route::resource("messages",'MessageController');
        Route::resource("variables",'VariableController');
        Route::resource("blacklist-emails",'BlacklistEmailController');


        Route::get("brand-data/{setting_id}",array("uses"=>"BrandController@getBrandData"));
        Route::get("assign-brand",array("uses"=>"BrandController@assignBrands"));
        Route::post("save-brand-assignment",array("uses"=>"BrandController@saveBrandAssignment"));
        Route::match(array("GET","POST"),"product-data",array('as'=>'product-data','uses'=>'BrandController@getProductData'));
        Route::get("feedbacks",array('as'=>'feedbacks','uses'=>'FeedbackController@index'));
        Route::get("feedback-detail/{feedback}",array("as"=>"feedback-detail","uses"=>"FeedbackController@show"));
        Route::match(array("GET","POST"),"feedbacks-data",array('as'=>'feedbacks-data','uses'=>'FeedbackController@getData'));
        Route::post("feedbacks/update/{feedback}",array('as'=>'feedbacks/update','uses'=>'FeedbackController@update'));

        Route::get("track-product",array("as"=>"track-product","uses"=>"BrandController@trackProduct"));
        Route::post("track-product",array("as"=>"track-product","uses"=>"BrandController@trackAndSaveProduct"));
        Route::get("list-brand-data",array("uses"=>"BrandController@getListBrandData"));
        Route::get("list-order-data",array("uses"=>"BrandController@getListOrderData"));
        Route::get("report_testing",array("uses"=>"TestController@report_testing"));
        Route::get("report_list_testing",array("uses"=>"TestController@report_list_testing"));
        Route::get("get_report_testing",array("uses"=>"TestController@get_report_testing"));





        /*
        |--------------------------------------------------------------------------
        | User Routes
        |--------------------------------------------------------------------------
        */
        Route::group(array("prefix"=>"user"),function(){

            Route::get("dashboard",array('uses'=>'UserController@index'));
            Route::get("rating-data",array("as"=>"rating-data","uses"=>"UserController@getProductData"));

            /*
             *  User MarketPlace Setting routes
             */
            Route::get("add-marketplace-setting",array('uses'=>'UserController@saveMarketplaceSetting'));
            Route::get("edit-marketplace-setting/{id}",array('uses'=>'UserController@saveMarketplaceSetting'));
            Route::post("save-marketplace-setting",array('uses'=>'UserController@saveMarketplaceSetting'));
            Route::get("marketplace-settings",array('uses'=>'UserController@marketplaceSettings'));
            Route::get("delete-marketplace-setting/{id}",array('uses'=>'UserController@deleteMarketplaceSetting'));
            /*
             *  User Product Advertising Setting routes
             */
            Route::get("add-aws-setting",array('uses'=>'UserController@saveProdAdvSetting'));
            Route::get("edit-aws-setting/{id}",array('uses'=>'UserController@saveProdAdvSetting'));
            Route::post("save-product-advertising-setting",array('uses'=>'UserController@saveProdAdvSetting'));
            Route::get("aws-settings",array('uses'=>'UserController@prodAdvSettings'));
            Route::get("delete-product-advertising-setting/{id}",array('uses'=>'UserController@deleteProdAdvSetting'));
        });




    });



});
