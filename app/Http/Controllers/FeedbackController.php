<?php

namespace App\Http\Controllers;

use App\Marketplace;
use App\UserAmazonSetting;
use Illuminate\Http\Request;
use View;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\AmazonFeedback;
use Session;
use App\Http\Requests\FeedbackRequest;
use Illuminate\Support\Facades\Redirect;


class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $marketplaces   =   Marketplace::all();
        $user   =   Session::get('user');
        $profile_id =   $user['profile_id'];
        $settings       =   UserAmazonSetting::getUserAmazonSettings("marketplace",$profile_id);


        $setting_str    =   self::build_setting_str($settings);

        return View::make("feedbacks.index",compact("marketplaces","setting_str"));



    }
    private static function build_setting_str(array $settings)
    {
        $setting_str        =   "";
        foreach($settings as $seting)
        {
            $setting_str   .=  "'".$seting['id'].":::".$seting['account_name']." (".$seting['seller_id'].")',";
        }
        if(!empty($setting_str))
            $setting_str        =   substr($setting_str,0,-1);
        return $setting_str;
    }
    public function getData(Request $request)
    {
        $response           =   array();

        $params             =   $request->input();
        $user               =   Session::get('user');
        $feedbacks          =   AmazonFeedback::getFeedbacks($user['profile_id'],$params);


        $response['recordsTotal']   =   $feedbacks['total'];
        unset($feedbacks['total']);

        $response['recordsFiltered']   =   $feedbacks['filtered'];
        unset($feedbacks['filtered']);


        $response['draw']   =   $params['draw'];

         $response['data']   =   $feedbacks;


        return json_encode($response,true);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(FeedbackRequest $request,AmazonFeedback $feedback)
    {
        return View::make("feedbacks.form",compact("feedback"));
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(FeedbackRequest $request, AmazonFeedback $feedback)
    {
        try
        {
            $msg    =   "";
            $user   =   Session::get("user");
            $table  =   "amazon_feedbacks_".$user['profile_id'];

            $feedback->setTable($table);
            $data   =   $request->input();
            $feedback->status   =   $data['status'];

            $feedback->save();

            return Redirect::route('feedbacks')->with('success_message', "Status updated");

        }
        catch(\Exception $exp)
        {
            $msg    =   $exp->getMessage();
        }

        return Redirect::route('feedbacks')->with('error_message', "Status could not be updated. \n".$msg);
    }


}
