<?php

namespace App\Http\Controllers;

use App\Jobs\getProducts;
use App\Jobs\getTopProducts;
use App\Jobs\getReviewLinks;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use App\AmazonCategory;
use App\FindOpportunity;
use App\AmazonRequest;
use Sunra\PhpSimple\HtmlDomParser;
use View;
use Session;

class OpportunityController extends Controller {

    public $marketPlaces = array('webservices.amazon.com', 'webservices.amazon.co.uk'); // Please also change in FindOpportunity::productsToDB

    public function __construct() {
        
    }

    public function index() {

        //$link = AmazonRequest::getReviewLink('B00U38Z18A', 'webservices.amazon.co.uk');

        $categories = AmazonCategory::orderBy("name", "asc")->orderBy("marketplace", "asc")
                ->get();
        if ($categories) {
            $categories = json_encode($categories);
            $categories = json_decode($categories, true);
        }
        return View::make('opportunityfinder.products_list', compact('categories'));
    }
    
    public function findOpportunity() {

        $data = array();
        if (Request::isMethod('post')) {
            $inputs = Input::all();
            $results = FindOpportunity::getList($inputs);
        } else {
            return redirect('opportunityfinder');
        }
        return View::make('opportunityfinder.search_products', compact('inputs'));
    }

    public function opportunityAjax() {
        $inputs = Input::all();
        $result = FindOpportunity::makeQueryResults($inputs);
        return $result;
    }

    public function makeAsin() {
        set_time_limit(0);

        //foreach ($this->marketPlaces as $marketPlace) {
        $marketPlace = 'webservices.amazon.co.uk';
        $nodes = \DB::table('browse_nodes_array')->select('browsnode_string', 'browsnode_id')->where('marketplace', '=', $marketPlace)->get();
        foreach ($nodes as $node) {
            sleep(1);
            $parentBrowseNodeId = $node->browsnode_id;

            $params = array(
                "Service" => "AWSECommerceService",
                "Operation" => "BrowseNodeLookup",
                "AWSAccessKeyId" => env('SES_KEY'),
                "AssociateTag" => env('SES_TAG'),
                "BrowseNodeId" => $node->browsnode_string,
                "ResponseGroup" => "TopSellers"
            );

            $res = AmazonRequest::getResult($params, $marketPlace);

            foreach ($res->BrowseNodes->BrowseNode as $BrowseNode) {
                $childBrowseNodeId = $BrowseNode->BrowseNodeId;
                $i = 1;
                $ar = array();
                $str = '';

                if (isset($BrowseNode->TopSellers->TopSeller)) {
                    foreach ($BrowseNode->TopSellers->TopSeller as $TopSeller) {
                        $str .= $TopSeller->ASIN . ',';
                        $i++;
                    }
                    $str = rtrim($str, ",");
                    $data = array(
                        'asin_string' => $str,
                        'browsnode_id' => $parentBrowseNodeId,
                        'child_browsnode_id' => $childBrowseNodeId,
                        'marketplace' => $marketPlace
                    );
                    \DB::table('browse_nodes_asins')->where('browsnode_id', '=', $parentBrowseNodeId)->where('child_browsnode_id', '=', $childBrowseNodeId)->where('marketplace', '=', $marketPlace)->delete();
                    \DB::table('browse_nodes_asins')->insert($data);
                }
            }
        }
    }

    public function test() {
        
        
        
//        set_time_limit(0);
//        $i = 1;
//        $data = array();
//        $data['top_seller'] = 1;
//
//        // foreach ($this->marketPlaces as $marketPlace) {
//        $asins = \DB::table('browse_nodes_asins')->select('asin_string', 'browsnode_id', 'child_browsnode_id')->where('id', '=', 876)->get();
////print_r($asins);
//        $asinAr = array();
//        $marketPlace = 'webservices.amazon.co.uk';
//        foreach ($asins as $asin) {
//
//            $asinAr[] = array('browsnode_id' => $asin->browsnode_id, 'child_browsnode_id' => $asin->child_browsnode_id, 'asin_string' => $asin->asin_string);
//
//            print_r($asinAr);
//
//            $this->dispatch_top_product_job($asinAr, $marketPlace, $data);
//            $asinAr = array();
//
//
//            $i++;
//        }
//        //}
    }

    public function populateProducts() {
        set_time_limit(0);
        $i = 1;
        $data = array();
        $data['top_seller'] = 1;

        foreach ($this->marketPlaces as $marketPlace) {
            $asins = \DB::table('browse_nodes_asins')->select('asin_string', 'browsnode_id', 'child_browsnode_id')->where('marketplace', '=', $marketPlace)->get();

            $asinAr = array();
            foreach ($asins as $asin) {
                $asinAr[] = array('browsnode_id' => $asin->browsnode_id, 'child_browsnode_id' => $asin->child_browsnode_id, 'asin_string' => $asin->asin_string);
                if ($i % 5 == 0) {
                    $this->dispatch_top_product_job($asinAr, $marketPlace, $data);
                    $asinAr = array();
                }
                $i++;
            }
        }

        echo 'Queue completed';

//        $records = \DB::table('find_opportunity')->select('id', 'reviews_link')->get();
//        $this->populateReviewsRating($records);
    }
    
    public function PopulateReviewLink() {
        set_time_limit(0);
        $i = 1;
        $data = array();
        $data['top_seller'] = 1;

        foreach ($this->marketPlaces as $marketPlace) {
            $asins = \DB::table('browse_nodes_asins')->select('asin_string', 'browsnode_id', 'child_browsnode_id')->where('marketplace', '=', $marketPlace)->get();

            $asinAr = array();
            foreach ($asins as $asin) {
                $asinAr[] = array('browsnode_id' => $asin->browsnode_id, 'child_browsnode_id' => $asin->child_browsnode_id, 'asin_string' => $asin->asin_string);
                if ($i % 5 == 0) {
                    $this->dispatch_review_links($asinAr, $marketPlace, $data);
                    $asinAr = array();
                }
                $i++;
            }
        }

        echo 'Queue completed';
    }

    public function populateGeneralProducts() {
        set_time_limit(0);

        foreach ($this->marketPlaces as $marketPlace) {
            $nodes = \DB::table('browse_nodes')
                            ->join('amazon_categories', 'amazon_categories.browse_nodes', '=', 'browse_nodes.browsnode_id')
                            ->select('browse_nodes.browsnode_id', 'browse_nodes.child_browsnode_id', 'amazon_categories.search_index')->where('browse_nodes.marketplace', '=', $marketPlace)->get();

            foreach ($nodes as $node) {
                $data = array();
                $data['top_seller'] = 0;
                $this->dispatch_product_job($node, $marketPlace, $data);
            }
        }

//        $records = \DB::table('find_opportunity')->select('id', 'reviews_link')->whereNull('reviews')->get();
//        $this->populateReviewsRating($records);
    }

    private function dispatch_product_job($node, $marketPlace, $data) {

        $product_queue = \Config::get("queue.connections.productqueue");
        if (!empty($product_queue['queue'])) {
            $product_job = (new getProducts($node, $marketPlace, $data));
            if ($product_job) {
                $product_job = $product_job->onQueue($product_queue['queue']);
                $this->dispatch($product_job);
            }
        }
    }

    private function dispatch_top_product_job($asin, $marketPlace, $data) {

        $product_queue = \Config::get("queue.connections.topproductqueue");
        if (!empty($product_queue['queue'])) {
            $product_job = (new getTopProducts($asin, $marketPlace, $data));
            if ($product_job) {
                $product_job = $product_job->onQueue($product_queue['queue']);
                $this->dispatch($product_job);
            }
        }
    }
    
    
    
    private function dispatch_review_links($asin, $marketPlace, $data) {

        $product_queue = \Config::get("queue.connections.getreviewlinks");
        if (!empty($product_queue['queue'])) {
            $product_job = (new getReviewLinks($asin, $marketPlace, $data));
            if ($product_job) {
                $product_job = $product_job->onQueue($product_queue['queue']);
                $this->dispatch($product_job);
            }
        }
    }

    function getReviewRating() {
        //$records = \DB::table('find_opportunity')->select('id', 'reviews_link')->where('rated', '=', 0)->get();
        $this->loadReviewRating();
    }

    function loadReviewRating() {
        set_time_limit(0);
        $records = \DB::table('find_opportunity')->select('id', 'reviews_link')->where('rated', '=', 0)->get();
        print_r($records);exit;
        try {
            $this->populateReviewsRating($records);
        } catch (\Exception $exp) {
            echo 'Exception';
//            $res = FindOpportunity::find($records[0]->id);
//            $res->reviews = "NA";
//            $res->rating = "NA";
//            $res->rated = 1;
//            $res->save();
            //$this->loadReviewRating();
        }
    }

    function populateReviewsRating($records) {
        foreach ($records as $record) {

            if ($record->reviews_link) {
                $url = base64_encode($record->reviews_link);
                $review_rating = $this->getRatingReviews($url);
                $row = FindOpportunity::find($record->id);
                $row->reviews = $review_rating['reviews'];
                $row->rating = $review_rating['rating'];
                $row->rated = 1;
                $row->save();
            }
        }
    }

    public function getRatingReviews($url = '') {
        $url = base64_decode($url);
        $rating = 'NA';
        $reviews = 'NA';
        $html = HtmlDomParser::file_get_html($url);

        $res = $html->find(".crIFrameHeaderHistogram", 0);
        if (isset($res)) {
            $reviews = explode(' ', $html->find(".crIFrameHeaderHistogram", 0)->childNodes(0)->childNodes(0)->innertext)[0];
            $reviews = str_replace(',', '', $reviews);
        }
        foreach ($html->find('.asinReviewsSummary img') as $element) {
            if (isset($element->title)) {
                $rating = explode(' ', $element->title)[0];
            }
        }

        return array('reviews' => $reviews, 'rating' => $rating);
    }

    function gePrice() {
        set_time_limit(0);
        $records = \DB::table('find_opportunity')->select('id', 'amazon_link')->where('price', '=', 0)->where('marketplace', '=', 0)->get();
//        echo '<pre>';
//        print_r($records);
//        exit;
        $this->populatePricw($records);
    }

    function populatePricw($records) {

        foreach ($records as $record) {

            if ($record->amazon_link) {
                $url = base64_encode($record->amazon_link);

                $price = $this->geRenderedPrice($url);
                //echo $record->amazon_link . "  -- " . $price . " -- " . $record->id . "<br>";
                $row = FindOpportunity::find($record->id);
                if ($price == 'unavailable') {
                    $row->delete();
                } else {
                    $row->price = $price;
                    $row->save();
                }
            }
        }
    }

    public function geRenderedPrice($url = '') {
        $url = base64_decode($url);
        //$url = 'http://www.amazon.co.uk/Boundary-Crossed-Magic-Book-ebook/dp/B00O4FK872%3FSubscriptionId%3D1PBWAFQ5FMA8911KRY02%26tag%3Dpbmissionscom-20%26linkCode%3Dxm2%26camp%3D2025%26creative%3D165953%26creativeASIN%3DB00O4FK872';


        $html = HtmlDomParser::file_get_html($url);

        $price = 0;
        if ($html) {
            if ($html->find(".swatchElement", 0)) {
                $str = $html->find(".swatchElement", 0)->childNodes(0)->childNodes(0)->childNodes(0)->childNodes(0)->childNodes(2)->innertext;
                $str = strip_tags($str);
                $price = trim(str_replace(array('from', '$', 'Â£', '£'), '', $str));
                //echo $url . "  -- ". $price . "<br>";
            } elseif ($html->find("#priceblock_ourprice", 0)) {
                $str = $html->find("#priceblock_ourprice", 0)->innertext;
                $str = strip_tags($str);
                $price = trim(str_replace(array('from', '$', 'Â£', '£'), '', $str));
                if (strpos($price, ' - ')) {
                    $price = explode(' - ', $price)[0];
                }
            } elseif ($html->find("#availability", 0)) {
                $price = 'unavailable';
            } elseif ($html->find("#a-autoid-2-announce", 0)) {
                //$price = 'unavailable';
                //echo $price;
            }
        }

        //echo $price;
        //exit;
        return $price;
    }


   

}
