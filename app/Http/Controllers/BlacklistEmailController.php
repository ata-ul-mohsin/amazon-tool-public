<?php

namespace App\Http\Controllers;

use App\BlacklistEmail;
use App\UserAmazonSetting;
use App\Http\Requests\BlacklistEmailRequest;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;

class BlacklistEmailController extends Controller
{
    private $amazon_settings;
    private $user;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $user                   =   Session::get('user');
        $amazon_settings        =   UserAmazonSetting::where(array("profile_id"=>$user['profile_id']))->lists("account_name","id");
        $this->user             =   $user;
        if($amazon_settings)
            $amazon_settings    =   $amazon_settings->toArray();
        else
            $amazon_settings    =   array();

        $this->amazon_settings  =   $amazon_settings;
    }
    public function index()
    {
        $blacklist_emails =   BlacklistEmail::getBlacklistEmails($this->user['profile_id']);
        return View::make("blacklistemails.index", array("blacklist_emails"=>$blacklist_emails,"amazon_settings"=>$this->amazon_settings));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make("blacklistemails.form")->with(array("amazon_settings"=>$this->amazon_settings));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlacklistEmailRequest $request)
    {
        $data = \Input::all();
        $data['profile_id'] =   $this->user['profile_id'];

        $result = BlacklistEmail::saveToDb($data);

        if ($result['status'] == '1')
        {
            if(isset($data['save_add']))
                return Redirect::route('blacklist-emails.create')->with('success_message', $result['message']);
            else
                return Redirect::route('blacklist-emails.index')->with('success_message', $result['message']);
        }
        else if ($result['status'] == '3')
        {
            return Redirect::route('blacklist-emails.index')->with('error_message',$result['message']);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(BlacklistEmail $blacklist_email)
    {
        return View::make("blacklistemails.form", array("blacklist_email"=>$blacklist_email,"amazon_settings"=>$this->amazon_settings));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BlacklistEmailRequest $request, $id)
    {
        $data = \Input::all();
        $data['profile_id'] =   $this->user['profile_id'];
        $result           =   BlacklistEmail::saveToDb($data);

        if ($result['status'] == '1')
        {
            if(isset($data['save_add']))
                return Redirect::route('blacklist-emails.create')->with('success_message', $result['message']);
            else
                return Redirect::route('blacklist-emails.index')->with('success_message', $result['message']);
        }
        else if ($result['status'] == '3')
        {
            return Redirect::route('blacklist-emails.index')->with('error_message',$result['message']);

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(BlacklistEmailRequest $request, BlacklistEmail $bl_email)
    {
        $is_deleted =   BlacklistEmail::deleteBlackListEmail($bl_email);
        if ($is_deleted)
        {
            $message    =" Email removed from BlackList of Corresponding Account. ";
            Session::flash("success_message",$message);
        }
        else
        {
            $message    =" Email could not be removed from BlackList of Corresponding Account. ";
            Session::flash("error_message",$message);
        }
        return url("blacklist-emails");
    }

    public function unsubscribe($email)
    {
        if(!empty($email))
        {
            if(filter_var($email,FILTER_VALIDATE_EMAIL))
            {
                $already_unsubscribed   =   BlacklistEmail::where(array("email"=>$email,"setting_id"=>0))->get();
                if(!$already_unsubscribed->isEmpty())
                {
                    return redirect("general-message")->with("error_message","Already Unsubscribed");
                }
                $bl_email               =   new BlacklistEmail();
                $bl_email->email        =   $email;
                $bl_email->created_at   =   date("Y-m-d h:i:s");
                $bl_email->updated_at   =   date("Y-m-d h:i:s");
                $bl_email->setting_id   =   "0";
                $bl_email->save();
                return redirect("general-message")->with("success_message","Successfully Unsubscribed");
            }
            return redirect("general-message")->with("error_message","Invalid email");
        }
        return redirect("general-message")->with("error_message","Invalid request");
    }
    public function general_message()
    {
        echo Session::get("error_message");echo "<br/><br/>";
        echo Session::get("success_message");
    }
}
