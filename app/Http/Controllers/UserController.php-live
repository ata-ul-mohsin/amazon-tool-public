<?php

namespace App\Http\Controllers;

use App\AmazonFeedback;
use App\Providers\AppServiceProvider;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;

use App\Jobs\FetchAmazonOrders;
use App\User;
use App\UserAmazonSetting;
use App\Region;
use View;
use Session;
use Sunra\PhpSimple\HtmlDomParser;
use App\Message;
use App\AmazonOrder;
use App\AmazonProduct;
use App\Brand;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

class UserController extends Controller
{

    public function __construct()
    {


    }



    public function getProductData()
    {
        try
        {
            $response = array();
            $params = Input::all();

            $products = AmazonProduct::getProductsForDashboard($params);

            $response['recordsTotal'] = $products['total'];
            unset($products['total']);

            $response['recordsFiltered'] = $products['filtered'];
            unset($products['filtered']);

            $response['draw'] = $params['draw'];

            $response['data'] = $products;

        }
        catch(Exception $exp)
        {
            echo $exp->getMessage();exit;
        }
        return json_encode($response,true);
    }

    public function products()
    {
        return View::make('products.index');
    }

    public function index()
    {

        $data['seven_days_orders'] = AmazonOrder::getOrdersCount(7);
        $data['one_month_orders'] = AmazonOrder::getOrdersCount(30);
        $data['three_month_orders'] = AmazonOrder::getOrdersCount(90);
        $data['full_year_orders'] = AmazonOrder::getOrdersCount(365);


        $data['seven_days_feedbacks'] = AmazonFeedback::getFeedbacksCount(7);
        $data['one_month_feedbacks'] = AmazonFeedback::getFeedbacksCount(30);
        $data['three_month_feedbacks'] = AmazonFeedback::getFeedbacksCount(90);
        $data['full_year_feedbacks'] = AmazonFeedback::getFeedbacksCount(365);


        $data['seven_days_negative_feedbacks'] = AmazonFeedback::getFeedbacksCount(7, "TRUE", "NEGATIVE");
        $data['one_month_negative_feedbacks'] = AmazonFeedback::getFeedbacksCount(30, "TRUE", "NEGATIVE");
        $data['three_month_negative_feedbacks'] = AmazonFeedback::getFeedbacksCount(90, "TRUE", "NEGATIVE");
        $data['full_year_negative_feedbacks'] = AmazonFeedback::getFeedbacksCount(365, "TRUE", "NEGATIVE");


        $data['seven_days_positive_feedbacks'] = AmazonFeedback::getFeedbacksCount(7, "TRUE", "POSITIVE");
        $data['one_month_positive_feedbacks'] = AmazonFeedback::getFeedbacksCount(30, "TRUE", "POSITIVE");
        $data['three_month_positive_feedbacks'] = AmazonFeedback::getFeedbacksCount(90, "TRUE", "POSITIVE");
        $data['full_year_positive_feedbacks'] = AmazonFeedback::getFeedbacksCount(365, "TRUE", "POSITIVE");

        $products   =   AmazonProduct::getProductsForDashboard();
        $response['recordsTotal'] = $products['total'];
        unset($products['total']);

        $response['recordsFiltered'] = $products['filtered'];
        unset($products['filtered']);

        $data['products_ratings'] = $products;

        $user           =   Session::get("user");
        $profile_id     =   $user['profile_id'];

        return View::make('users.index', compact("data"));
    }

    public function login()
    {
        $is_logged = Session::get("user");

        if (!empty($is_logged['is_logged']))
            return Redirect::to('/')->with('error_message', 'You are already logged in the system');

        if (Request::isMethod('post'))
        {
            $result = User::loginUser(Input::all());

            if ($result['status'] == '1')
            {
                return Redirect::to('/')->with('success_message', $result['message']);
            }
            else if ($result['status'] == '2')
            {
                return Redirect::back()->withErrors($result['validator']);
            }
            else if ($result['status'] == '3')
            {
                return Redirect::to('user/login')->with('error_message', $result['message']);
            }
        }
        else
        {
            return View::make('users.form');
        }
    }

    public function logout()
    {
        Session::forget("user");
        Session::flush();
        return Redirect::to('user/login')->with('success_message', 'You are successfully logged out.');
    }



    public function saveMarketplaceSetting($update_id = "")
    {
        $user = Session::get("user");
        $regions = Region::orderBy("name", "ASC")->get();
        if ($regions)
            $regions = \CommonHelper::objToArray($regions);
        if (Request::isMethod('post'))
        {
            $data = Input::all();
            $data['profile_id'] = $user['profile_id'];
            $data['setting_type'] = 'marketplace';
            $result = UserAmazonSetting::saveToDb($data, "marketplace");

            if ($result['status'] == '1')
            {
                return Redirect::to('user/marketplace-settings')->with('success_message', $result['message']);
            }
            else if ($result['status'] == '2')
            {
                return Redirect::back()
                    ->withInput(Input::all())
                    ->withErrors($result['validator']);
            }
            else if ($result['status'] == '3')
            {
                return Redirect::to('user/marketplace-settings')
                    ->with('error_message', $result['message'])
                    ->withInput(Input::all());
            }
        }
        else if ($update_id)
        {
            if (is_numeric($update_id) && $update_id > 0)
            {
                $amazon_setting = UserAmazonSetting::where(array("id" => $update_id,
                    "profile_id" => $user['profile_id'],
                    "setting_type" => "marketplace"))
                    ->get();
                if (!$amazon_setting->isEmpty())
                {
                    $amazon_setting = $amazon_setting->first();
                    $amazon_setting = $amazon_setting->toArray();
                    return View::make('users.marketplace_settings_form', array("user_setting" => $amazon_setting, 'regions' => $regions));
                }
                else
                {
                    return Redirect::to('user/marketplace-settings')->with('error_message', 'Setting not found in system!');
                }
            }
            else
            {
                return Redirect::to('user/marketplace-settings')->with('error_message', ' Invalid request!');
            }
        }
        else
        {
            $profile_id = $user['profile_id'];
            $single_setting = UserAmazonSetting::where(array("profile_id" => $profile_id, "setting_type" => "marketplace"))->get();
            if (!$single_setting->isEmpty())
            {
                return Redirect::to('user/marketplace-settings')->with('error_message', "Only one account allowed for the time being. Please try again later to add additional accounts.");
            }

            $amazon_setting = new UserAmazonSetting();
            return View::make('users.marketplace_settings_form', array("user_setting" => $amazon_setting, 'regions' => $regions));
        }
    }

    public function saveProdAdvSetting($update_id = "")
    {
        $user = Session::get("user");
        $regions = Region::orderBy("name", "ASC")->get();
        if ($regions)
            $regions = \CommonHelper::objToArray($regions);
        if (Request::isMethod('post'))
        {
            $data = Input::all();
            $data['profile_id'] = $user['profile_id'];
            $data['setting_type'] = 'prod_adv';
            $result = UserAmazonSetting::saveToDb($data, "prod_adv");

            if ($result['status'] == '1')
            {
                return Redirect::to('user/aws-settings')
                    ->with('success_message', $result['message']);
            }
            else if ($result['status'] == '2')
            {
                return Redirect::back()
                    ->withInput(Input::all())
                    ->withErrors($result['validator']);
            }
            else if ($result['status'] == '3')
            {
                return Redirect::to('user/aws-settings')
                    ->with('error_message', $result['message'])
                    ->withInput(Input::all());
            }
        }
        else if ($update_id)
        {
            if (is_numeric($update_id) && $update_id > 0)
            {
                $amazon_setting = UserAmazonSetting::where(array("id" => $update_id,
                    "profile_id" => $user['profile_id'],
                    "setting_type" => "prod_adv"))
                    ->get();
                if (!$amazon_setting->isEmpty())
                {
                    $amazon_setting =   $amazon_setting->first();
                    $amazon_setting = $amazon_setting->toArray();
                    return View::make('users.prod_adv_settings_form', array("user_setting" => $amazon_setting, 'regions' => $regions));
                }
                else
                {
                    return Redirect::to('user/aws-settings')->with('error_message', 'Invalid request!');
                }
            }
            else
            {
                return Redirect::to('user/aws-settings')->with('error_message', 'Setting not found in system!');
            }
        }
        else
        {
            $profile_id = $user['profile_id'];
            $single_setting = UserAmazonSetting::where(array("profile_id" => $profile_id, "setting_type" => "prod_adv"))->get();
            if (!$single_setting->isEmpty())
            {
                return Redirect::to('user/aws-settings')->with('error_message', "Only one account allowed for the time being. Please try again later to add additional accounts.");
            }
            $amazon_setting = new UserAmazonSetting();
            return View::make('users.prod_adv_settings_form', array("user_setting" => $amazon_setting, 'regions' => $regions));

        }
    }

    public function prodAdvSettings()
    {
        $user = Session::get("user");
        $prod_adv_settings = UserAmazonSetting::getUserAmazonSettings("prod_adv", $user['profile_id']);
        return View::make("users.prod_adv_settings_list", compact("prod_adv_settings"));
    }

    public function marketplaceSettings()
    {
        $user = Session::get("user");
        $marketplace_settings = UserAmazonSetting::getUserAmazonSettings("marketplace", $user['profile_id']);
        return View::make("users.marketplace_settings_list", compact("marketplace_settings"));

    }

    public function chooseDefaultSetting($setting_id = "")
    {
        $result = UserAmazonSetting::chooseDefaultSetting($setting_id);

        if ($result['status'] == '1')
        {
            return Redirect::back()->with('message', 'success= ' . $result['message']);
        }
        else if ($result['status'] == '2')
        {
            return Redirect::back()->with('message', 'danger= ' . $result['message']);
        }

    }

    public function deleteMarketplaceSetting(Request $request, $id)
    {
        $is_deleted = UserAmazonSetting::deleteUserAmazonSetting("marketplace", $id);

        if ($is_deleted)
        {
            $message = "Marketplace setting deleted from the system";
            Session::flash("success_message", $message);
        }
        else
        {
            $message = "Marketplace setting could not be deleted";
            Session::flash("error_message", $message);
        }
        return url("user/marketplace-settings");

    }

    public function deleteProdAdvSetting(Request $request, $id)
    {
        $is_deleted = UserAmazonSetting::deleteUserAmazonSetting("prod_adv", $id);
        if ($is_deleted)
        {
            $message = "AWS setting deleted from the system";
            Session::flash("success_message", $message);
        }
        else
        {
            $message = "AWS setting could not be deleted";
            Session::flash("error_message", $message);
        }
        return url("user/aws-settings");

    }

}