<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;

use App\AmazonCategory;
use View;
use Session;



class OppertunityController extends Controller
{
    public function __construct()
    {

    }
    public function index()
    {
        $categories =   AmazonCategory::orderBy("name","asc")
                        ->get();
        if($categories)
        {
            $categories =   json_encode($categories);
            $categories =   json_decode($categories,true);
        }

        return View::make('opportunityfinder.products_list',compact('categories'));
    }

}
