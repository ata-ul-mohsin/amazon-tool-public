<?php
namespace App\Http\Controllers;

use App\MessageSetting;
//use App\Providers\AppServiceProvider;
use App\TimeZone;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use Aws\Ses\SesClient;
use App\Message;
use App\Country;
use View;
use Session;
use App\MessageVariable;

class MessagesController extends Controller
{
    protected $countries;

    public function __construct()
    {
        $this->countries = Country::get();
    }

    public function index()
    {
        $messages  =   Message::all()
            ->toArray();
        return View::make('messages.index',compact('messages'));
    }

    public function saveMessage()
    {
        $update_id = Input::get("id");
        if (Request::isMethod('post'))
        {

            $result = Message::saveToDb(Input::all());

            if ($result['status'] == '1')
            {
                return Redirect::route('messages')->with('message', 'success= ' . $result['message']);
            }
            else if ($result['status'] == '2')
            {
                return Redirect::back()->withErrors($result['validator']);
            }
            else if ($result['status'] == '3')
            {
                return Redirect::route('messages')->with('message', 'danger= ' . $result['message']);
            }
        }
        else if ($update_id)
        {
            if (is_numeric($update_id) && $update_id > 0)
            {
                $message = Message::find($update_id);
                if (!empty($message))
                {

                    return View::make('messages.form', array("message" => $message, "countries" => $this->countries));
                }
                else
                {
                    return Redirect::route('messages')->with('message', 'danger= Message not found in system!');
                }
            }
            else
            {
                return Redirect::route('messages')->with('message', 'danger= Message not found in system!');
            }
        }
        else
        {
            $message = new Message();
            return View::make('messages.form', array("message" => $message, "countries" => $this->countries));

        }
    }

    public function saveSetting()
    {
        $timezones  =   TimeZone::all()->toArray();
        $user = Session::get("user");

        if (Request::isMethod('post'))
        {
            $data                 =   Input::all();
            $data['profile_id']   =    $user['profile_id'];
            $result = MessageSetting::saveToDb($data);

            if ($result['status'] == '1')
            {
                return Redirect::route('settings')->with('setting', 'message= ' . $result['message']);
            }
            else if ($result['status'] == '2')
            {
                return Redirect::back()->withErrors($result['validator']);
            }
            else if ($result['status'] == '3')
            {
                return Redirect::route('settings')->with('message', 'danger= ' . $result['message']);
            }
        }
        else
        {
            $user_settings = MessageSetting::where("profile_id", $user['profile_id'])
                ->get();
            if (empty($user_settings))
                $user_settings = new MessageSetting();
            else
                $user_settings    =   $user_settings[0];
            return View::make('messages.setting_form', array("settings" => $user_settings,'timezones'=>$timezones));

        }
    }

    public function deleteVariable(Request $request,$id)
    {
        $is_deleted =   MessageVariable::deleteVariable($id);

        if ($is_deleted)
            return redirect("messages/variables")->with("message","success= Variable deleted from the system");
        else
            return redirect('messages/variables')->with('message', 'danger= Variable could not be deleted');
    }

    public function deleteMessage(Request $request,$id)
    {
        $is_deleted =   Message::deleteMessage($id);

        if ($is_deleted)
            return redirect("messages")->with("message","success= Message deleted from the system");
        else
            return redirect('messages')->with('message', 'danger= Message could not be deleted');
    }

    public function test()
    {
        ini_set('max_execution_time', 0);

        $client = SesClient::factory(array(
            'credentials' => array(
                'key' => '1PBWAFQ5FMA8911KRY02',
                'secret'  =>'Xr5TANg4Cp0FkijlshJfQw7o2tvc8wF9LsdMEqkn'
            ),
            'region' => 'us-east-1',
            'http'   => ["verify"=>FALSE],
            'version'=> "latest"
        ));

        $msg = array();
        //ToAddresses must be an array
        $msg['Destination']['ToAddresses'][] = "ataulmohsin@outlook.com";//"success@simulator.amazonses.com";
        $msg['Message']['Subject']['Data'] = "Text only subject";
        $msg['Message']['Subject']['Charset'] = "UTF-8";
        $msg['Message']['Body']['Text']['Data'] = "Text data of email";
        $msg['Message']['Body']['Text']['Charset'] = "UTF-8";
        $msg['Message']['Body']['Html']['Data'] = "<h1>testing</h1><br/><img src='http://vignette1.wikia.nocookie.net/fictionalcrossover/images/e/ef/A_totoro.jpg' /> <br />";
        $msg['Message']['Body']['Html']['Charset'] = "UTF-8";
        try
        {
            $result = $client->sendEmail($msg);
            $msg_id = $result->get('MessageId');
            echo("MessageId: $msg_id");
        }
        catch (SesException  $e)
        {
            echo "<h1>error</h1>";
            echo($e->getMessage());
        }
    }
    public function fetchOrders()
    {
        $ab="HTTP POST
POST /Orders/2013-09-01?AWSAccessKeyId=AKIAJJSKKA7INN5U37SQ
&Action=ListOrders
&SellerId=A2MNBF0CFNJKAW
&SignatureVersion=2
&Timestamp=2015-12-29T06%3A57%3A03Z
    &Version=2013-09-01
    &Signature=mrJaSarUTRqo3vqCOP5R%2BUET7xTXaq4jXTHQ8VCOmeA%3D
    &SignatureMethod=HmacSHA256
    &CreatedAfter=2010-08-31T19%3A00%3A00Z
    &MarketplaceId.Id.1=ATVPDKIKX0DER HTTP/1.1
Host: mws.amazonservices.com
x-amazon-user-agent: AmazonJavascriptScratchpad/1.0 (Language=Javascript)
Content-Type: text/xml";
    }
    public function saveVariable($update_id = "")
    {
        $user = Session::get("user");
        if (Request::isMethod('post'))
        {
            $data                 =   Input::all();
            $data['profile_id']   =    $user['profile_id'];
            $result = MessageVariable::saveToDb($data);

            if ($result['status'] == '1')
            {
                return Redirect::route('variables')->with('message', 'success= ' . $result['message']);
            }
            else if ($result['status'] == '2')
            {
                return Redirect::back()->withErrors($result['validator']);
            }
            else if ($result['status'] == '3')
            {
                return Redirect::route('variables')->with('message', 'danger= ' . $result['message']);
            }
        }
        else if ($update_id)
        {
            if (is_numeric($update_id) && $update_id > 0)
            {
                $variable = MessageVariable::where(array("id"=>$update_id,"profile_id"=>$user['profile_id']))
                            ->get()
                            ->first();
                if($variable)
                    $variable   =   $variable->toArray();
                if (!empty($variable))
                {

                    return View::make('messages.variable_form', array("variable" => $variable));
                }
                else
                {
                    return Redirect::route('variables')->with('message', 'danger= Variable not found in system!');
                }
            }
            else
            {
                return Redirect::route('variables')->with('message', 'danger= Variable not found in system!');
            }
        }
        else
        {
            $variable = new MessageVariable();
            return View::make('messages.variable_form', array("message" => $variable));

        }
    }
    public function variablesList()
    {
        $variables  =   MessageVariable::all()
                        ->toArray();
        return View::make("messages.variables_list",compact("variables"));
    }

}
