<?php

namespace App\Http\Controllers;

use App\Marketplace;
use Illuminate\Support\Facades\Redirect;

use App\Http\Requests;
use App\Http\Requests\MessageRequest;
use App\Message;
use Session;
use App\Country;
use View;
use App\MessageSetting;
use App\TimeZone;
use Illuminate\Support\Facades\Input;

use App\MessageVariable;

class MessageController extends Controller
{
    private   $user;
    protected $countries;
    protected $marketplaces;
    protected $statuses;
    protected $when_send_to_buyer;
    protected $send_after;
    protected $fulfillment_channel;

    public function __construct()
    {
        $this->user = Session::get("user");
        $this->countries = Country::lists("name", "id");
        $this->marketplaces = Marketplace::lists("endpoint", "id");
        $this->statuses = array(
            "Active" => "Active",
            "Test Mode" => "Test Mode",
            "Inactive" => "Inactive",
        );

        $when_send_to_buyer = array();
        $when_send_to_buyer["2d"] = "2 days";
        $when_send_to_buyer["3d"] = "3 days";
        $when_send_to_buyer["4d"] = "4 days";
        $when_send_to_buyer["5d"] = "5 days";
        $when_send_to_buyer["6d"] = "6 days";
        $when_send_to_buyer["7d"] = "7 days";
        $when_send_to_buyer["8d"] = "8 days";
        $when_send_to_buyer["9d"] = "9 days";
        $when_send_to_buyer["10d"] = "10 days";
        $when_send_to_buyer["11d"] = "11 days";
        $when_send_to_buyer["12d"] = "12 days";
        $when_send_to_buyer["13d"] = "13 days";
        $when_send_to_buyer["14d"] = "14 days";
        $when_send_to_buyer["15d"] = "15 days";
        $when_send_to_buyer["16d"] = "16 days";
        $when_send_to_buyer["17d"] = "17 days";
        $when_send_to_buyer["18d"] = "18 days";
        $when_send_to_buyer["19d"] = "19 days";
        $when_send_to_buyer["20d"] = "20 days";
        $when_send_to_buyer["21d"] = "21 days";
        $when_send_to_buyer["4w"] = "4 weeks";
        $when_send_to_buyer["5w"] = "5 weeks";
        $when_send_to_buyer["6w"] = "6 weeks";
        $when_send_to_buyer["7w"] = "7 weeks";
        $when_send_to_buyer["8w"] = "8 weeks";
        $when_send_to_buyer["9w"] = "9 weeks";
        $when_send_to_buyer["10w"] = "10 weeks";

        $this->when_send_to_buyer = $when_send_to_buyer;

        $send_after = array();
        $send_after["confirmed"] = "Confirmed";
        $send_after["shipped"] = "Shipped";
        $send_after["out_for_delivery"] = "Out For Delivery";
        $send_after["delivered"] = "Delivered";

        $this->send_after = $send_after;



        $fulfillment_channel = array();
        $fulfillment_channel["Any"] = "Any";
        $fulfillment_channel["FBA"] = "FBA";
        $fulfillment_channel["Merchant"] = "Merchant Fulfilled";

        $this->fulfillment_channel = $fulfillment_channel;

        //fulfillment_channe

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user      =   Session::get('user');
        $messages  =   Message::where(array("profile_id"=>$user['profile_id']))
                    ->get()
                    ->toArray();

        return View::make('messages.index',compact('messages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('messages.form', array(
                "countries" => $this->countries,
                "marketplaces" => $this->marketplaces,
                "statuses" => $this->statuses,
                "when_send_to_buyer" => $this->when_send_to_buyer,
                "send_after" => $this->send_after,
                "fulfillment_channel" => $this->fulfillment_channel,
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MessageRequest $request)
    {
        $data = \Input::all();

        $user               = Session::get("user");
        $data['profile_id'] =   $user['profile_id'];
        $result = Message::saveToDb($data);

        if ($result['status'] == '1')
        {
            return Redirect::route('messages.index')->with('success_message',  $result['message']);
        }
        else if ($result['status'] == '3')
        {
            return Redirect::route('messages.index')->with('error_message', $result['message']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(MessageRequest $request,Message $message)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(MessageRequest $request,Message $message)
    {
        return View::make('messages.form', array(
                "message" => $message,
                "countries" => $this->countries,
                "marketplaces" => $this->marketplaces,
                "statuses" => $this->statuses,
                "when_send_to_buyer" => $this->when_send_to_buyer,
                "send_after" => $this->send_after,
                "fulfillment_channel" => $this->fulfillment_channel,
            )
        );
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MessageRequest $request, Message $message)
    {
        $data   =    \Input::all();
        $data ['profile_id']  =     Session::get("user")['profile_id'];

        $result = Message::saveToDb($data);
        if ($result['status'] == '1')
        {
            return Redirect::route('messages.index')->with('success_message', $result['message']);
        }
        else if ($result['status'] == '3')
        {
            return Redirect::route('messages.index')->with('error_message', $result['message']);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(MessageRequest $request, Message $message)
    {
        $is_deleted =   Message::deleteMessage($message);
        if ($is_deleted)
        {
            $message    ="Message deleted from the system";
            Session::flash("success_message",$message);
        }
        else
        {
            $message    ="Message could not be deleted";
            Session::flash("error_message",$message);
        }
        return url("messages");
    }


    public function saveSetting()
    {
        $timezones  =   TimeZone::all()->toArray();
        $user = Session::get("user");

        if (\Request::isMethod('post'))
        {
            $data                 =   Input::all();
            $data['profile_id']   =    $user['profile_id'];
            $result = MessageSetting::saveToDb($data);

            if ($result['status'] == '1')
            {
                return Redirect::route('settings')->with('success_message',  $result['message']);
            }
            else if ($result['status'] == '2')
            {
                return Redirect::back()->withErrors($result['validator']);
            }
            else if ($result['status'] == '3')
            {
                return Redirect::route('settings')->with('error_message',  $result['message']);
            }
        }
        else
        {
            $user_settings = MessageSetting::where("profile_id", $user['profile_id'])
                ->get();
            if ($user_settings->isEmpty())
                $user_settings = new MessageSetting();
            else
                $user_settings    =   $user_settings[0];
            return View::make('messages.setting_form', array("settings" => $user_settings,'timezones'=>$timezones));

        }
    }
}
