<?php

namespace App\Http\Controllers;

use App\AmazonProduct;
use App\UserAmazonSetting;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\BrandRequest;
use View;
use App\Brand;
use Session;
use Illuminate\Support\Facades\Redirect;
use App\Marketplace;
use App\AmazonOrder;

class BrandController extends Controller
{
    private static $marketplace_settings;
    private $user;
    public function __construct()
    {

        $this->user = Session::get("user");

        self::$marketplace_settings =   array();
        $marketplace_settings = UserAmazonSetting::where(array("profile_id" => $this->user['profile_id'], "setting_type" => "marketplace"))
            ->select(\DB::raw("concat(account_name,'(',seller_id,')'), id"))
            ->get();
        if($marketplace_settings)
        {
            $marketplace_settings=$marketplace_settings->toArray();
            if(!empty($marketplace_settings))
            {
                foreach($marketplace_settings as $settings)
                {
                    self::$marketplace_settings [$settings['id']]   =   $settings["concat(account_name,'(',seller_id,')')"];
                }
            }
        }
    }
    public function index()
    {
        $setting_str    =   self::build_setting_str(TRUE);
        return View::make("brands.index", compact("setting_str"));
    }
    public function getListBrandData(Request $request)
    {
        try
        {
            $response = array();
            $params = $request->input();
            $user = Session::get('user');

            $brands = Brand::getBrandsBySetting($params);

            $response['recordsTotal'] = $brands['total'];
            unset($brands['total']);

            $response['recordsFiltered'] = $brands['filtered'];
            unset($brands['filtered']);


            $response['draw'] = $params['draw'];

            $response['data'] = $brands;

        }
        catch(Exception $exp)
        {
            echo $exp->getMessage();exit;
        }
        return json_encode($response,true);
    }
    public function create()
    {
        return View::make("brands.form",array("marketplace_settings"=>self::$marketplace_settings));
    }
    public function store(BrandRequest $request)
    {

        $data = \Input::all();
        $user = Session::get("user");
        $data['profile_id'] = $user['profile_id'];
        $result = Brand::saveToDb($data);

        if ($result['status'] == '1')
        {
            if(isset($data['save_add']))
                return Redirect::route('brands.create')->with('success_message', $result['message']);
            else
                return Redirect::route('brands.index')->with('success_message', $result['message']);
        }
        else if ($result['status'] == '3')
        {
            return Redirect::route('brands.index')->with('error_message',$result['message']);

        }

    }
    public function show($id)
    {
        echo "show method";
        //
    }
    public function edit(BrandRequest $request,Brand $brand)
    {
       return View::make("brands.form")->with(array("brand"=>$brand,"marketplace_settings"=>self::$marketplace_settings));
    }
    public function update(BrandRequest $request, Brand $brand)
    {
        $data   =    \Input::all();
        $data ['profile_id']  =     Session::get("user")['profile_id'];
        $result = Brand::saveToDb($data);
        if ($result['status'] == '1')
        {
            if(isset($data['save_add']))
                return Redirect::route('brands.create')->with('success_message', $result['message']);
            else
                return Redirect::route('brands.index')->with('success_message', $result['message']);
        }
        else if ($result['status'] == '3')
        {
            return Redirect::route('brands.index')->with('error_message',$result['message']);

        }
    }
    public function destroy(BrandRequest $request, Brand $brand)
    {
        $is_deleted =   Brand::deleteBrand($brand);
        if ($is_deleted)
        {
            $brand    ="Brand deleted from the system";
            Session::flash("success_message",$brand);
        }
        else
        {
            $brand    ="Brand could not be deleted";
            Session::flash("error_message",$brand);
        }
        return url("brands");
    }
    private static function build_setting_str($already_populated="",array $settings=array())
    {
        $setting_str        =   "";
        if(!empty($already_populated))
        {
            $settings   =   self::$marketplace_settings;
            if(!empty($settings))
            {
                foreach ($settings as $key => $setting)
                {
                    $setting_str .= "'" . $key . ":::" . $setting . "',";
                }
            }
        }
        else
        {
            foreach ($settings as $setting)
            {
                $setting_str .= "'" . $setting['id'] . ":::" . $setting['account_name'] . " (" . $setting['seller_id'] . ")',";
            }
        }
        if(!empty($setting_str))
            $setting_str        =   substr($setting_str,0,-1);
        return $setting_str;
    }
    private static function build_brand_str(array $brands)
    {
        $brands_str         =   "";
        foreach($brands as $brand)
        {
            $brands_str   .=  "'".$brand['id'].":::".$brand['name']."',";
        }
        if(!empty($brands_str))
            $brands_str         =   substr($brands_str,0,-1);
        return $brands_str;
    }
    public function assignBrands()
    {
        $brands         =   Brand::getBrands("","",TRUE);
        $setting_str    =   self::build_setting_str(TRUE);
        $brands_str       =   self::build_brand_str($brands);
        return View::make("brands.brand_assignment", compact("setting_str","brands_str"));
    }
    public function getProductData(Request $request)
    {
        try
        {
            $response = array();
            $params = $request->input();
            $user = Session::get('user');

            $products = AmazonProduct::getProductsBySetting(TRUE,$params);

            $response['recordsTotal'] = $products['total'];
            unset($products['total']);

            $response['recordsFiltered'] = $products['filtered'];
            unset($products['filtered']);


            $response['draw'] = $params['draw'];

            $response['data'] = $products;

        }
        catch(Exception $exp)
        {
            echo $exp->getMessage();exit;
        }
        return json_encode($response,true);
    }
    public function getBrandData($setting_id)
    {
        $brands =   Brand::where(array("setting_id"=>$setting_id))
               ->select(["id","name"])
               ->get();
        if(!$brands->isEmpty())
            $brands =   $brands->toArray();
        else
            $brands =   array();
        return $brands;
    }
    public function saveBrandAssignment(Request $request)
    {
        $data   =   $request->input();

        $user = Session::get("user");
        $data['profile_id'] = $user['profile_id'];
        $result = Brand::assignBrandsToProducts($data);

        if ($result['status'] == '1')
        {
            return redirect('assign-brand')->with('success_message', $result['message']);
        }
        else if ($result['status'] == '3')
        {
            return redirect('assign-brand')->with('error_message',$result['message']);

        }

    }
    public function trackProduct()
    {
        return View::make("brands.products_traking_form",array("marketplace_settings"=>self::$marketplace_settings));
    }
    public function trackAndSaveProduct()
    {
        $data   =  \Input::all();

        $user = Session::get("user");
        $data['profile_id'] = $user['profile_id'];
        $result = AmazonProduct::trackProduct($data);

        if ($result['status'] == '1')
        {
            return redirect('track-product')->with('success_message', $result['message']);
        }
        else if ($result['status'] == '3')
        {
            return redirect('track-product')->with('error_message',$result['message']);

        }
        else
        {
            return redirect('track-product')->with('error_message'," Error in system. Please try again later");

        }
    }
    public function orders()
    {
        $setting_str    =   self::build_setting_str(TRUE);
        return View::make("messages.orders", compact("setting_str"));
    }
    public function getListOrderData(Request $request)
    {
        try
        {
            $response = array();
            $params = $request->input();
            $user = Session::get('user');

            $orders =   AmazonOrder::getOrdersBySetting($params);
            $response['recordsTotal'] = $orders['total'];
            unset($orders['total']);

            $response['recordsFiltered'] = $orders['filtered'];
            unset($orders['filtered']);


            $response['draw'] = $params['draw'];

            $response['data'] = $orders;

        }
        catch(Exception $exp)
        {
            echo $exp->getMessage();exit;
        }
        return json_encode($response,true);
    }
}
