<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Http\Requests;
use App\Http\Requests\VariableRequest;
use View;
use App\Variable;
use Session;
use Illuminate\Support\Facades\Redirect;
class VariableController extends Controller
{
    public function __construct()
    {
        $this->middleware('is_admin', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user    =   Session::get("user");

        if(!empty($user['profile_id']))
            if(!empty($user['is_admin']))
                $is_admin   =   TRUE;
            else
                $is_admin   =   FALSE;
        $variables = Variable::all();
        if(!$variables->isEmpty())
            $variables  = $variables->toArray();

        return View::make("variables.index", compact("variables","is_admin"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make("variables.form");
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(VariableRequest $request)
    {
        $data = \Input::all();
        $data['profile_id'] = Session::get("user")['profile_id'];
        $result = Variable::saveToDb($data);
        if ($result['status'] == '1')
        {
            return Redirect::route('variables.index')->with('success_message', $result['message']);
        }
        else if ($result['status'] == '3')
        {
            return Redirect::route('variables.index')->with('error_message', $result['message']);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit( Variable $variable)
    {
        return View::make("variables.form",compact("variable"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(VariableRequest $request, $variable)
    {
        $data = \Input::all();
        $data['id']         =   $variable->id;
        $data['profile_id'] = Session::get("user")['profile_id'];
        $result = Variable::saveToDb($data);
        if ($result['status'] == '1')
        {
            return Redirect::route('variables.index')->with('success_message', $result['message']);
        }
        else if ($result['status'] == '3')
        {
            return Redirect::route('variables.index')->with('error_message', $result['message']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(VariableRequest $request, Variable $variable)
    {
        $is_deleted =   Variable::deleteVariable($variable);
        if ($is_deleted)
        {
            $variable    ="Variable deleted from the system";
            Session::flash("success_message",$variable);
        }
        else
        {
            $variable    ="Variable could not be deleted";
            Session::flash("error_message",$variable);
        }
        return url("variables");
    }

}
