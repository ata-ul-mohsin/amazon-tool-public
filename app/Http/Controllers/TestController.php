<?php

namespace App\Http\Controllers;

use App\Region;
use App\User;
use App\UserAmazonSetting;
use Illuminate\Http\Request;
use App\Jobs\SendReminderEmail;
use App\Http\Controllers\Controller;
use MarketplaceWebServiceOrders_Client;
use MarketplaceWebServiceOrders_Model_ListOrderItemsRequest;
use MarketplaceWebServiceOrders_Model_ListOrdersRequest;
use DOMDocument;
use App\AmazonOrder;
use App\AmazonProduct;
use Session;
use App\AmazonFeedback;

class TestController extends Controller
{
    /**
     * Send a reminder e-mail to a given user.
     *
     * @param  Request $request
     * @param  int $id
     * @return Response
     */
    public function testing()
    {
        echo phpinfo();
        exit;
    }
    public function sendReminderEmail(Request $request, $id)
    {
        /*$user = User::findOrFail($id);*/
        $user = "just a test";
        //  $this->dispatch(new SendReminderEmail($user));
    }

    public function test_fetch_products()
    {
        $setting_id = '16';
        $order_id = "111-6039804-2200249";
        $user = Session::get("user");
        $profile_id = $user['profile_id'];
        $settings_data = UserAmazonSetting::getSettingForMWSRequest($profile_id, $setting_id);
        if (!empty($settings_data['user']) &&
            !empty($settings_data['region']) &&
            !empty($settings_data['marketplace'])
        )
        {
            $user = $settings_data['user'];
            unset($settings_data['user']);
            $product = new AmazonProduct($user, $settings_data);
            $product->fetchProductsFromOrder($order_id);
        }
    }

    public function just_test()
    {
        $region = Region::find(1);
        $test = $region->marketplace()->get();
        \CommonHelper::debug($test, true);
        /*$result =   UserAmazonSetting::getUsersDefaultSettingsIds("marketplace");
        \CommonHelper::debug($result,true);*/
        $mws_settings = UserAmazonSetting::find('16');
        if ($mws_settings)
        {
            $mws_settings->user = $mws_settings->user()->get();
            $mws_settings = \CommonHelper::objToArray($mws_settings);

        }
        \CommonHelper::debug($mws_settings, true);
    }

    public function get_report_testing()
    {
        $mws_settings = UserAmazonSetting::find(16);
        if ($mws_settings) {
            $mws_settings->user = $mws_settings->user()->get();
            $mws_settings->region = $mws_settings->region()->get();
            if ($mws_settings->region) {
                $region = $mws_settings->region;
                $region = $region->first();
                $mws_settings->region = $region;
                $marketplace = $region->marketplace()->get();

                if ($marketplace) {
                    $mws_settings->marketplace = $marketplace->first();
                }
            }


            $mws_settings = \CommonHelper::objToArray($mws_settings);
            if (isset($mws_settings['user'])) {
                $mws_user = $mws_settings['user'][0];
                unset($mws_settings['user']);
            }
        }
       // \CommonHelper::sendMWSRequest('get_report', $mws_settings);
        \CommonHelper::debug($mws_settings,true);
    }
    public function report_list_testing()
    {
        $mws_settings = UserAmazonSetting::find(16);
        if ($mws_settings) {
            $mws_settings->user = $mws_settings->user()->get();
            $mws_settings->region = $mws_settings->region()->get();
            if ($mws_settings->region) {
                $region = $mws_settings->region;
                $region = $region->first();
                $mws_settings->region = $region;
                $marketplace = $region->marketplace()->get();

                if ($marketplace) {
                    $mws_settings->marketplace = $marketplace->first();
                }
            }


            $mws_settings = \CommonHelper::objToArray($mws_settings);
            if (isset($mws_settings['user'])) {
                $mws_user = $mws_settings['user'][0];
                unset($mws_settings['user']);
            }
        }
        \CommonHelper::sendMWSRequest('request_report_list', $mws_settings);

    }
    public function report_testing()
    {
        $mws_settings = UserAmazonSetting::find(16);
        if ($mws_settings) {
            $mws_settings->user = $mws_settings->user()->get();
            $mws_settings->region = $mws_settings->region()->get();
            if ($mws_settings->region) {
                $region = $mws_settings->region;
                $region = $region->first();
                $mws_settings->region = $region;
                $marketplace = $region->marketplace()->get();

                if ($marketplace) {
                    $mws_settings->marketplace = $marketplace->first();
                }
            }


            $mws_settings = \CommonHelper::objToArray($mws_settings);
            if (isset($mws_settings['user'])) {
                $mws_user = $mws_settings['user'][0];
                unset($mws_settings['user']);
            }
        }
        \CommonHelper::sendMWSRequest('request_report', $mws_settings);
    }
    public function local_testing()
    {
        $setting_id = '16';
        $user       =   Session::get("user");
        $profile_id =   $user['profile_id'];
        $settings_data = UserAmazonSetting::getSettingForMWSRequest($profile_id,$setting_id);
        if (!empty($settings_data['user']) &&
            !empty($settings_data['region']) &&
            !empty($settings_data['marketplace'])
        )
        {
            $user = $settings_data['user'];
            unset($settings_data['user']);
            $order = new AmazonOrder($user, $settings_data);
            $order->fetchAmazonOrders();
        }
    }

    public function test_feedback()
    {
        $result = UserAmazonSetting::getUsersDefaultSettingsIds("marketplace");

        if (!empty($result) && is_array($result))
        {
            foreach ($result as $setting)
            {
                $amazon_feedback    =   new AmazonFeedback( $setting['id']);
                $amazon_feedback->getFeedbacksAgainstSellerId();
            }
        }

    }

    public function test_reviews()
    {
        $offset = 0;
        $limit = 10;
        do
        {
            $result = AmazonProduct::select(array("id", "asin", "marketplace", "reviews_link"))
                ->skip($offset)
                ->take($limit)
                ->get();

            if (!$result->isEmpty())
            {
                $result = $result->toArray();
                $data   =  $result;
                $offset += $limit;
                if (!empty($data))
                {
                    $data = AmazonProduct::getRatingAndReviewUrlData($data);
                    if (!empty($data) && is_array($data))
                    {
                        foreach ($data as $product)
                        {
                            $amz_product = AmazonProduct::find($product['id']);

                            $amz_product->reviews_link = $product['reviews_link'];

                            $rating_review_data = AmazonProduct::getRatingReviews($amz_product->reviews_link);

                            $amz_product->reviews = $rating_review_data['reviews'];
                            $amz_product->ratings = $rating_review_data['rating'];
                            $amz_product->save();
                        }
                    }
                }
            }
            else
                $result = array();
        } while (!empty($result));



    }

    public function test_compose_message()
    {

        \CommonHelper::composeMessage(1);

    }

}