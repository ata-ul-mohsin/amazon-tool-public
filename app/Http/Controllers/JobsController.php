<?php

namespace App\Http\Controllers;

use App\AmazonOrder;
use App\AmazonProduct;
use App\AmazonReportRequest;
use App\Jobs\FetchFeedbacks;
use App\Jobs\SendReminderEmail;
use App\Jobs\FetchProductsAgainstOrders;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Jobs\FetchReviews;
use App\Http\Controllers\Controller;
use App\User;
use App\UserAmazonSetting;
use App\AmazonFeedback;
use App\Jobs\FetchAmazonOrders;
use Illuminate\Database\Eloquent\Collection;

class JobsController extends Controller
{

    public function index()
    {
        //
    }

    public function enqueueFetchOrderJob()
    {
        //  if (\CommonHelper::isLocal())
        $result = UserAmazonSetting::getUsersDefaultSettingsIds("marketplace");
        if (!empty($result) && is_array($result))
        {
            foreach ($result as $setting)
            {
                $this->dispatch(new FetchAmazonOrders($setting['id']));
            }
            echo "success";
            exit;
        }
        echo "failure";
    }
    /**
     * Push order against default settings of active users into product queue using AWS SQS
     */
    public function createOutboxEntries()
    {
        $count = 1;
        $active_users = User::getActiveUsersWithMessages();
        foreach ($active_users as $user)
        {
            foreach ($user['messages'] as $user_message)
            {
                $orders = AmazonOrder::getOrdersForMessages($user['profile_id'], $user_message['id']);
                $message    =   "";
                if(!empty($orders['message']))
                {
                    $message = $orders['message'];
                    unset($orders['message']);
                }
                if (!empty($orders) && is_array($orders) && !empty($message))
                {
                   foreach($orders as $order)
                   {

                       $already_created =   DB::table('outbox_messages')->where(
                           [
                               'message_id' => $user_message['id'],
                               'order_id'   => $order['order_id'],
                               'setting_id' => $order['setting_id'],
                               'profile_id' => $user['profile_id']
                           ]
                       )->get();
                       if($already_created)
                           continue;

                       DB::table('outbox_messages')->insert(
                           [
                               'message_id' => $user_message['id'],
                               'order_id'   => $order['order_id'],
                               'email'      => $order['email'],
                               'products'   => $order['products'],
                               'setting_id' => $order['setting_id'],
                               'profile_id' => $user['profile_id'],
                               'created_at' => date("Y-m-d h:i:s"),
                               'updated_at' => date("Y-m-d h:i:s"),
                               'due_date'   => date("Y-m-d h:i:s"),
                           ]
                       );
                   }
                }
            }
        }
    }
    public function enqueueFetchProductsJob()
    {
        $errors =   array();
        $result = UserAmazonSetting::getUsersDefaultSettingsIds("marketplace", "", 1);
        if (!empty($result) && is_array($result))
        {
            foreach ($result as $setting)
            {
                $orders = AmazonOrder::getOrdersForProducts($setting['profile_id'], $setting['id']);

                if (!empty($orders) && is_array($orders))
                {
                    $product_orders = array();
                    foreach ($orders as $product_order)
                    {
                        $product_orders[] = $product_order->order_id;
                    }

                    $orders_dispatched  =   $this->dispatch_fetch_products_job($setting['profile_id'], $setting['id'], $product_orders);

                    if(empty($orders_dispatched))
                        $errors[$setting['profile_id']] = "1";
                }
            }
        }
        if(empty($errors))
            echo "success";
        else
            echo "some task could not be performed";
        exit;
    }
    public function enqueueSendEmailsJob()
    {
        $count = 1;
        $active_users = User::getActiveUsersWithMessages();
        foreach ($active_users as $user)
        {
            foreach ($user['messages'] as $user_message)
            {
                $orders = AmazonOrder::getOrdersForMessages($user['profile_id'], $user_message['id']);
                if (!empty($orders) && is_array($orders))
                {
                    $message_orders = array();
                    foreach ($orders as $message_order)
                    {
                        $message_orders[] = $message_order->order_id;
                    }
                    $this->dispatch_email_job($user['profile_id'], $user_message['id'], $message_orders);
                }
            }
        }
    }
    public function enqueueFetchFeedbacks()
    {
        $result = UserAmazonSetting::getUsersDefaultSettingsIds("marketplace");
        $no_to_dispatch =   count($result);
        $no_dispatched  =   0;
        if (!empty($result) && is_array($result))
        {
            $no_dispatched  =   $this->dispatch_fetch_feedbacks_job($result);
        }

        if($no_dispatched < $no_to_dispatch)
            echo "Error ".($no_to_dispatch - $no_dispatched)." jobs could not be dispatched";
        else
            echo "success";
    }
    public function enqueueFetchReviewsAndRatings()
    {
        $offset = 0;
        $limit = 10;
        do
        {
            $result = AmazonProduct::select(array("id", "asin", "marketplace", "reviews_link"))
                ->skip($offset)
                ->take($limit)
                ->get();

            if (!$result->isEmpty())
            {
                $result = $result->toArray();
                $this->dispatch_fetch_reviews_job($result);
                $offset += $limit;
            }
            else
                $result = array();
        } while (!empty($result));

    }
    public function requestOrdersReports()
    {
        try
        {
            $settings = UserAmazonSetting::getUsersDefaultSettingsIds("marketplace","",TRUE,"request_orders_report");
            $status =   FALSE;
            if($settings instanceof Collection)
            {
                if(!$settings->isEmpty())
                    $status =   AmazonReportRequest::sendReportRequests($settings);
                echo $status['success']." requests succeeded. ".$status['failure']." requests failed. ";
                exit;
            }

        }
        catch(\Exception $exp)
        {
            echo $exp->getMessage(); exit;
        }
        echo "No Report Requested";
        exit;

    }
    public function checkRequestStatus()
    {
        try
        {

            $settings = UserAmazonSetting::getUsersDefaultSettingsIds("marketplace","",TRUE,"request_list_report");
            $status =   "No Status Updated";
            if($settings instanceof Collection)
            {
                if(!$settings->isEmpty())
                    $status =   AmazonReportRequest::sendListRequests($settings);

                if(isset($status['success']))
                {
                    echo $status['success'];
                    echo " requests updated.";
                    exit;
                }

            }
        }
        catch(Exception $exp)
        {
            echo $exp->getMessage();
        }
        echo "No Status Updated";
        exit;

    }
    public function getOrderReports()
    {
        try
        {
            $settings = UserAmazonSetting::getUsersDefaultSettingsIds("marketplace","",TRUE,"request_get_report");
            $status =   "";
            if($settings instanceof Collection)
            {
                if(!$settings->isEmpty())
                    $status =   AmazonReportRequest::getReports($settings);

                if(isset($status['success']))
                {
                    echo $status['success'];
                    echo " requests updated.";
                    exit;
                }

            }
        }
        catch(Exception $exp)
        {
            echo $exp->getMessage();
        }
        echo "No Status Updated";
        exit;

    }
    /*
     * Helper function to actually dispatch the product fetching job on the corresponding queue
     */
    private function dispatch_fetch_products_job($profile_id, $setting_id, $product_orders)
    {
        $products_by_orders_queue = \Config::get("queue.connections.productsByOrders");
        if (!empty($products_by_orders_queue['queue']))
        {
            $products_fetching_job = (new FetchProductsAgainstOrders($profile_id, $setting_id, $product_orders));

            if ($products_fetching_job)
            {
                $products_fetching_job = $products_fetching_job->onQueue($products_by_orders_queue['queue']);
                $this->dispatch($products_fetching_job);
                return TRUE;
            }
        }
        return FALSE;
    }

    private function dispatch_email_job($profile_id, $message_id, $orders)
    {
        $messages_queue = \Config::get("queue.connections.messages");
        if (!empty($messages_queue['queue']))
        {
            $messaging_job = (new SendReminderEmail($profile_id, $message_id, $orders));
            if ($messaging_job)
            {
                $messaging_job = $messaging_job->onQueue($messages_queue['queue']);
                $this->dispatch($messaging_job);
            }
        }
    }

    private function dispatch_fetch_feedbacks_job(array $setting_ids)
    {
        $no_dispatched  =   0;
        if (!empty($setting_ids))
            $feedback_queue = \Config::get("queue.connections.feedbacks");

        if (!empty($feedback_queue['queue']))
        {
            foreach ($setting_ids as $setting)
            {
                $feedback_job = (new FetchFeedbacks($setting['id']));
                if ($feedback_job)
                {
                    $feedback_job = $feedback_job->onQueue($feedback_queue['queue']);
                    $this->dispatch($feedback_job);
                    $no_dispatched++;
                }
            }
        }
        return $no_dispatched;
    }

    private function dispatch_fetch_reviews_job(array $product_data)
    {
        if (!empty($product_data))
        {
            $reviews_queue = \Config::get("queue.connections.reviews");

            if (!empty($reviews_queue['queue']))
            {
                $reviews_job = (new FetchReviews($product_data));
                if ($reviews_job)
                {
                    $reviews_job = $reviews_job->onQueue($reviews_queue['queue']);
                    $this->dispatch($reviews_job);
                }

            }
        }
    }

    public function test()
    {
        //products, brands, against settings

    }

}
