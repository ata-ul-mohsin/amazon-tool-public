<?php

namespace App\Http\Requests;

use App\BlacklistEmail;
use App\Http\Requests\Request;
use Session;
class BlacklistEmailRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    private $user;
    private $action;
    public function __construct()
    {
        parent::__construct();
        $this->user   = Session::get("user");
        $this->action = \Route::getCurrentRoute()->getActionName();
        $this->action = explode("@", $this->action);
        if (isset($this->action[1]))
        {
            $this->action = $this->action[1];
        }
    }
    public function authorize()
    {
        $bl_email = $this->route('blacklist_emails');
        if ($bl_email)
            $bl_email = $bl_email->id;
        if (!empty($this->action))
            switch ($this->action)
            {
                case "update":
                case "edit":
                case "destroy":
                    if (!empty($this->user['profile_id']) && $bl_email)
                    {
                        $is_owner = BlacklistEmail::where(array("profile_id" => $this->user['profile_id'], "id" => $bl_email))->get();
                        if (!$is_owner->isEmpty())
                        {
                            return TRUE;
                        }
                    }
                    return FALSE;
                    break;
                default:
                    break;
            }
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $bl_email = $this->route('blacklist_emails');
        if ($bl_email)
            $bl_email = $bl_email->id;
        else
            $bl_email  =   "";
        if (!empty($this->action))
        {
            switch ($this->action)
            {
                case "create":
                case "destroy":
                case "edit":
                    return array();
                    break;
                default:
                    break;
            }
        }
        return [
            "email"         =>  "required|email",
            "setting_id"    =>  "required|integer"
        ];
    }
}
