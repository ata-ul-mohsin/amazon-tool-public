<?php
namespace App\Http\Requests;
use App\Http\Requests\Request;
use Session;
use App\Brand;
class BrandRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    private $user;
    private $action;

    public function __construct()
    {
        parent::__construct();
        $this->user = Session::get("user");
        $this->action = \Route::getCurrentRoute()->getActionName();
        $this->action = explode("@", $this->action);
        if (isset($this->action[1]))
        {
            $this->action = $this->action[1];
        }

    }

    public function authorize()
    {

        $brand = $this->route('brands');
        if ($brand)
            $brand = $brand->id;


        if (!empty($this->action))
            switch ($this->action)
            {
                case "update":
                case "edit":
                    if (!empty($this->user['profile_id']) && $brand)
                    {
                        $is_owner = Brand::where(array("profile_id" => $this->user['profile_id'], "id" => $brand))->get();
                        if (!$is_owner->isEmpty())
                        {
                            return TRUE;
                        }
                    }
                    return FALSE;
                    break;
                default:
                    break;
            }
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $brand = $this->route('brands');
        if ($brand)
            $brand = $brand->id;
        else
            $brand  =   "";
        if (!empty($this->action))
        {
            switch ($this->action)
            {
                case "create":
                case "destroy":
                case "edit":
                    return array();
                    break;
                default:
                    break;
            }
        }



        switch($this->method())
        {

            case 'POST':
            {
                return [
                    "name" => "required|unique:brands,name",
                    "signature" => "required",
                    "brand_logo" => "image|mimes:jpg,jpeg,png,gif|max:500"
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    "name" => "required|unique:brands,name,".$brand,
                    "signature" => "required",
                    "brand_logo" => "image|mimes:jpg,jpeg,png,gif|max:500"
                ];
            }
            default:break;
        }


    }
}