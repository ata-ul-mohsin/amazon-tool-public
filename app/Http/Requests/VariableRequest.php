<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Session;
class VariableRequest extends Request
{

    private $user;
    private $action;

    public function __construct()
    {
        parent::__construct();
        $this->user   = Session::get("user");
        $this->action = \Route::getCurrentRoute()->getActionName();
        $this->action = explode("@", $this->action);
        if (isset($this->action[1]))
        {
            $this->action = $this->action[1];
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (!empty($this->action))
        {
            switch ($this->action)
            {
                case "index":
                case "show":
                case "create":
                case "edit":
                case "destroy":
                    return array();
                    break;
                default:
                    break;
            }
        }
        return [
            "name"=> "required",
            "value"=> "required",
            "description"=>"required"
        ];
    }
}
