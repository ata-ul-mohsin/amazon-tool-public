<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Session;
use App\Message;
class MessageRequest extends Request
{
    private $user;
    private $action;

    public function __construct()
    {
        parent::__construct();
        $this->user   = Session::get("user");
        $this->action = \Route::getCurrentRoute()->getActionName();
        $this->action = explode("@", $this->action);
        if (isset($this->action[1]))
        {
            $this->action = $this->action[1];
        }
    }

    public function authorize()
    {
       $message = $this->route('messages');
            if ($message)
                $message = $message->id;

        if (!empty($this->action))
            switch ($this->action)
            {
                case "update":
                case "edit":
                case "destroy":
                    if (!empty($this->user['profile_id']) && $message)
                    {
                        $is_owner = Message::where(array("profile_id" => $this->user['profile_id'], "id" => $message))->get();
                        if (!$is_owner->isEmpty())
                        {
                            return TRUE;
                        }
                    }
                    return FALSE;
                    break;
                default:
                    break;
            }
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (!empty($this->action))
        {
            switch ($this->action)
            {
                case "create":
                case "edit":
                case "destroy":
                    return array();
                    break;
                default:
                    break;
            }
        }
        return [
            'title' => "required",
            'subject' => "required",
            "content" => "required",
            "message_file" => 'max:2048|mimes:doc,docx,pdf,jpg,jpeg,png,gif'
        ];
    }
}