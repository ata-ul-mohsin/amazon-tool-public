<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Redirect;
use Session;
use App\User;
class Validate
{
   protected $auth;
   public function __construct(Guard $auth)
   {
       $this->auth = $auth;
   }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
       $user    =   Session::get("user");
        if(!empty($user['profile_id']))
        {
            return $next($request);
        }
        else
        {
            if(User::validate_remember_me($request))
                return $next($request);
            return Redirect::to("user/login");
        }
    }
}
