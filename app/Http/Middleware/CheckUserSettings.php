<?php
namespace App\Http\Middleware;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Redirect;
use Session;
use App\UserAmazonSetting;

class CheckUserSettings
{
    protected $auth;
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = Session::get("user");

        $error_msg= "";
        if(Session::has("error_message"))
            $error_msg  =   Session::get("error_message");

        $current_route_name = \Request::route();
        if($current_route_name)
        {
            $current_route_name = $current_route_name->getCompiled();
            if($current_route_name)
                $current_route_name =   $current_route_name->getStaticPrefix();
        }

        if(strcmp($current_route_name,"/user/save-product-advertising-setting")==0 ||
            strcmp($current_route_name,"/user/add-aws-setting")==0 ||
            strcmp($current_route_name,"/user/save-marketplace-setting")==0 ||
            strcmp($current_route_name,"/user/add-marketplace-setting")==0 ||
            strcmp($current_route_name,"/user/delete-product-advertising-setting")==0 ||
            strcmp($current_route_name,"/user/delete-marketplace-setting")==0 ||
            strcmp($current_route_name,"/user/marketplace-settings")==0 ||
            strcmp($current_route_name,"/user/aws-settings")==0)
        {
            return $next($request);
        }
        else
        {
            if (!empty($user['profile_id']))
            {
                $marketplace_settings = UserAmazonSetting::getUserAmazonSettings("marketplace", $user['profile_id']);

                if (!empty($marketplace_settings))
                {
                    foreach ($marketplace_settings as $mkt_setting)
                    {

                        if (strcmp($mkt_setting['is_default'],"TRUE")==0)
                        {
                            $aws_settings = UserAmazonSetting::getUserAmazonSettings("prod_adv", $user['profile_id']);
                            if (!empty($aws_settings))
                            {
                                foreach ($aws_settings as $aws_setting)
                                {
                                    if (strcmp($aws_setting['is_default'],"TRUE")==0)
                                    {
                                        return $next($request);
                                    }
                                }

                                return redirect("user/aws-settings")->with('error_message', $error_msg.'<br/>'.'Please choose default AWS setting to access any functionality!');

                            }
                            else
                            {
                                return redirect("user/add-aws-setting")->with('error_message', $error_msg.'<br/>'.'Please add AWS setting to access any functionality!');
                            }
                        }

                    }
                    return redirect("user/marketplace-settings")->with('error_message', $error_msg.'<br/>'.'Please choose default marketplace setting to access any functionality!');
                }
                else
                {
                    return redirect("user/add-marketplace-setting")->with('error_message', $error_msg.'<br/>'.'Please add marketplace setting to access any functionality!');
                }
            }
            else
            {
                return   Redirect::to("user/login")->with('error_message', $error_msg.'<br/>'.'Please login to proceed!');
            }

        }

    }
}
