<?php
namespace App\Http\Middleware;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Redirect;
use Session;

class ValidateAdmin
{
    protected $auth;
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user    =   Session::get("user");

        if(!empty($user['profile_id']))
        {
            if(!empty($user['is_admin']))
                return $next($request);
            else
                return Redirect::to("/")->with('error_message', 'Please login as admin to access this functionality!');
        }
        else
        {
            return   Redirect::to("user/login")->with('error_message', 'Please login as admin to access this functionality!');
        }
    }
}
