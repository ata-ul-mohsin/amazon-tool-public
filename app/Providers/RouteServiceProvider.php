<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Session;
use App\AmazonFeedback;
use Illuminate\Support\Facades\Schema;
class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        //
        $router->pattern('id','[0-9]+');
        $router->model('user', 'App\User');
        $router->model("brands","App\Brand");
        $router->model("messages","App\Message");
        $router->model("variables","App\Variable");
        $router->model("blacklist_emails","App\BlacklistEmail");
        $router->model("feedback","App\AmazonFeedback",function($feedback){
            $user   =   Session::get("user");
            $table  =   "amazon_feedbacks_".$user['profile_id'];

            if(Schema::hasTable($table))
            {
                $feedback_obj = new AmazonFeedback();
                $feedback_obj->setTable($table);
                $feedback_obj   =   $feedback_obj->where("id", $feedback)->get();

                if (!$feedback_obj->isEmpty())
                {
                    return $feedback_obj->first();
                }
            }
             abort(404);
        });
        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
