<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Session;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {

            $current_route_name = \Request::route();
            if($current_route_name)
            {
                $current_route_name = $current_route_name->getCompiled();
                if($current_route_name)
                    $current_route_name =   $current_route_name->getStaticPrefix();
            }


            //$current_route_name =   $current_route_name->tokens;
            //  $current_route_name =   $current_route_name['Symfony\Component\Routing\CompiledRoutetokens'];
            //Symfony\Component\Routing\CompiledRoutetokens
            $user   =   Session::get("user");
            $first_name  =   $user['first_name'];
            $last_name  =   $user['last_name'];
            $user_name  =   $first_name." ".$last_name;
            $view->with('current_route_name', $current_route_name)->with('user_name', $user_name);

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
