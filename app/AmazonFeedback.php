<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserAmazonSetting;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;
use Session;
use Mail;
class AmazonFeedback extends Model
{

    private $feedback_url;
    private $settings;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($setting_id="")
    {
        if(is_numeric($setting_id) && $setting_id >0)
        {
            $settings = UserAmazonSetting::where(array("id" => $setting_id, "setting_type" => "marketplace"))
                ->with("region.marketplace")
                ->get();

            if (!$settings->isEmpty())
            {
                $settings = $settings->first();
                $settings = $settings->toArray();
            }
            else
                $settings = array();
            if (isset($settings['region']['marketplace']['marketplace_id']) && isset($settings['seller_id']))
            {
                $this->settings = $settings;
                $this->feedback_url = "http://www.amazon.com/sp/ajax/feedback";
                $this->feedback_url = $this->feedback_url .
                    "?seller=" . $this->settings['seller_id'] .
                    "&marketplaceID=" . $this->settings['region']['marketplace']['marketplace_id'];
            }
        }

    }

    public function getFeedbacksAgainstSellerId($page = 1)
    {
        $settings = (array)$this->settings;
        $url = $this->feedback_url;
        if(!empty($settings['seller_id']) && !empty($url))
        {

            try
            {
                $result = array();
                      $options = array();
                $options[CURLOPT_RETURNTRANSFER] = 1;
                $options[CURLOPT_URL] = $this->feedback_url . "&pageNumber=" . $page;

                $curl = curl_init();
                curl_setopt_array($curl, $options);
                $result = curl_exec($curl);
                curl_close($curl);

                $result = json_decode($result, true);

                if(!empty($result['details']))
                {
                    $this::saveFeedbacksToDB($result['details']);
                }
                if (!empty($result["hasNextPage"]))
                {
                    $page++;
                    $this->getFeedbacksAgainstSellerId($page);
                }
            }
            catch (Exception $exp)
            {
                echo $exp->getMessage();
                exit;
                return FALSE;
            }
            return TRUE;
        }
        return FALSE;
    }

    private function saveFeedbacksToDB(array $feedbacks)
    {
        $table = \CommonHelper::find_or_create_table($this->settings['profile_id'], "feedbacks");

        foreach ($feedbacks as $feedback)
        {
            try
            {
                $amazon_feedback = new AmazonFeedback();
                $amazon_feedback->setTable($table);
                $where = array();
                $where['rating_text'] = $feedback['ratingData']['text']['expandedText'];
                $where['rater'] = $feedback['rater'];
                $where['rating'] = $feedback['rating'];
                $amazon_feedback = $amazon_feedback->where($where)->get();

                if ($amazon_feedback->isEmpty())
                {
                    $amazon_feedback = new AmazonFeedback();
                    $amazon_feedback->setTable($table);
                    $amazon_feedback->created_at = date("Y-m-d h:i:s");
                }
                else
                    $amazon_feedback = $amazon_feedback->first();
                $feedback['rating']     =   2;
                if($amazon_feedback->rating != $feedback['rating'] && ($feedback['rating'] <=3 ))
                {
                    self::sendLowFeedbackEmail($feedback);
                }
                $amazon_feedback->setting_id = $this->settings['id'];
                $amazon_feedback->rater = $feedback['rater'];
                $amazon_feedback->rating = $feedback['rating'];
                $amazon_feedback->rating_text = $feedback['ratingData']['text']['expandedText'];
                $amazon_feedback->responseRatingData = $feedback['responseRatingData'];
                $amazon_feedback->hasResponse = $feedback['hasResponse'];
                $amazon_feedback->rating_date = $feedback['ratingData']['date'];
                $amazon_feedback->updated_at = date("Y-m-d h:i:s");
                $amazon_feedback->save();
            }
            catch (Exception $exp)
            {
                echo $exp->getMessage();exit;
            }
        }

    }
    private function sendLowFeedbackEmail(array $feedback=array())
    {
        $settings   =   $this->settings;
        $user       =   User::where("profile_id","=",$settings['profile_id'])->get();

        if(!empty($feedback) && (!$user->isEmpty()))
        {
            $feedback_data  =   array();

            if(isset( $feedback['rating']))
                $feedback_data['rating'] =   $feedback['rating'];
            else
                $feedback_data['rating']= "";

            if(isset( $feedback['rater']))
                $feedback_data['rater'] =   $feedback['rater'];
            else
                $feedback_data['rater']= "";

            if(isset( $feedback['date']))
                $feedback_data['date']= $feedback['date'];
           else
               $feedback_data['date']= "";

           if(isset($feedback['ratingData']['text']['expandedText']))
                $feedback_data['rating_text'] =   $feedback['ratingData']['text']['expandedText'];

            else
                $feedback_data['rating_text'] =  "";



            $user   =   $user->first();
            $user   =   $user->toArray();
            $user_email     =   $user['email'];

                Mail::send('emails.low_feedback_rating', ['settings' =>$settings, 'user' => $user,"feedback"=>$feedback_data], function ($message) use ($user_email) {

                    $message
                        ->to($user_email)
                        ->cc('attaulmohsin@gmail.com')
                        ->cc('namdar.tariq@vaival.com')
                        ->from('attaulmohsin@gmail.com')
                        ->subject('email low feedback rating');
                });
        }

    }
    public static function getFeedbacks($profile_id,$params=array())
    {

        try
        {
            $result = array();
            $count =    0;
            $no_of_rec      = $params['length'];
            $search_cols    =   $params['columns'];
            $account_where  =   $search_cols[0]['search']['value'];
            $rating_where  =   $search_cols[2]['search']['value'];
            $status_where  =   $search_cols[3]['search']['value'];


            $skip = $params['start'];
            $i = $skip + 1;
            $table = "amazon_feedbacks_" . $profile_id;
            if (Schema::hasTable($table))
            {
                $default_setting = UserAmazonSetting::getUsersDefaultSettingsIds("marketplace", $profile_id);
                $amazon_feedback = new AmazonFeedback();
                $amazon_feedback->setTable($table);
                $amazon_feedback = $amazon_feedback->where(array("setting_id" => $default_setting[0]['id']));
                if(!empty($rating_where))
                    $amazon_feedback    =   $amazon_feedback->whereRaw("round(rating) = '".$rating_where."'");
                if(!empty($status_where))
                    $amazon_feedback    =   $amazon_feedback->where("status","=",$status_where);

                $count = $amazon_feedback->count();
                $amazon_feedback = $amazon_feedback->take($no_of_rec);
                $amazon_feedback->skip($skip);

                $feedbacks = $amazon_feedback->get();
                if (!$feedbacks->isEmpty())
                {
                    $feedbacks = $feedbacks->toArray();

                    foreach ($feedbacks as $feedback)
                    {
                        $link   =   "<a href='".url('feedback-detail/'.$feedback['id'])."' >".$feedback['rater']."</a>";
                        $result[] = array("" . $i++,
                            $link,
                         //   $feedback['rating_date'],
                            $feedback['rating'],
                            $feedback['status'],
                            $feedback['rating_text']);
                    }
                }

            }

        }
        catch(\Exception $exp)
        {
            $result=array();
        }
        $result['total'] = $count;
        $result['filtered'] = $count;
        return $result;
    }

    public static function getFeedbacksCount($no_of_days,$is_default="",$type="")
    {
        $count          =   0;
        try
        {
            $user = Session::get("user");
            $table_feedbacks = "amazon_feedbacks_" . $user['profile_id'];
            if(Schema::hasTable($table_feedbacks))
            {
                $setting = UserAmazonSetting::getUsersDefaultSettingsIds("marketplace", $user['profile_id'], "TRUE");
                $setting = $setting[0];
                $feedbacks = new AmazonFeedback();
                $feedbacks->setTable($table_feedbacks);
                $feedbacks = $feedbacks->where(array("setting_id" => $setting['id']))
                    ->whereBetween('created_at', array(Carbon::now()->subDays($no_of_days), Carbon::now()));
                if(!empty($type))
                    if(strcmp($type,"NEGATIVE")==0)
                        $feedbacks = $feedbacks->where('rating','<=','3');
                    else if(strcmp($type,"POSITIVE")==0)
                        $feedbacks = $feedbacks->where('rating','>','3');

                $count = $feedbacks->count();
            }
        }
        catch(\Exception $exp)
        {
            $count=0;
        }
        return $count;
    }

}
