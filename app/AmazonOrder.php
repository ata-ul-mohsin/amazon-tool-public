<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;
use Session;

class AmazonOrder extends Model
{
    private $user;
    private $mws_settings;
    private $created_before;
    public function __construct($user="", $settings="")
    {
        $this->user = $user;
        $this->mws_settings = $settings;
    }
    public function fetchAmazonOrders($next_token = "")
    {
        if (empty($next_token))
        {
            $orders = \CommonHelper::sendMWSRequest("list_orders", $this->mws_settings);
        }
        else
        {
            $orders = \CommonHelper::sendMWSRequest("list_orders_by_next_token", $this->mws_settings, $next_token);
        }

        if (!empty($orders))
        {
            $orders_list = array();
            if (isset($orders['Orders']))
            {
                $orders_list = $orders['Orders'];

                $orders_list = $orders_list['FieldValue'];
                unset($orders['Orders']);
                if (isset($orders['CreatedBefore']['FieldValue']))
                    $this->created_before = date("Y-m-d h:i:s", strtotime($orders['CreatedBefore']['FieldValue']));
                else
                    $this->created_before = date("Y-m-d h:i:s");
                $this->saveBulkOrders($orders_list);
                if (isset($orders['NextToken']['FieldValue']) && !empty($orders['NextToken']['FieldValue']))
                {
                    $this->fetchAmazonOrders($orders['NextToken']['FieldValue']);
                }
                else
                {
                    return TRUE;
                }
            }
        }
    }
    private function saveBulkOrders(array $orders)
    {

        try
        {
            $table = \CommonHelper::find_or_create_table($this->user['profile_id'], "orders");
            $error_count = 0;
            if (!empty($orders) && $table)
            {
                $start_line = PHP_EOL . "===================================Orders data START[" . date('d-F-Y h:i:s') . "]===================================" . PHP_EOL;
                \CommonHelper::saveLog("orders", $start_line);
                $end_line = PHP_EOL . "==========================================END[" . date('d-F-Y h:i:s') . "]==========================================" . PHP_EOL;
                foreach ($orders as $order)
                {
                    //106-8239254-4397011
                    \CommonHelper::saveLog("orders", $order);

                    try
                    {
                        $order = (array)($order);
                        $order = reset($order);
                        if (!empty($order['BuyerEmail']['FieldValue']) && !empty($order['AmazonOrderId']['FieldValue']))
                        {
                            $where= array();
                            $where["order_id"] = $order['AmazonOrderId']['FieldValue'];;
                            $amazon_order = new AmazonOrder($this->user, $this->mws_settings);
                            $amazon_order->setTable($table);

                            $amazon_order   =   $amazon_order->where($where)->get();

                            if (!$amazon_order->isEmpty())
                            {
                                $amazon_order   =   $amazon_order->first();
                            }
                            else
                            {
                                $amazon_order = new AmazonOrder($this->user, $this->mws_settings);
                                $amazon_order->created_at = date("Y-m-d h:i:s");
                            }
                            $amazon_order                           =   $amazon_order->setTable($table);
                            $amazon_order->earliest_ship_date       =   $order['EarliestShipDate']['FieldValue'];
                            $amazon_order->latest_ship_date         =   $order['LatestShipDate']['FieldValue'];
                            $amazon_order->purchase_date            =   $order['PurchaseDate']['FieldValue'];
                            $amazon_order->fulfillment_channel      =   $order['FulfillmentChannel']['FieldValue'];
                            $amazon_order->marketplace_id           =   $order['MarketplaceId']['FieldValue'];

                            if(!empty($order['ShippingAddress']['FieldValue']))
                            {
                                $shipping_add          =   $order['ShippingAddress']['FieldValue'];
                                $shipping_add          =   (array)($shipping_add);
                                $shipping_add          =   reset($shipping_add);

                                $amazon_order->shipping_name            =   $shipping_add['Name']['FieldValue'];
                                $amazon_order->shipping_address         =   $shipping_add['AddressLine1']['FieldValue'];
                                $amazon_order->shipping_city            =   $shipping_add['City']['FieldValue'];
                                $amazon_order->shipping_state           =   $shipping_add['StateOrRegion']['FieldValue'];
                                $amazon_order->shipping_postal_code     =   $shipping_add['PostalCode']['FieldValue'];
                                $amazon_order->shipping_country         =   $shipping_add['CountryCode']['FieldValue'];
                                $amazon_order->shipping_phone           =   $shipping_add['Phone']['FieldValue'];

                            }
                            $amazon_order->order_id         =       $order['AmazonOrderId']['FieldValue'];
                            $amazon_order->buyer_name       =       $order['BuyerName']['FieldValue'];
                            $amazon_order->buyer_email      =       $order['BuyerEmail']['FieldValue'];
                            $amazon_order->status           =       $order['OrderStatus']['FieldValue'];
                            $amazon_order->setting_id       =       $this->mws_settings['id'];
                            $amazon_order->created_before   =       $this->created_before;
                            $amazon_order->updated_at       =       date("Y-m-d h:i:s");
                            $amazon_order->save();
                            //self::create_outbox_entry($amazon_order);
                        }
                    }
                    catch (\Exception $exp)
                    {
                        echo $exp->getMessage();
                        echo "<br/><br/>========================================<br/><br/>";
                        \CommonHelper::debug($amazon_order);
                        echo "<br/><br/>========================================<br/><br/>";
                    }

                }
                \CommonHelper::saveLog("orders", $end_line);
            }


        }
        catch (\Exception $exp)
        {
            echo $exp->getMessage();
            exit;
        }

    }
    private static function create_outbox_entry(AmazonOrder $order)
    {
        $order  =   $order->toArray();
        \CommonHelper::debug($order,true);
    }
    public static function getOrdersForMessages($profile_id, $message_id)
    {
        $orders = array();
        try
        {
            $default_setting = UserAmazonSetting::getUsersDefaultSettingsIds("marketplace", $profile_id);
            if (!empty($default_setting))
            {
                $default_setting = reset($default_setting);
                $default_setting = $default_setting['id'];
                $orders = BlacklistEmail::getMessageEmailList($profile_id, $message_id, $default_setting);
            }
        }
        catch (\Exception $exp)
        {
            echo $exp->getMessage();
            $orders = array();
        }
        return $orders;
    }
    public static function getOrdersForProducts($profile_id, $setting_id)
    {
        $orders = array();
        try
        {
            $table  =   "amazon_orders_".$profile_id;
            if(Schema::hasTable($table))
            {
                $where  =   array();
                $where['setting_id']    =   $setting_id;
                $orders = \DB::table($table)
                 ->where($where)
                 ->whereNull('products_orders.order_id')
                 ->join("products_orders",$table.".order_id","=","products_orders.order_id","left")
                 ->get([$table.".order_id"]);
            }
        }
        catch (\Exception $exp)
        {
            echo $exp->getMessage();
            exit;
        }
        return $orders;
    }
    public function setting()
    {
        return $this->belongsTo("App\UserAmazonSetting", "setting_id", "id");
    }
    public function products()
    {
        return $this->belongsToMany('App\AmazonProduct', 'products_orders', 'order_id', 'order_id');
    }
    public static function getOrdersCount($no_of_days,$is_default="TRUE")
    {
        $count          =   0;
        try
        {
            $user = Session::get("user");
            $table_orders = "amazon_orders_" . $user['profile_id'];
            if(Schema::hasTable($table_orders))
            {
                $setting = UserAmazonSetting::getUsersDefaultSettingsIds("marketplace", $user['profile_id'], "TRUE");
                $setting = $setting[0];
                $orders = new AmazonOrder();
                $orders->setTable($table_orders);
                $orders = $orders->where(array("setting_id" => $setting['id']))
                    ->whereBetween('created_at', array(Carbon::now()->subDays($no_of_days), Carbon::now()));
                $count = $orders->count();
            }
        }
        catch(\Exception $exp)
        {
            $count=0;
        }
        return $count;
    }
    public static function getOrdersBySetting($params = array(), $profile_id = "", $setting_id = "")
    {
        try
        {

            $search_cols = $params['columns'];
            $setting_id = $search_cols[0]['search']['value'];
            $count = 0;

            if (empty($profile_id))
            {
                $user = Session::get("user");
                $profile_id = $user['profile_id'];

            }
            $setting_where = array();
            $setting_where['profile_id'] = $profile_id;
            $setting_where['setting_type'] = "marketplace";

            if (empty($setting_id))
            {
                $setting_where['is_default'] = "TRUE";

            }
            else
            {
                $setting_where['id'] = $setting_id;
            }

            $default_setting = UserAmazonSetting::where($setting_where)
                ->get();

            if (!$default_setting->isEmpty())
            {
                $default_setting = $default_setting->first();
                $default_setting = $default_setting->toArray();
                $setting_id = $default_setting['id'];
            }

            if (!empty($profile_id) && !empty($default_setting['id']))
            {
                $orders = array();
                $no_of_rec = $params['length'];
                $global_search = $params['search']['value'];
                $skip = $params['start'];
                $i = $skip + 1;


                $table  =   "amazon_orders_".$profile_id;
                if(Schema::hasTable($table))
                {
                    $where  =   array();
                    $where['setting_id']    =   $default_setting['id'];

                    $orders_data = \DB::table($table)
                                ->where($where);
                    if (!empty($global_search))
                    {
                        $orders_data = $orders_data->where(function ($query) use ($global_search) {
                            $query->orWhere("order_id", "like", "%" . $global_search . "%");
                            $query->orWhere("buyer_email", "like", "%" . $global_search . "%");
                            $query->orWhere("buyer_name", "like", "%" . $global_search . "%");
                        });
                    }
                    $count          = $orders_data->count();
                    $orders_data    = $orders_data->take($no_of_rec);
                    $orders_data    = $orders_data->skip($skip);
                    $orders_data    = $orders_data->get();

                    if (!empty($orders_data))
                    {
                        foreach ($orders_data as $order)
                        {
                            $order  =   (array)$order;
                            $orders[] = array($i++,
                                $order['order_id'],
                                $order['buyer_name'],
                                $order['buyer_email'],
                                $order['fulfillment_channel'],
                                $order['status'],
                                $order['shipping_country']);
                        }
                    }
                }
            }
        }
        catch (Exception $exp)
        {
            echo $exp->getMessage();
            exit;
        }
        $orders['total']    = $count;
        $orders['filtered'] = $count;
        return $orders;
    }
    private static function getTotalPrice($price_data = array())
    {
        $total_price    =   0.0;
        if(!empty($price_data) && is_array($price_data))
        {

            if(!empty($price_data['Component']) && is_array($price_data['Component']))
            {

                $price_components   =   $price_data['Component'];
                foreach($price_components as $component)
                {
                    if(!empty($component['Amount']))
                        $total_price += $component['Amount'];
                }
            }
         }
        return $total_price;
    }
    public static function saveReportOrders(array $orders=array(),array $settings=array())
    {
        if(!empty($orders) && !empty($settings))
        {
            $table = \CommonHelper::find_or_create_table($settings['profile_id'], "orders");
            $error_count = 0;
            if ($table)
            {
                foreach ($orders as $order)
                {
                    try
                    {
                        $order = $order['Order'];;
                        if (!empty($order['AmazonOrderID']))
                        {
                            $where = array();
                            $where["order_id"] = $order['AmazonOrderID'];
                            $amazon_order = new AmazonOrder();
                            $amazon_order->setTable($table);

                            $amazon_order = $amazon_order->where($where)->get();

                            if (!$amazon_order->isEmpty())
                            {
                                $amazon_order = $amazon_order->first();
                            }
                            else
                            {
                                $amazon_order = new AmazonOrder();
                                $amazon_order->created_at = date("Y-m-d h:i:s");
                                $amazon_order->setting_id = $settings['id'];
                            }
                            $amazon_order = $amazon_order->setTable($table);

                            if (!empty($order['AmazonOrderID']))
                                $amazon_order->order_id = $order['AmazonOrderID'];
                            if (!empty($order['MerchantOrderID']))
                                $amazon_order->merchant_order_id = $order['MerchantOrderID'];
                            if (!empty($order['PurchaseDate']))
                                $amazon_order->purchase_date = date('Y-m-d',strtotime($order['PurchaseDate']));
                            if (!empty($order['LastUpdatedDate']))
                                $amazon_order->last_updated_date = date('Y-m-d',strtotime($order['LastUpdatedDate']));
                            if (!empty($order['OrderStatus']))
                                $amazon_order->status = $order['OrderStatus'];
                            if (!empty($order['SalesChannel']))
                                $amazon_order->sales_channel = $order['SalesChannel'];
                            if (!empty($order['FulfillmentData']['FulfillmentChannel']))
                                $amazon_order->fulfillment_channel = $order['FulfillmentData']['FulfillmentChannel'];
                            if (!empty($order['FulfillmentData']['ShipServiceLevel']))
                                $amazon_order->ship_service_level = $order['FulfillmentData']['ShipServiceLevel'];
                            if (!empty($order['FulfillmentData']['Address']['City'] ))
                                $amazon_order->shipping_city = $order['FulfillmentData']['Address']['City'];
                            if (!empty($order['FulfillmentData']['Address']['State'] ))
                                $amazon_order->shipping_state = $order['FulfillmentData']['Address']['State'];
                            if (!empty($order['FulfillmentData']['Address']['PostalCode'] ))
                                $amazon_order->shipping_postal_code = $order['FulfillmentData']['Address']['PostalCode'];
                            if (!empty($order['FulfillmentData']['Address']['Country'] ))
                                $amazon_order->shipping_country = $order['FulfillmentData']['Address']['Country'];
                            if (!empty($order['OrderItem']['ASIN']))
                                $amazon_order->asin = $order['OrderItem']['ASIN'];
                            if (!empty($order['OrderItem']['SKU']))
                                $amazon_order->sku = $order['OrderItem']['SKU'];
                            if (!empty($order['OrderItem']['ItemStatus']))
                                $amazon_order->item_status = $order['OrderItem']['ItemStatus'];
                            if (!empty($order['OrderItem']['ProductName']))
                                $amazon_order->product_name = $order['OrderItem']['ProductName'];
                            if (!empty($order['OrderItem']['Quantity']))
                                $amazon_order->quantity = $order['OrderItem']['Quantity'];
                            if (!empty($order['OrderItem']['ItemPrice']))
                                $amazon_order->item_price = self::getTotalPrice($order['OrderItem']['ItemPrice']);


                            $amazon_order->updated_at    =    date("Y-m-d h:i:s");
                            $amazon_order->save();

                        }

                    }
                    catch(Exception $exp)
                    {
                        continue;
                    }

                }
            }

        }
    }
}