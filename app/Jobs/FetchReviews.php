<?php

namespace App\Jobs;

use App\AmazonProduct;
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class FetchReviews extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    private $data;

    /**
     * Create a new job instance.
     * $data parameter receives  $data=array( Array([id] =>"",
     *                                              [asin] => "",
     *                                              [marketplace] => "",
     *                                              [reviews_link] =>"")
     *
     * @return void
     */
    public function __construct($data = array())
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data))
        {
            $data = AmazonProduct::getRatingAndReviewUrlData($this->data);
            if (!empty($data) && is_array($data))
            {
                foreach ($data as $product)
                {
                    $amz_product = AmazonProduct::find($product['id']);

                    $amz_product->reviews_link = $product['reviews_link'];

                    $rating_review_data = AmazonProduct::getRatingReviews($amz_product->reviews_link);

                    $amz_product->reviews = $rating_review_data['reviews'];
                    $amz_product->ratings = $rating_review_data['rating'];
                    $amz_product->save();
                }
            }
        }
    }

}