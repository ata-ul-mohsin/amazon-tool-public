<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\AmazonRequest;
use App\FindOpportunity;

class getProducts extends Job implements SelfHandling, ShouldQueue {

    use InteractsWithQueue,
        SerializesModels;

    public $node = array();
    public $marketPlace;
    public $data = array();
    /**
     * Create a new job instance.
     * @param  User $user
     * @return void
     */
    public function __construct($node, $marketPlace = 'webservices.amazon.com', $data) {
        $this->node = $node;
        $this->marketPlace = $marketPlace;
        $this->data = $data;

    }

    /**
     * Execute the job.
     *
     * @param  Mailer $mailer
     * @return void
     */
    public function handle() {

        $this->data['browsnode_id'] = $this->node->browsnode_id;
        $this->data['child_browsnode_id'] = $this->node->child_browsnode_id;

        for ($i = 1; $i <= 10; $i++) {
            $params = array(
                "Service" => "AWSECommerceService",
                "Operation" => "ItemSearch",
                "AWSAccessKeyId" => env('SES_KEY'),
                "AssociateTag" => env('SES_TAG'),
                "SearchIndex" => $this->node->search_index,
                "ResponseGroup" => "ItemAttributes,OfferFull,Reviews,SalesRank",
                "Sort" => "salesrank",
                "BrowseNode" => $this->data['child_browsnode_id'],
                "ItemPage" => $i
            );

            $res = AmazonRequest::curl($params, $this->marketPlace);
            FindOpportunity::productsToDB($res, $this->data, $this->marketPlace );
        }
        
    }
    
    
    
    

}
