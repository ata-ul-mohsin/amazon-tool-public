<?php

namespace App\Jobs;

use App\User;
use App\Jobs\Job;
use App\UserAmazonSetting;
use App\AmazonOrder;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class FetchAmazonOrders extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $user;
    private $setting;

    /**
     * Create a new job instance.
     * @param  user amazon setting id
     * @return void
     */
    public function __construct($setting_id)
    {
        $this->setting = $setting_id;
    }

    /**
     * Execute the job.
     * @param  Mailer $mailer
     * @return void
     */

    public function handle()
    {
        try
        {
            $mws_settings = UserAmazonSetting::find($this->setting);
            if ($mws_settings)
            {
                $mws_settings->user = $mws_settings->user()->get();
                $mws_settings->region = $mws_settings->region()->get();
                if ($mws_settings->region)
                {

                    $region = $mws_settings->region;
                    $region = $region->first();
                    $mws_settings->region = $region;
                    $marketplace = $region->marketplace()->get();

                    if ($marketplace)
                    {
                        $mws_settings->marketplace = $marketplace->first();
                    }
                }

                $mws_settings = \CommonHelper::objToArray($mws_settings);

                if (isset($mws_settings['user']))
                {
                    $mws_user = $mws_settings['user'][0];
                    unset($mws_settings['user']);
                    $order = new AmazonOrder($mws_user, $mws_settings);
                    $order->fetchAmazonOrders();
                }

            }
        }
        catch (Exception $ex)
        {

        }

    }
}