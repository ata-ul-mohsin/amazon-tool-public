<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\UserAmazonSetting;
use App\AmazonProduct;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;


class FetchProductsAgainstOrders extends Job implements SelfHandling, ShouldQueue
{
    private $profile_id;
    private $setting_id;
    private $orders;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($profile_id, $setting_id, $orders)
    {
        $this->profile_id = $profile_id;
        $this->setting_id = $setting_id;
        $this->orders = $orders;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $settings_data = UserAmazonSetting::getSettingForMWSRequest($this->profile_id, $this->setting_id);
        if (!empty($settings_data['user']) &&
            !empty($settings_data['region']) &&
            !empty($settings_data['marketplace'])
        )
        {
            $user = $settings_data['user'];
            unset($settings_data['user']);
            foreach ($this->orders as $order)
            {
                $product = new AmazonProduct($user, $settings_data);
                $product->fetchProductsFromOrder($order);
            }

        }
    }
}
