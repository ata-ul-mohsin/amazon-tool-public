<?php

namespace App\Jobs;

use App\AmazonFeedback;
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class FetchFeedbacks extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $setting_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($setting_id)
    {
        $this->setting_id   =   $setting_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $amazon_feedback    =   new AmazonFeedback($this->setting_id);
        $amazon_feedback->getFeedbacksAgainstSellerId();
    }

}
