<?php

namespace App\Jobs;

use App\User;
use App\Jobs\Job;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class SendReminderEmail extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $profile_id;
    private $message_id;
    private $order_id;

    /**
     * Create a new job instance.
     * @param  User $user
     * @return void
     */

    public function __construct($profile_id, $message_id, $order_id)
    {
        $this->profile_id   =   $profile_id;
        $this->message_id   =   $message_id;
        $this->order_id     =   $order_id;
    }

    /**
     * Execute the job.
     *
     * @param  Mailer $mailer
     * @return void
     */

    public function handle(Mailer $mailer)
    {
        /* try
         {*/
        $emails =array();
        $emails[]=  "anjumvip@gmail.com";
        $emails[]=  "namdar.tariq@vaival.com";
        $emails[]=  "mubashirluqman732@gmail.com";
        $counter=0;
        $email  =    $emails[0];

        for($i=0; $i<600; $i++)
        {
            if ($i % 150 == 0)
                $email = $emails[$counter++];

            Mail::send('emails.reminder', ['user' => $this->user], function ($message) use ($email)
            {

                $message
                    ->to($email)
                    ->from('attaulmohsin@gmail.com')
                    ->subject('email ses testing');


            });
        }

        /* }
         catch(\Exception $exp)
         {
             echo $exp->getMessage();
         }*/

        /*  $this->user->reminders()->create(...);*/
    }
}