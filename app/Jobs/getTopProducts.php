<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\AmazonRequest;
use App\FindOpportunity;

class getTopProducts extends Job implements SelfHandling, ShouldQueue {

    use InteractsWithQueue,
        SerializesModels;

    public $asinAr = array();
    public $marketPlace;
    public $data = array();
    /**
     * Create a new job instance.
     * @param  User $user
     * @return void
     */
    public function __construct($asinAr, $marketPlace = 'webservices.amazon.com', $data) {
        $this->asinAr = $asinAr;
        $this->marketPlace = $marketPlace;
        $this->data = $data;

        
//        foreach ($this->asinAr as $asin) {
//            $this->data['browsnode_id'] = $asin['browsnode_id'];
//            $this->data['child_browsnode_id'] = $asin['child_browsnode_id'];
//            
//            $params = array(
//                "Service" => "AWSECommerceService",
//                "Operation" => "ItemLookup",
//                "AWSAccessKeyId" => env('SES_KEY'),
//                "AssociateTag" => env('SES_TAG'),
//                "ItemId" => $asin['asin_string'],
//                "IdType" => "ASIN",
//                "ResponseGroup" => "ItemAttributes,OfferFull,Reviews,SalesRank"
//            );
//
//            $res = AmazonRequest::curl($params, $this->marketPlace);
//            FindOpportunity::productsToDB($res, $this->data, $this->marketPlace );
//        }
        
        
    }

    /**
     * Execute the job.
     *
     * @param  Mailer $mailer
     * @return void
     */
    public function handle() {

        foreach ($this->asinAr as $asin) {
            $this->data['browsnode_id'] = $asin['browsnode_id'];
            $this->data['child_browsnode_id'] = $asin['child_browsnode_id'];
            
            $params = array(
                "Service" => "AWSECommerceService",
                "Operation" => "ItemLookup",
                "AWSAccessKeyId" => env('SES_KEY'),
                "AssociateTag" => env('SES_TAG'),
                "ItemId" => $asin['asin_string'],
                "IdType" => "ASIN",
                "ResponseGroup" => "ItemAttributes,OfferFull,Reviews,SalesRank"
            );

            $res = AmazonRequest::curl($params, $this->marketPlace);
            FindOpportunity::productsToDB($res, $this->data, $this->marketPlace );
        }
        
    }
    
    
    
    

}
