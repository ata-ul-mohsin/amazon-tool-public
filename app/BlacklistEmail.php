<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\View;
class BlacklistEmail extends Model
{
    public static function getBlacklistEmails($profile_id)
    {
        $result  =   array();
        try
        {
            $blacklistemails    =   BlacklistEmail::where(array("profile_id"=>$profile_id))
                                    ->with("settings")
                                    ->orderBy("created_at","desc")
                                    ->get();
            $result   =    $blacklistemails->toArray();
        }
        catch(\Exception $exp)
        {

        }
        return $result;
    }
    public static function saveToDb($data = array())
    {
        $result = array("status" => '3', "message" => " Error in saving data! ");

       try
       {
           $already_black_listed    =       BlacklistEmail::where(array('profile_id' => $data['profile_id'],'setting_id' => $data['setting_id']));

           if(!empty($data['id']))
           {
               $already_black_listed    =   $already_black_listed->where("id",'!=',$data['id']);
               $already_black_listed    =   $already_black_listed->where("email",'=',$data['email']);
               $already_black_listed    =   $already_black_listed->get();
               if(!$already_black_listed->isEmpty())
               {
                   $result['message']   =   "This email has already been blacklisted against this account. Please select another account.";
                   return $result;
               }
               $blacklist_email  =    BlacklistEmail::where(array('profile_id' => $data['profile_id'],'setting_id' => $data['setting_id'], 'id' => $data['id']))->get();
               if($blacklist_email->isEmpty())
               {
                   $result['message']   =   "Record Not found in the system.";
                   return $result;
               }
               $blacklist_email =   $blacklist_email->first();
           }
           else
           {
               $already_black_listed    =   $already_black_listed->where("email",'=',$data['email']);
               $already_black_listed    =   $already_black_listed->get();
               if(!$already_black_listed->isEmpty())
               {
                   $result['message']   =   "This email has already been blacklisted against this account. Please select another account.";
                   return $result;
               }
               $blacklist_email = new BlacklistEmail();
               $blacklist_email->created_at = date("Y-m-d h:i:s");

           }




           $blacklist_email->updated_at = date("Y-m-d h:i:s");


           if (isset($data['email']))
               $blacklist_email->email = $data['email'];


           if (isset($data['setting_id']))
               $blacklist_email->setting_id = $data['setting_id'];

           if (isset($data['profile_id']))
               $blacklist_email->profile_id = $data['profile_id'];


           if ($blacklist_email->save())
           {
               $result ["status"] = '1';
               $result ["message"] = " Successfully saved to database. ";
           }
       }
       catch(\Exception $exp)
       {

       }
        return $result;
    }

    public function settings()
    {
        return $this->hasOne("App\UserAmazonSetting","id","setting_id");
    }

    public static function getMessageEmailList($profile_id,$message_id,$setting_id)
    {
        $message_email_list = array();
        try
        {
            $order_table = "amazon_orders_" . $profile_id;
            if (Schema::hasTable($order_table))
            {
                \DB::enableQueryLog();
                $message = Message::find($message_id);
                $message = $message->toArray();
                //$date_where = Message::getDateWhere($message['when_send_to_buyer'],"purchase_date");
                $email_black_list = self::getEmailBlackList($profile_id, $setting_id, $message_id);

                $message_email_list = \DB::table($order_table)
                                        ->where("setting_id", "=", $setting_id)
                                        ->whereNotIn('buyer_email', $email_black_list)
                                        ->whereNotNull('buyer_email');
                //  ->whereRaw($date_where)
                if (!empty($message['order_placed_on_or_after']) && strcmp($message['order_placed_on_or_after'], "0000-00-00 00:00:00") != 0)
                {
                    $message_email_list = $message_email_list->whereRaw("DATE(purchase_date) >= '".date("Y-m-d",strtotime($message['order_placed_on_or_after']))."'");
                }
                if (!empty($message['order_placed_before']) && strcmp($message['order_placed_before'], "0000-00-00 00:00:00") != 0)
                {
                    $message_email_list = $message_email_list->whereRaw("DATE(purchase_date) < '".date("Y-m-d",strtotime($message['order_placed_before']))."'");
                }
                if (!empty($message['shipping_country']))
                {
                    $shipping_countries = explode(",", $message['shipping_country']);
                    $message_email_list = $message_email_list->whereIn("shipping_country", $shipping_countries);
                }
                if (!empty($message['shipping_country_is_not']))
                {
                    $non_shipping_countries = explode(",", $message['shipping_country_is_not']);
                    $message_email_list = $message_email_list->whereNotIn("shipping_country", $non_shipping_countries);
                }
                if (!empty($message['status']))
                {
                    $message_email_list = $message_email_list->where("status", $message['status']);
                }
                if (!empty($message['fulfillment_channel']))
                {
                    switch($message['fulfillment_channel'])
                    {
                        case "FBA":
                            $message_email_list = $message_email_list->whereIn("fulfillment_channel", array("FBA","Merchant"));
                                break;
                        case "Merchant":
                            $message_email_list = $message_email_list->whereNotIn("fulfillment_channel", array("FBA","Merchant"));
                                break;
                        default:
                                break;
                    }
                }
                if (!empty($message['sku_filter']))
                {
                    $sku_filters = explode(",", $message['sku_filter']);
                    $message_email_list = $message_email_list->whereIn("sku", $sku_filters);
                }
                if (!empty($message['asin_filter']))
                {
                    $asin_filters = explode(",", $message['asin_filter']);
                    $message_email_list = $message_email_list->whereIn("asin", $asin_filters);
                }

                $message_email_list = $message_email_list->get();

                if(!empty($message_email_list))
                {
                    $emails =   array();
                    foreach($message_email_list as $message_email)
                    {
                        $emails[]   =   array('email'=>$message_email->buyer_email,
                                              'order_id'=>$message_email->order_id,
                                              'setting_id'=>$setting_id);
                    }
                    $message_email_list             =   $emails;
                    $message_email_list['message']  =   $message;
                }
                else
                    $message_email_list =   array();

            }
        }
        catch (\Exception $exp)
        {
            echo $exp->getMessage();

        }
        return $message_email_list;
    }

    private static function getEmailBlackList($profile_id,$setting_id)
    {
        $email_black_list   =   array();

        try
        {
            $setting_black_list = BlacklistEmail::where(array("blacklist_emails.setting_id"=>$setting_id,"blacklist_emails.profile_id"=>$profile_id))
                ->orWhere(array("blacklist_emails.setting_id"=>0,"blacklist_emails.profile_id"=>""))
                ->select("email")
                ->get();

            foreach ($setting_black_list as $email)
            {
                $email_black_list[] = $email['email'];
            }
            $email_black_list   =   array_unique($email_black_list);
        }
        catch(\Exception $exp)
        {
           echo $exp->getMessage(); exit;
        }
        return $email_black_list;
    }
    public static function deleteBlackListEmail(BlacklistEmail $bl_email)
    {
        try
        {
            $bl_email->delete();
            return TRUE;
        }
        catch (\Exception $ex)
        {

        }
        return FALSE;
    }
}
