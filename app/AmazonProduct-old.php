<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use MarketplaceWebServiceOrders_Model_Money;
use Sunra\PhpSimple\HtmlDomParser;
use Session;

class AmazonProduct extends Model
{
    private $mws_settings;
    private $user;

    public function __construct($user = "", $settings = "")
    {
        $this->user = $user;
        $this->mws_settings = $settings;
    }

    public function fetchProductsFromOrder($order_id, $next_token = "")
    {
        if (empty($next_token))
        {
            $products = \CommonHelper::sendMWSRequest("list_order_items", $this->mws_settings, "", $order_id);
        }
        else
        {
            $products = \CommonHelper::sendMWSRequest("list_order_items_by_next_token", $this->mws_settings, $next_token, $order_id);
        }
        if (isset($products['OrderItems']))
        {
            $products_list = $products['OrderItems']['FieldValue'];
            unset($products['Orders']);
            $this->saveBulkProducts($products_list, $order_id);

            if (isset($products['NextToken']['FieldValue']) && !empty($products['NextToken']['FieldValue']))
            {
                $this->fetchProductsFromOrder($order_id, $products['NextToken']['FieldValue']);
            }
            else
            {
                return TRUE;
            }
        }
    }

    private static function moneyObj_to_json(MarketplaceWebServiceOrders_Model_Money $data)
    {
        $result = array();
        $result['amount'] = (string)$data->getAmount();
        $result['currency_code'] = (string)$data->getCurrencyCode();
        $result = serialize($result);
        return $result;
    }

    private function saveBulkProducts(array $products, $order_id)
    {
        try
        {
            $error_count = 0;
            if (!empty($products))
            {
             //   $start_line = PHP_EOL . "===================================Products data START[" . date('d-F-Y h:i:s') . "]===================================" . PHP_EOL;
           //     \CommonHelper::saveLog("products_by_order", $start_line);
            //    $end_line = PHP_EOL . "==========================================END[" . date('d-F-Y h:i:s') . "]==========================================" . PHP_EOL;
                $setting_id = $this->mws_settings['id'];
                $where = array();
                $where['setting_id'] = $setting_id;

                foreach ($products as $product)
                {
              //      \CommonHelper::saveLog("products", $product);
                    $product = (array)($product);
                    $product = reset($product);

                    if (!empty($product['ASIN']['FieldValue']) && !empty($product['OrderItemId']['FieldValue']))
                    {
                        $where["asin"] = $product['ASIN']['FieldValue'];
                        $where["marketplace"] = $this->mws_settings['marketplace']['marketplace_id'];

                        $amazon_product = AmazonProduct::where($where)->get();

                        if ($amazon_product->isEmpty())
                        {
                            $amazon_product = new AmazonProduct($this->user, $this->mws_settings);
                            $amazon_product->setting_id = $setting_id;
                            $amazon_product->created_at = date("Y-m-d h:i:s");
                            $amazon_product->asin = $product['ASIN']['FieldValue'];
                            $amazon_product->marketplace = $this->mws_settings['marketplace']['marketplace_id'];
                        }
                        else
                            $amazon_product = $amazon_product->first();

                        $amazon_product->seller_sku = $product['SellerSKU']['FieldValue'];
                        $amazon_product->title = $product['Title']['FieldValue'];
                        $amazon_product->item_price = (($product['ItemPrice']['FieldValue'] instanceof MarketplaceWebServiceOrders_Model_Money) ? self::moneyObj_to_json($product['ItemPrice']['FieldValue']) : "");
                        $amazon_product->shipping_price = (($product['ShippingPrice']['FieldValue'] instanceof MarketplaceWebServiceOrders_Model_Money) ? self::moneyObj_to_json($product['ShippingPrice']['FieldValue']) : "");
                        $amazon_product->gift_wrap_price = (($product['GiftWrapPrice']['FieldValue'] instanceof MarketplaceWebServiceOrders_Model_Money) ? self::moneyObj_to_json($product['GiftWrapPrice']['FieldValue']) : "");
                        $amazon_product->shipping_discount = (($product['ShippingDiscount']['FieldValue'] instanceof MarketplaceWebServiceOrders_Model_Money) ? self::moneyObj_to_json($product['ShippingDiscount']['FieldValue']) : "");
                        $amazon_product->promotion_discount = (($product['PromotionDiscount']['FieldValue'] instanceof MarketplaceWebServiceOrders_Model_Money) ? self::moneyObj_to_json($product['PromotionDiscount']['FieldValue']) : "");
                        $amazon_product->order_item_id = $product['OrderItemId']['FieldValue'];

                        $amazon_product->updated_at = date("Y-m-d h:i:s");
                        $amazon_product->save();
                        $order_product_data = array();
                        $order_product_data['product_id'] = $amazon_product->id;
                        $order_product_data['order_id'] = $order_id;
                        $order_product = \DB::table('products_orders')->where($order_product_data)->get();
                        if (empty($order_product))
                        {
                            $order_product_data['created_at'] = date("Y-m-d h:i:s");
                            $order_product_data['updated_at'] = date("Y-m-d h:i:s");
                            $result = \DB::table('products_orders')->insert($order_product_data);

                        }

                    }
                }
             //   \CommonHelper::saveLog("orders", $end_line);
            }


        }
        catch (Exception $exp)
        {
            echo $exp->getMessage();
            exit;
        }

    }

    public static function getRatingAndReviewUrlData($asin_data, $response_group = "Reviews", $endpoint = "webservices.amazon.com")
    {
        try
        {
            $response = FALSE;
            if (is_array($asin_data))
            {
                $asin = "";
                foreach ($asin_data as $product)
                {
                    // if(empty($product['reviews_link'])) generate locally here
                    $asin .= $product['asin'] . ",";
                }
                if (!empty($asin))
                    $asin = substr($asin, 0, -1);
            }
            else
                $asin = $asin_data;


            if (!empty($asin))
            {

                $aws_secret_key = env('SES_SECRET');
                $params = array();
                $params['Service'] = "AWSECommerceService";
                $params['Operation'] = "ItemLookup";
                $params['ResponseGroup'] = $response_group;
                $params['IdType'] = "ASIN";
                $params['ItemId'] = $asin;
                $params['AssociateTag'] = env('ASSOCIATE_TAG');
                $params['AWSAccessKeyId'] = env('ACCESS_KEY_ID');
                $params['Timestamp'] = gmdate('Y-m-d\TH:i:s\Z');
                $params['Version'] = "2011-08-01";
                ksort($params);
                $canonical_query_string = "http://" . $endpoint . "/onca/xml" . '?';
                $temp_str = "";
                foreach ($params as $key => $value)
                {
                    $temp_str .= rawurlencode($key) . "=" . rawurlencode($value) . "&";

                }

                $string_to_sign = "GET\n" . $endpoint . "\n/onca/xml\n" . substr($temp_str, 0, -1);

                $signature = base64_encode(hash_hmac("sha256", $string_to_sign, $aws_secret_key, true));

                $canonical_query_string .= $temp_str;
                $canonical_query_string .= "Signature=" . rawurlencode($signature);
                $result = file_get_contents($canonical_query_string);
                $xml_data = json_decode(json_encode(simplexml_load_string(trim($result))), true);

                if (strcmp($response_group, "Reviews") == 0)
                {
                    if (isset($xml_data['Items']['Item']['CustomerReviews']['IFrameURL']) && (!is_array($asin_data)))
                        $response = $xml_data['Items']['Item']['CustomerReviews']['IFrameURL'];
                    elseif (isset($xml_data['Items']['Item'][0]['CustomerReviews']['IFrameURL']) && is_array($asin_data))
                    {
                        $items = $xml_data['Items']['Item'];
                        $reviews = array();
                        foreach ($items as $item)
                        {
                            $reviews[$item['ASIN']] = $item ['CustomerReviews']['IFrameURL'];
                        }
                        foreach ($asin_data as $key => $product)
                        {
                            if (!empty($reviews[$product['asin']]))
                                $asin_data[$key]['reviews_link'] = $reviews[$product['asin']];
                        }
                        $response = $asin_data;
                    }
                }
                else if (strcmp($response_group, "ItemAttributes") == 0)
                {
                    $response = $xml_data;

                }

            }

        }
        catch (\Exception $exp)
        {
            echo $exp->getMessage();
            exit;
        }
        return $response;
    }

    public static function getRatingReviews($url = '')
    {
        $rating = 'NA';
        $reviews = 'NA';
        $html = HtmlDomParser::file_get_html($url);
        $res = $html->find(".crIFrameHeaderHistogram", 0);
        if (isset($res))
        {
            $reviews = explode(' ', $html->find(".crIFrameHeaderHistogram", 0)->childNodes(0)->childNodes(0)->innertext)[0];
            $reviews = str_replace(',', '', $reviews);
        }
        foreach ($html->find('.asinReviewsSummary img') as $element)
        {
            if (isset($element->title))
            {
                $rating = explode(' ', $element->title)[0];
            }
        }

        return array('reviews' => $reviews, 'rating' => $rating);
    }

    public static function getProductsBySetting($is_branded = FALSE, $params = array(), $profile_id = "", $setting_id = "")
    {
        try
        {
            $products = array();
            $no_of_rec = $params['length'];
            $search_cols = $params['columns'];
            $setting_id = $search_cols[0]['search']['value'];
            $brand_id = $search_cols[1]['search']['value'];
            $global_search = $params['search']['value'];
            $skip = $params['start'];
            $i = $skip + 1;
            $count = 0;

            if (empty($profile_id))
            {
                $user = Session::get("user");
                $profile_id = $user['profile_id'];

            }
            $setting_where = array();
            $setting_where['profile_id'] = $profile_id;
            $setting_where['setting_type'] = "marketplace";

            if (empty($setting_id))
            {
                $setting_where['is_default'] = "TRUE";

            }
            else
            {
                $setting_where['id'] = $setting_id;
            }

            $default_setting = UserAmazonSetting::where($setting_where)
                ->get();

            if (!$default_setting->isEmpty())
            {
                $default_setting = $default_setting->first();
                $default_setting = $default_setting->toArray();
                $setting_id = $default_setting['id'];
            }


            if (!empty($profile_id) && !empty($setting_id))
            {

                $order_table = "amazon_orders_" . $profile_id;
                if (Schema::hasTable($order_table))
                {
                    $amazon_order = new AmazonOrder();
                    $amazon_order->setTable($order_table);
                    $orders = $amazon_order->where("setting_id", "=", $setting_id)->get(['id', 'order_id']);
                    if (!$orders->isEmpty())
                    {
                        $orders = $orders->toArray();

                        $order_ids = array();
                        $product_ids = array();
                        foreach ($orders as $order)
                        {
                            $order_ids[] = $order['order_id'];
                        }
                        $related_products = \DB::table("products_orders")
                            ->whereIn('order_id', $order_ids)
                            ->get(['product_id']);

                        if (!empty($related_products))
                        {
                            $related_products = json_decode(json_encode($related_products), true);
                            foreach ($related_products as $product)
                            {
                                $product_ids[] = $product['product_id'];
                            }
                            $product_ids = array_unique($product_ids);

                            $products_data = AmazonProduct::whereIn("id", $product_ids);


                            if ($is_branded)
                            {
                                $products_data = $products_data->whereNull("brand_id");
                            }
                            if (!empty($brand_id))
                            {
                                $products_data = $products_data->orWhere("brand_id", "=", $brand_id);
                            }
                            if (!empty($global_search))
                            {
                                $products_data->where("title", "like", "%" . $global_search . "%");
                            }

                            $count = $products_data->count();
                            $products_data = $products_data->take($no_of_rec);
                            $products_data = $products_data->skip($skip);
                            $products_data = $products_data->get(['id', 'asin', 'title', 'brand_id']);
                            if (!$products_data->isEmpty())
                            {
                                $products_data = $products_data->toArray();

                                foreach ($products_data as $product)
                                {
                                    $products[] = array("" . $i++,
                                        $product['asin'],
                                        $product['title'],
                                        self::getActionCheckHtml($product, $brand_id));
                                }
                            }
                        }
                    }

                }


            }
        }
        catch (Exception $exp)
        {
            echo $exp->getMessage();
            exit;
        }
        $products['total'] = $count;
        $products['filtered'] = $count;
        return $products;
    }

    private static function getActionCheckHtml($product = array(), $brand_id = "")
    {
        $checkbox_html = "";
        $is_checked = "";
        if (!empty($brand_id) && !empty($product['brand_id']) && $product['brand_id'] == $brand_id)
            $is_checked = "checked='checked'";

        if (!empty($product) && !empty($product['asin']))
        {
            $checkbox_html .= "<input type='checkbox' name='chk_product_" . $product['asin'] . "' " . $is_checked . " value='" . $product['id'] . "'>
                                <input type='hidden' name='product_" . $product['asin'] . "' value='" . $product['id'] . "'>";
        }
        return $checkbox_html;
    }

    public function orders()
    {
        return $this->belongsToMany('App\AmazonOrder', 'products_orders', 'id', 'product_id');

    }

    public function brands()
    {
        return $this->belongsTo('App\Brand', 'brand_id', 'id');

    }

    public static function trackProduct(array $data)
    {
        $result_to_return = array("status" => '3', "message" => " Error in tracking product! ");
        try
        {
            if (!empty($data['setting_id']) && !empty($data['asin']))
            {
                $marketplace = UserAmazonSetting::where("id", "=", $data['setting_id'])->with("region.marketplace")->get();
                $already_exists = AmazonProduct::where('asin', '=', $data['asin']);

                if (!$marketplace->isEmpty())
                {
                    $marketplace = $marketplace->first();
                    $marketplace = $marketplace->toArray();
                    $mkt_place = $marketplace['region']['marketplace'];
                    $already_exists = $already_exists->where('marketplace', '=', $mkt_place['marketplace_id']);
                }
                $already_exists = $already_exists->get();
                if (!$already_exists->isEmpty())
                {
                    $result_to_return['message'] = "This product is already present in the system.";
                    return $result_to_return;
                }
                $result = self::getRatingAndReviewUrlData($data['asin'], "ItemAttributes");

                if (isset($result['Items']))
                {

                    $product = new AmazonProduct();
                    $response = $result['Items'];
                    if (isset($response['Request']))
                    {
                        $request_response = $response['Request'];
                        if (isset($request_response['Errors']))
                        {
                            $error_response = $request_response['Errors'];
                            if (isset($error_response['Message']))
                            {
                                $error_msg = $error_response['Message'];
                                $result_to_return['message'] = $error_msg;
                                return $result_to_return;
                            }
                        }
                    }

                    if (isset($response['Item']))
                    {
                        $response = $response['Item'];

                        if (isset($response['ASIN']))
                            $product->asin = $response['ASIN'];
                        if (isset($response['ItemAttributes']))
                        {
                            $attributes = $response['ItemAttributes'];
                            if (!empty($attributes) && is_array($attributes))
                            {
                                if (!empty($attributes['Model']))
                                    $product->seller_sku = $attributes['Model'];
                                if (!empty($attributes['Title']))
                                    $product->title = $attributes['Title'];
                                if (!empty($attributes['ListPrice']))
                                {
                                    $price = $attributes['ListPrice'];
                                    if (!empty($price ['Amount']))
                                    {
                                        $price = $price['Amount'];
                                        if (!empty($price ['FormattedPrice']))
                                        {
                                            $product->item_price = $price['FormattedPrice'];
                                        }
                                    }
                                }


                            }
                        }
                        if (isset($response['ItemLinks']))
                        {
                            $links = $response['ItemLinks'];
                            if (isset($links['ItemLink']))
                            {
                                $links = $links['ItemLink'];
                                if (!empty($links) && is_array($links))
                                {
                                    foreach ($links as $link)
                                    {
                                        if (!empty($link['Description']) && !empty($link['URL']))
                                        {
                                            if (strcmp($link['Description'], "All Customer Reviews") == 0)
                                            {
                                                $product->reviews_link = $link['URL'];
                                                $reviews_and_ratings = self::getRatingReviews($product->reviews_link);
                                                if (!empty($reviews_and_ratings['reviews']))
                                                    $product->reviews = $reviews_and_ratings['reviews'];
                                                if (!empty($reviews_and_ratings['rating']))
                                                    $product->ratings = $reviews_and_ratings['rating'];
                                                break;
                                            }
                                        }
                                    }

                                }
                            }
                        }
                        if (isset($product->asin) &&
                            isset($product->title) &&
                            isset($product->seller_sku)
                        )
                        {
                            if (!empty($mkt_place['marketplace_id']))
                                $product->marketplace = $mkt_place['marketplace_id'];
                            $product->created_at = date("Y-m-d h:i:s");
                            $product->created_at = date("Y-m-d h:i:s");
                            $product->setting_id = $data['setting_id'];
                            $product->save();
                            $result_to_return['message'] = "Successfully tracked product";
                            $result_to_return['status'] = '1';
                        }
                    }

                }

            }
            else
            {
                $result_to_return['message'] = "Invalid request";
            }
        }
        catch (\Exception $exp)
        {
            $result_to_return['message'] = $exp->getMessage();
            $result_to_return['status'] = '3';
        }
        return $result_to_return;

    }

    public static function getProductsForDashboard( $params = array(),$is_branded = FALSE, $profile_id = "", $setting_id = "")
    {
        try
        {
            $products = array();
            $no_of_rec = $params['length'];
            $search_cols = $params['columns'];
            $rating = $search_cols[4]['search']['value'];

            $global_search = $params['search']['value'];
            $skip = $params['start'];
            $i = $skip + 1;
            $count = 0;

            if (empty($profile_id))
            {
                $user = Session::get("user");
                $profile_id = $user['profile_id'];
            }
            $setting_where = array();
            $setting_where['profile_id'] = $profile_id;
            $setting_where['setting_type'] = "marketplace";

            if (empty($setting_id))
            {
                $setting_where['is_default'] = "TRUE";
            }
            else
            {
                $setting_where['id'] = $setting_id;
            }

            $default_setting = UserAmazonSetting::where($setting_where)
                ->get();

            if (!$default_setting->isEmpty())
            {
                $default_setting = $default_setting->first();
                $default_setting = $default_setting->toArray();
                $setting_id = $default_setting['id'];
            }


            if (!empty($profile_id) && !empty($setting_id))
            {
                $products_data = AmazonProduct::where("setting_id", "=", $setting_id);
                if (!empty($global_search))
                {
                    $products_data->where("title", "like", "%" . $global_search . "%");
                }
                if (!empty($rating))
                {
                    $products_data->whereRaw("round(ratings) = '".$rating."'");
                }
                else
                    $products_data->orderBy("ratings","desc");

                $count = $products_data->count();
                $products_data = $products_data->with("brands");



                $products_data = $products_data->take($no_of_rec);
                $products_data = $products_data->skip($skip);
                $products_data = $products_data->get();
                if (!$products_data->isEmpty())
                {
                    $products_data = $products_data->toArray();

                    foreach ($products_data as $product)
                    {
                        if(isset($product['brands']['name']))
                            $brand    =   $product['brands']['name'];
                        else
                            $brand      =   "";

                        $products[]     = array($i++,
                                            $brand,
                                            substr($product['title'],0,15),
                                            $product['ratings'],
                                            $product['reviews']);

                    }
                }

            }


        }
        catch (Exception $exp)
        {
            echo $exp->getMessage();
            exit;
        }
        $products['total'] = $count;
        $products['filtered'] = $count;
        return $products;
    }
}