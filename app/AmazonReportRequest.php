<?php
/**AmazonReportRequest
 * Created by Atta-Ul-Mohsin.
 * Date: 2/10/2016
 * Time: 12:23 PM
 */

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;
use Session;
use Illuminate\Database\Eloquent\Collection;

class AmazonReportRequest extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function __construct()
    {
    }
    private static function getFormattedMWSSettings(Collection $settings)
    {
        $formatted_setting  =   array();

        if(!$settings->isEmpty())
        {

            foreach($settings as $amz_setting)
            {
                if(isset($amz_setting->generated_report_id))
                    $report_id        =   $amz_setting->generated_report_id;
                if(isset($amz_setting->request))
                    $request        =   $amz_setting->request;
                if(isset($amz_setting->request_id))
                    $request_id     =   $amz_setting->request_id;
                $amz_setting    =   $amz_setting->where("id","=",$amz_setting->id);
                $amz_setting    =   $amz_setting->with(array("region","region.marketplace"));
                $amz_setting    =   $amz_setting->get();
                if(!$amz_setting->isEmpty())
                {
                    $amz_setting                =   $amz_setting->first();
                    $amz_setting                =   $amz_setting->toArray();
                   if(!empty($request) && !empty($request_id))
                   {
                       $amz_setting['request_id'] = $request_id;
                       $amz_setting['request'] = $request;
                   }
                   if(!empty($report_id))
                   {
                       $amz_setting['report_id'] = $report_id;
                   }

                    if(isset($amz_setting['region']['marketplace']))
                    {
                        $amz_setting['marketplace'] =   $amz_setting['region']['marketplace'];
                        unset($amz_setting['region']['marketplace']);
                    }
                    $formatted_setting []   =   $amz_setting;
                }
           }

        }
        return $formatted_setting;
    }
    public static function sendReportRequests(Collection $settings)
    {
        $results    =   array('success'=>0,'failure'=>0);
        $mws_settings   =   self::getFormattedMWSSettings($settings);
        if(!empty($mws_settings))
        {
            foreach($mws_settings as $setting)
            {
                $response = \CommonHelper::sendMWSRequest("request_orders_report", $setting);
                if (is_array($response) && !empty($response))
                {
                    $response['setting_id'] = $setting['id'];
                    $response['profile_id'] = $setting['profile_id'];
                    if(self::saveReportRequestToDb($response))
                        $results['success']++;
                    else
                        $results['failure']++;
                }
            }
        }
        return $results;
    }

    public static function sendListRequests(Collection $settings)
    {
        $results    =   array('success'=>0,'failure'=>0);
        $mws_settings   =   self::getFormattedMWSSettings($settings);
        if(!empty($mws_settings))
        {
            foreach($mws_settings as $setting)
            {
                $response = self::fetchRequestsList($setting);
                if($response)
                    $results['success']++;
                else
                    $results['failure']++;
            }
        }
        return $results;
    }


    public static function getReports(Collection $settings)
    {
        $results    =   array('success'=>0,'failure'=>0);
        $mws_settings   =   self::getFormattedMWSSettings($settings);
        if(!empty($mws_settings))
        {
            foreach($mws_settings as $setting)
            {
                $response = \CommonHelper::sendMWSRequest("request_get_report", $setting);

                if(!empty($response['Message']))
                {
                    AmazonOrder::saveReportOrders($response['Message'],$setting);
                }

               /* if (is_array($response) && !empty($response))
                {
                    $response['setting_id'] = $setting['id'];
                    $response['profile_id'] = $setting['profile_id'];
                    if(self::saveReportRequestToDb($response))
                        $results['success']++;
                    else
                        $results['failure']++;
                }*/
            }
        }
        exit;
        return $results;
    }
    private static function fetchRequestsList($mws_setting,$next_token="")
    {
        if(!empty($mws_setting))
        {
            $report_data    =   array();
            if (empty($next_token))
            {
                $report_list = \CommonHelper::sendMWSRequest("request_list_report", $mws_setting);
            }
            else
            {
                $report_list = \CommonHelper::sendMWSRequest("request_list_by_next_token_report", $mws_setting, $next_token);
            }
            if(isset($report_list['ReportRequestInfo']['FieldValue']))
            {
                $report_data = $report_list['ReportRequestInfo']['FieldValue'];
            }
            if(isset($mws_setting['request_id']))
                $report_data['request_id']  =   $mws_setting['request_id'];
            if(isset($mws_setting['request']))
                $report_data['request']     =   $mws_setting['request'];

            self::updateReportsData($report_data);

            if(!empty($report_list['NextToken']['FieldValue']))
            {
                $next_token                 =   $report_list['NextToken']['FieldValue'];
                self::fetchRequestsList($mws_setting,$next_token);
            }
            else
            {
                return TRUE;
            }
        }
        return FALSE;
    }

    private static function updateReportsData(array $report_data=array())
    {
        if(isset($report_data['request_id']))
            $request_id =   $report_data['request_id'];
        if(isset($report_data['request']))
            $id         =   $report_data['request'];

        if(!empty($report_data))
        {
            foreach($report_data as $key=>$report)
            {
                try
                {
                    $report             =   (array)$report;
                    if(!empty($report))
                        $report         =   reset($report);

                    $report_type        =   "";
                    if(isset($report['ReportRequestId']['FieldValue']))
                    {
                        $request_id = $report['ReportRequestId']['FieldValue'];
                        $curr_report = AmazonReportRequest::where(array("request_id"=>$request_id));
                        if(isset($report['ReportType']['FieldValue']))
                        {
                            $report_type    =   $report['ReportType']['FieldValue'];
                            $curr_report    =   $curr_report->where(array("type"=>$report_type));
                        }
                        $curr_report    =   $curr_report->get();

                        if(!$curr_report->isEmpty())
                        {
                            $curr_report    =   $curr_report->first();




                            if(is_object($report['StartDate']['FieldValue']))
                                if(isset($report['StartDate']['FieldValue']->date))
                                    $curr_report->start_date    =    date("Y-m-d h:i:s",strtotime($report['StartDate']['FieldValue']->date));

                            if(is_object($report['EndDate']['FieldValue']))
                                if(isset($report['EndDate']['FieldValue']->date))
                                    $curr_report->end_date    =    date("Y-m-d h:i:s",strtotime($report['EndDate']['FieldValue']->date));


                            if(is_object($report['SubmittedDate']['FieldValue']))
                                if(isset($report['SubmittedDate']['FieldValue']->date))
                                    $curr_report->submitted_date    =     date("Y-m-d h:i:s",strtotime($report['SubmittedDate']['FieldValue']->date));

                            if(is_object($report['StartedProcessingDate']['FieldValue']))
                                if(isset($report['StartedProcessingDate']['FieldValue']->date))
                                    $curr_report->started_processing_date    =     date("Y-m-d h:i:s",strtotime($report['StartedProcessingDate']['FieldValue']->date));


                            if(is_object($report['CompletedDate']['FieldValue']))
                                if(isset($report['CompletedDate']['FieldValue']->date))
                                    $curr_report->completed_date    =     date("Y-m-d h:i:s",strtotime($report['CompletedDate']['FieldValue']->date));




                            if(isset($report['ReportProcessingStatus']['FieldValue']))
                                $curr_report->processing_status      =   $report['ReportProcessingStatus']['FieldValue'];


                            if(isset($report['Scheduled']['FieldValue']))
                                $curr_report->scheduled      =   $report['Scheduled']['FieldValue'];


                            if(isset($report['GeneratedReportId']['FieldValue']))
                                $curr_report->generated_report_id      =   $report['GeneratedReportId']['FieldValue'];

                            $curr_report->updated_at                  =    date("Y-m-d h:i:s");
                            $curr_report->save();


                        }

                    }
                }
                catch(\Exception $exp)
                {
                    continue;
                }

            }

        }



    }

 /*
  switch ($type)
        {
            case    "request_orders_report":
                break;
            case    "request_list_report":
                break;
            case    "request_list_by_next_token_report":
                break;
            case    "request_get_report":
                break;
            default:
                break;
        }



  // private if (empty($next_token))
{
$orders = \CommonHelper::sendMWSRequest("list_orders", $this->mws_settings);
}
else
    {
        $orders = \CommonHelper::sendMWSRequest("list_orders_by_next_token", $this->mws_settings, $next_token);
    }

*/
    private static function saveReportRequestToDb(array $report_data =   array())
    {
        $result =   FALSE;
        if(!empty($report_data))
        {
            try
            {


                $request                                            =   new AmazonReportRequest();
                $request->request_id                                =    $report_data['ReportRequestId']['FieldValue'];
                $request->type                                      =    $report_data['ReportType']['FieldValue'];

                if(is_object($report_data['StartDate']['FieldValue']))
                    if(isset($report_data['StartDate']['FieldValue']->date))
                        $request->start_date                        =     date("Y-m-d h:i:s",strtotime($report_data['StartDate']['FieldValue']->date));

                if(is_object($report_data['EndDate']['FieldValue']))
                    if(isset($report_data['EndDate']['FieldValue']->date))
                        $request->end_date                          =     date("Y-m-d h:i:s",strtotime($report_data['EndDate']['FieldValue']->date));

                $request->scheduled                                 =    $report_data['Scheduled']['FieldValue'];


                if(is_object($report_data['SubmittedDate']['FieldValue']))
                    if(isset($report_data['SubmittedDate']['FieldValue']->date))
                        $request->submitted_date                    =     date("Y-m-d h:i:s",strtotime($report_data['SubmittedDate']['FieldValue']->date));

                $request->processing_status                         =    $report_data['ReportProcessingStatus']['FieldValue'];
                $request->generated_report_id                       =    $report_data['GeneratedReportId']['FieldValue'];

                if(is_object($report_data['StartedProcessingDate']['FieldValue']))
                    if(isset($report_data['StartedProcessingDate']['FieldValue']->date))
                         $request->started_processing_date           =     date("Y-m-d h:i:s",strtotime($report_data['StartedProcessingDate']['FieldValue']->date));


                if(is_object($report_data['CompletedDate']['FieldValue']))
                    if(isset($report_data['CompletedDate']['FieldValue']->date))
                        $request->completed_date                    =     date("Y-m-d h:i:s",strtotime($report_data['CompletedDate']['FieldValue']->date));


                $request->app_status                                =    "Pending";
                $request->setting_id                                =    $report_data['setting_id'];
                $request->profile_id                                =    $report_data['profile_id'];
                $request->created_at                                =    date("Y-m-d h:i:s");
                $request->updated_at                                =    date("Y-m-d h:i:s");
                if($request->save())
                    $result =   TRUE;
            }
            catch(\Exception $exp)
            {
                //echo $exp->getMessage();
                //exit;
            }
        }
        return $result;
    }


    public function sellers()
    {
        return $this->belongsTo('App\UserAmazonSetting', 'setting_id', 'id');
    }
}