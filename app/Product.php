<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Validator;
class Product extends Model
{
    public static function saveToDb($data=array())
    {
        $result =   array("status"=>'3',"message"=>" Error in saving data! ");

        if(!empty($data))
        {
            if(Message::validateMessage($data))
            {
                if(!empty($data['message_id']))
                {
                    $message        =   Message::where('id', $data['message_id'])->first();
                    if (!$message)
                    {
                        $result['message']  =   " Message not found in the system. ";
                        return $result;
                    }
                }
                else
                {
                    $message = new Message();
                    $message->created_at = date("Y-m-d h:i:s");
                }


                $message->title                             =   $data['title'];
                $message->subject                           =   $data['subject'];
                $message->content                           =   $data['content'];
                if(isset($data['marketplace']))
                    $message->marketplace                   =   $data['marketplace'];

                if(isset($data['status']))
                    $message->status                        =   $data['status'];


                if(isset($data['when_send_to_buyer']))
                    $message->when_send_to_buyer            =   $data['when_send_to_buyer'];

                if(isset($data['send_after']))
                    $message->send_after                    =   $data['send_after'];

                if(isset($data['priority']))
                    $message->priority                      =   $data['priority'];

                if(isset($data['order_placed_on_or_after']))
                    $message->order_placed_on_or_after      =   date("Y-m-d h:i:s",strtotime($data['order_placed_on_or_after']));

                if(isset($data['order_placed_before']))
                    $message->order_placed_before           =   date("Y-m-d h:i:s",strtotime($data['order_placed_before']));

                if(isset($data['sku_filter']))
                    $message->sku_filter                    =   $data['sku_filter'];

                if(isset($data['asin_filter']))
                    $message->asin_filter                   =   $data['asin_filter'];

                if(isset($data['fulfillment_channel']))
                    $message->fulfillment_channel           =   $data['fulfillment_channel'];

                if(isset($data['shipping_country_is_not']))
                    if(is_array($data['shipping_country_is_not']))
                    {
                        $message->shipping_country_is_not    =  implode(",",$data['shipping_country_is_not']);
                    }


                if(isset($data['shipping_country']))
                    if(is_array($data['shipping_country']))
                    {
                        $message->shipping_country    =  implode(",",$data['shipping_country']);
                    }

                if($message->save())
                {
                    $result ["status"]   =   '1';
                    $result ["message"]  =   " Successfully saved to database ";
                }
            }
            else
            {
                $result['validator'] =   self::$validator;
                self::$validator     =   "";
                $result ["status"]   =   '2';
                $result['message']   =   " Validation Failed. ";
            }

            return $result;
        }
    }

    public function getProductsList($filters=array())
    {
        /*keyword category price rank review rating*/
        if(!empty($filters['keyword']))
        $result=false;
        $this->db->select($select);
        $this->db->from($from);
        if(!empty($join_array) && is_array($join_array))
            $this->db->join($join_array[0],$join_array[1],$join_array[2]);

        if(!empty($where))
        {
            $this->build_where($where);
            /*			 if(isset($where['where_string']))
                         {
                            foreach($where['where_string'] as $where_str)
                                $this->db->where($where_str, NULL, FALSE);
                            unset($where['where_string']);
                         }
                         $this->db->where($where);
            */		 }
        if(!empty($search_array) && is_array($search_array))
        {
            $counter=1;
            $like_str="( ";
            foreach($search_array as $field=>$value)
            {
                if(is_array($value))
                {
                    foreach($value as $val)
                        if($counter++ ==1)
                            $like_str.=$field." LIKE '%".$val."%'";
                        else
                            $like_str.= " OR ". $field." LIKE '%".$val."%'";
                    continue;
                }
                else
                {
                    if($counter++ ==1)
                        $like_str.=$field." LIKE '%".$value."%'";
                    else
                        $like_str.= " OR ". $field." LIKE '%".$value."%'";
                }
            }
            $like_str.=" )";
            $this->db->where($like_str);
        }
        if(!empty($group_by))
            $this->db->group_by($group_by);

        if(!empty($is_count))
        {
            $count=clone $this->db;
            $count=$count->count_all_results();
        }

        if(!empty($order_by))
            $this->db->order_by($order_by);

        if(!empty($is_result))
            $result=$this->db->get()->result('array');
        else
        {

            if(($offset > -1) && ($limit > 0))
            {
                $this->db->limit($limit,$offset);
            }
            else if($limit > 0)
            {
                $this->db->limit($limit);
            }

            $result=$this->db->get()->result('array');
            if(!empty($count))
            {
                $result['total']=$count;
            }
        }
        //	debug($result,true);
        return $result;
    }


}