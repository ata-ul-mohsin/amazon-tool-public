<?php
use Illuminate\Support\Facades\Schema;
use App\Message;
use App\AmazonOrder;
use App\Variable;
use App\AmazonProduct;
	class CommonHelper
	{
		public static function debug($data, $exit = false)
		{
			if (is_object($data) || is_array($data))
			{
				echo "<pre>";
				print_r($data);
				echo "</pre>";
			}
			else
				echo $data;
			if ($exit)
				exit;
		}

		public static function alertMessage($mod = 'success', $message)
		{
			if (strstr($message, "="))
			{
				$msg = explode("=", $message);
				$mod = $msg[0];
				$message = $msg[1];
			}
			$alert = "<div class='alert alert-" . $mod . "' role='alert'>" . $message . "</div>";
			return $alert;
		}

		public static function uploadFile($file, $path = 'media', $file_to_replace = "", $user_id = "")
		{
			if (!empty($file))
			{
				try
				{
					$file_name = (!empty($user_id) ? $user_id . "_" : "") . md5(time());
					$extension = $file->getClientOriginalExtension();
					$file_name = $file_name . "." . $extension;
					$path = base_path("/assets") . $path;
					if (!($dir = is_dir($path)))
					{
						$dir = mkdir($path, '0777', true);
					}
					else
						$dir = $path;

					$uploaded = $file->move($dir, $file_name);
					if (!empty($file_to_replace))
					{
						\File::delete($path . "/" . $file_to_replace);
					}
				}
				catch (\Exception $exp)
				{
					return FALSE;
				}
			}
			if (!empty($uploaded))
			{

				return $file_name;
			}
			else
			{
				return FALSE;
			}
		}

		public static function objToArray($obj = "")
		{
			$array = array();
			if (is_object($obj))
			{
				$array = json_encode($obj);
				$array = json_decode($array, true);
			}
			return $array;
		}

		public static function sendMWSRequest($type, $settings, $next_token = "", $order_id = "",$report_id="")
		{
			$result = FALSE;
		//	try
			{
				sleep(3);
				$access_key = $settings    ['access_key_id'];
				$secret_key = $settings    ['secret_key'];
				$application_name = ' Amazon Tool ';
				$application_version = ' 1.0 ';
				$merchant_id = $settings    ['seller_id'];

				$marketplace = $settings    ['marketplace']['marketplace_id'];
				$sub_type	=	substr($type, -7);

				/*
				 * 	If request is for report
				 */
				if(strcmp($sub_type,"_report")==0)
				{

					$serviceUrl = "https://mws.amazonservices.com";
					$config = array(
						'ServiceURL' => $serviceUrl,
						'ProxyHost' => null,
						'ProxyPort' => -1,
						'MaxErrorRetry' => 3
					);
					$marketplaceIdArray = array("Id" => array($settings['marketplace']['marketplace_id']));
					$service = new MarketplaceWebService_Client(
						$access_key,
						$secret_key,
						$config,
						$application_name,
						$application_version
					);

					$list_from_date = new DateTime();
					$requested_from_date = $list_from_date->modify('-1 day')->format(DateTime::ISO8601);

				/*
					$list_from_date = new DateTime();
					$list_from_date->add(DateInterval::createFromDateString('yesterday'));
					$requested_from_date= $list_from_date->format(DateTime::ISO8601);*/


					//'_IN_PROGRESS_',
					//'_SUBMITTED_',
					$status_types	=	array(
						                      '_CANCELLED_',
						                      '_DONE_NO_DATA_',
						                      '_DONE_',
											);

					$status_list = new MarketplaceWebService_Model_StatusList();
					$status_list->setStatus($status_types);
					$report_processing_status_list	=	$status_list;

					$type_list = new MarketplaceWebService_Model_TypeList();

				 	$type_list->setType('_GET_XML_ALL_ORDERS_DATA_BY_ORDER_DATE_');
					$report_type_list				=	$type_list;


				}
				else
				{
					$serviceUrl = "https://mws.amazonservices.com/Orders/2013-09-01";
					$config = array(
						'ServiceURL' => $serviceUrl,
						'ProxyHost' => null,
						'ProxyPort' => -1,
						'ProxyUsername' => null,
						'ProxyPassword' => null,
						'MaxErrorRetry' => 3,
					);



					$service = new MarketplaceWebServiceOrders_Client($access_key,
						$secret_key, $application_name,
						$application_version, $config);
				}


				switch ($type)
				{
					case "list_orders":
						$create_after = date("c", time() - (10 * 24 * 60 * 60));
						$create_before = date("c", time() - (2 * 60));

						$request = new MarketplaceWebServiceOrders_Model_ListOrdersRequest();
						$request->setSellerId($merchant_id);
						$request->setMarketplaceId($marketplace);
						$request->setCreatedAfter($create_after);
						// $request->setCreatedBefore($create_before);
						$result = $service->ListOrders($request);
						$result = (array)($result);
						$result = reset($result);
						$result = reset($result);
						$result = reset($result);
						$result = (array)($result);
						$result = reset($result);

						break;

					case "list_orders_by_next_token":
						if (!empty($next_token))
						{
							$request = new MarketplaceWebServiceOrders_Model_ListOrdersByNextTokenRequest();
							$request->setSellerId($merchant_id);
							$request->setNextToken($next_token);
							$result = $service->ListOrdersByNextToken($request);
							$result = (array)($result);
							$result = reset($result);
							$result = reset($result);
							$result = reset($result);
							$result = (array)($result);
							$result = reset($result);
						}
						break;
					case "list_order_items":
						if (!empty($order_id))
						{
							$request = new MarketplaceWebServiceOrders_Model_ListOrderItemsRequest();
							$request->setSellerId($merchant_id);
							$request->setAmazonOrderId($order_id);
							$result = $service->ListOrderItems($request);
							$result = (array)$result;
							if (!empty($result))
								$result = reset($result);
							if (is_object($result))
								$result = (array)$result;
							if (is_array($result) && !empty($result))
								$result = reset($result);
							if (is_object($result))
								$result = (array)$result;
							if (is_array($result) && !empty($result))
								$result = reset($result);
							if (is_object($result))
								$result = (array)$result;
							if (is_array($result) && !empty($result))
								$result = reset($result);

						}
						break;
					case "list_order_items_by_next_token":
						if (!empty($order_id))
						{
							/*$request = new MarketplaceWebServiceOrders_Model_ListOrderItemsRequest();
							$request->setSellerId($merchant_id);
							$request->setAmazonOrderId($order_id);
							$result = $service->ListOrderItems($request);
							$result = (array)$result;
							if (!empty($result))
								$result = reset($result);
							if (is_object($result))
								$result = (array)$result;
							if (is_array($result) && !empty($result))
								$result = reset($result);
							if (is_object($result))
								$result = (array)$result;
							if (is_array($result) && !empty($result))
								$result = reset($result);
							if (is_object($result))
								$result = (array)$result;
							if (is_array($result) && !empty($result))
								$result = reset($result);*/

						}
						break;
					case "request_orders_report":

						$parameters = array (
							'Merchant' => $merchant_id,
							'MarketplaceIdList' => $marketplaceIdArray,
							'ReportType' => '_GET_XML_ALL_ORDERS_DATA_BY_ORDER_DATE_',
							'ReportOptions' => 'ShowSalesChannel=true',
							'MWSAuthToken' => $access_key,
							'StartDate' => date('Y-m-d',strtotime("-2 days"))
							//'EndDate' => date('Y-m-d',time())
						);



						$request = new MarketplaceWebService_Model_RequestReportRequest($parameters);
						$result = $service->requestReport($request);

						if($result instanceof MarketplaceWebService_Model_RequestReportResponse)
						{
							$response	=	$result->getRequestReportResult();
							for($i=0; $i<4; $i++)
							{
								if(!empty($response))
								{
									if (is_array($response))
										$response = reset($response);
									else
									{
										$response = (array)($response);
										$response = reset($response);
									}
								}
							}

						}

						if(!empty($response))
							$result	=	$response;
						else
							$result	=	FALSE;

						break;

					case "request_list_report":

						$parameters = array (
							'Merchant' => $merchant_id,
							'MWSAuthToken' => $access_key,
						    'RequestedFromDate' => $requested_from_date
						);
						$request = new MarketplaceWebService_Model_GetReportRequestListRequest($parameters);

					 	$request->setReportProcessingStatusList($report_processing_status_list);
						$request->setReportTypeList($report_type_list);

						$response = $service->getReportRequestList($request);

						$response=	$response->getGetReportRequestListResult();

						$response	=	(array)$response;
						if(!empty($response))
							$response	=	reset($response);

						if(!empty($response))
							$result	=	$response;
						else
							$result	=	FALSE;


						break;
					case "request_list_by_next_token_report":


						$parameters = array (
							'Merchant' => $merchant_id,
							'MWSAuthToken' => $access_key,
							'NextToken' => $next_token,
							'RequestedFromDate' => $requested_from_date
						);

						$request = new MarketplaceWebService_Model_GetReportRequestListByNextTokenRequest($parameters);

						$response = $service->getReportRequestListByNextToken($request);
						$response=	$response->getGetReportRequestListByNextTokenResult();
						$response	=	(array)$response;
						if(!empty($response))
							$response	=	reset($response);

						if(!empty($response))
							$result	=	$response;
						else
							$result	=	FALSE;

						break;
					case "request_get_report":
						if(!empty($settings['report_id']))
						{
							$report_id	=	$settings['report_id'];
							$parameters = array (
								'Merchant' => $merchant_id,
								'Report' => @fopen('php://memory', 'rw+'),
								'ReportId' => $report_id,
								'MWSAuthToken' => $access_key
							);
							$request = new MarketplaceWebService_Model_GetReportRequest($parameters);
							$response = $service->getReport($request);
							$report_data	=stream_get_contents($request->getReport());
							$xml = simplexml_load_string($report_data);
							$report_data = json_decode(json_encode($xml),TRUE);
						}
						if(!empty($report_data))
							$result	=	$report_data;
						else
							$result	=	FALSE;
						break;
					default:
						break;
				}

			}
		//	catch (Exception $ex)
			{
				//echo "<h1>error:=" . $ex->getMessage() . "</h1>";

			//	$result = FALSE;
			}

			return $result;

		}

		public static function find_or_create_table($profile_id, $type)
		{
			try
			{
				$table = FALSE;
				switch ($type)
				{
					case "orders":
							$table = "amazon_" . $type . "_" . $profile_id;
							if (!Schema::hasTable($table))
							{
								Schema::connection('mysql')->create($table, function ($table)
								{
									$table->increments('id');
									$table->string('order_id', 50);
									$table->string('buyer_name', 200);
									$table->string('buyer_email', 100);


									$table->dateTime('earliest_ship_date')->default('0000-00-00 00:00:00');
									$table->dateTime('latest_ship_date')->default('0000-00-00 00:00:00');
									$table->dateTime('purchase_date')->default('0000-00-00 00:00:00');
									$table->string('fulfillment_channel', 10)->nullable();
									$table->string('shipping_address', 100)->nullable();
									$table->string('shipping_city', 20)->nullable();
									$table->string('shipping_state', 15)->nullable();
									$table->string('shipping_postal_code', 20)->nullable();
									$table->string('shipping_country', 5)->nullable();
									$table->string('marketplace_id', 20)->nullable();
									$table->string('shipping_name', 50)->nullable();
									$table->string('shipping_phone',30)->nullable();
									$table->string('status', 50);
									$table->integer('setting_id');
									$table->dateTime('created_before');
									$table->dateTime('created_at')->default('0000-00-00 00:00:00');
									$table->dateTime('updated_at')->default('0000-00-00 00:00:00');


									$table->string('ship_service_level', 50)->nullable();
									$table->string('merchant_order_id', 50)->nullable();
									$table->dateTime('last_updated_date', 50)->default('0000-00-00 00:00:00');
									$table->string('sales_channel', 50)->nullable();
									$table->string('asin', 50)->nullable();
									$table->string('sku', 20)->nullable();
									$table->string('item_status', 50)->nullable();
									$table->string('product_name', 100)->nullable();
									$table->string('quantity', 5)->nullable();


									$table->softDeletes();
								});
							}
							break;
					case "emails":
							$table = "amazon_" . $type . "_" . $profile_id;
							if (!Schema::hasTable($table))
							{
								Schema::connection('mysql')->create($table, function ($table)
								{
									$table->increments('id');
									$table->string('order_id', 50);
									$table->string('buyer_name', 200);
									$table->string('buyer_email', 100);
									$table->string('status', 50);
									$table->integer('setting_id');
									$table->dateTime('created_before');
									$table->dateTime('created_at')->default('0000-00-00 00:00:00');
									$table->dateTime('updated_at')->default('0000-00-00 00:00:00');
									$table->softDeletes();
								});
							}

							break;
					case "feedbacks":
							$table = "amazon_" . $type . "_" . $profile_id;
							if (!Schema::hasTable($table))
							{
								Schema::connection('mysql')->create($table, function ($table)
								{
									$table->increments('id');
									$table->integer('setting_id');
									$table->string('rater', 50);
									$table->char('rating', 2);
									$table->enum('status', ['Collected','New','Waiting for user response','Responded','Ticket Opened','Resolved','Unresolvable'])->default('Collected');
									$table->string('rating_text', 500)->nullable();
									$table->string('responseRatingData',5)->nullable();
									$table->string('hasResponse',5)->nullable();
									$table->dateTime('rating_date')->default('0000-00-00 00:00:00');
									$table->dateTime('created_at')->default('0000-00-00 00:00:00');
									$table->dateTime('updated_at')->default('0000-00-00 00:00:00');
									$table->softDeletes();

								});
							}
							break;
					default:
						break;

				}

			}
			catch (\Exception $exp)
			{
				echo $exp->getMessage(); exit;
			}

			return $table;
		}


		public static function composeMessage($outbox_id)
		{
			$outbox_record	=	\DB::table("outbox_messages")
				->where("id","=",$outbox_id)
				->whereRaw("deleted_at IS NULL")
				->get();
			if(!empty($outbox_record))
			{
				$outbox_record	=	(array)(reset($outbox_record));
				$message 		=	Message::where("id","=",$outbox_record['message_id'])
								    ->whereNull("deleted_at");
				$message		=	$message->get();
				$profile_id		=	$outbox_record['profile_id'];
				$order		=	new AmazonOrder();
				$table		=	"amazon_orders_".$profile_id;
				$order		=	$order->setTable($table);
				$order		=	$order->where("order_id","=",$outbox_record['order_id'])
								->whereNull("deleted_at");;
				$order		=	$order->get();

				if(!$order->isEmpty() && !$message->isEmpty())
				{

					$order		=	$order->first();
					$message	=	$message->first();

					$products	=	AmazonProduct::getProductsOfOrder($order->order_id,"array");

					$message_body	=	Variable::replaceVariables($message->content);

					self::debug($message_body,true);
					self::debug($products,true);

					//self::debug($variables,true);
				}
			}


			/*
			 *
			 * Array
(
    [id] => 1
    [message_id] => 7
    [order_id] => 110-8662541-3515449
    [email] => 84n3kxt5308jcyk@marketplace.amazon.com
    [products] =>
    [setting_id] => 16
    [due_date] => 2016-02-18 13:27:53
    [profile_id] => 119158
    [created_at] => 0000-00-00 00:00:00
    [updated_at] => 0000-00-00 00:00:00
    [deleted_at] =>
)
			 *
			$message 		=	Message::where("id","=",$message_id);
			$message		=	$message->get();
			$profile_id		=	$settings['profile_id'];
			$order		=	new AmazonOrder();
			$table		=	"amazon_orders_".$profile_id;
			$order		=	$order->setTable($table);
			$order		=	$order->where("order_id","=",$order_id);
			$order		=	$order->get();
*/


			//if (!$message->isEmpty() && !$order->isEmpty())
			{
		//		$order = $order->toArray();
				/*
				 *
				 *
				 *
				 *
				 *
				 *
				 * [[logo]]	The logo for your store	Please upload a logo on the Settings page first
[[logo-link]]	The logo for your store, which links to your Amazon store	Please upload a logo on the Settings page first
[[logo-link-alt]]	The logo for your store, with alternate link to your Amazon store (page with listings and no feedback)	Please upload a logo on the Settings page first
[[buyer-name]]	The buyer's full name	John Doe
[[first-name]]	The buyer's first name	John
[[product-name]]	The name of the item that the customer bought	X-Box 360 Slim
[[order-id]]	The Customer order ID	105-4697172-1635425
[[msku]]	Your Merchant SKU (msku) for the item	ABC-123
[[asin]]	The Amazon ASIN for the product	B000123456
[[quantity]]	The quantity ordered	1
[[price-item]]	The Price of the item (without currency sign)	29.99
[[price-shipping]]	The amount paid for shipping (without currency sign)	3.99
[[condition-note]]	The 'condition-note' provided with the listing
[[buyer-email]]	The encoded email address of the buyer	cfabcdes1h1ty82@marketplace.amazon.com
[[recipient]]	Recipient Name (may not be the same as the buyer)	John Doe
[[ship-address1]]	Address Line 1	123 Oak Street
[[ship-address2]]	Address Line 2	Apt #3A
[[ship-city]]	Shipping City	Athens
[[ship-state]]	Shipping State	GA
[[ship-zip]]	Postal Code Code	30601
[[ship-country]]	Shipping Country	US
[[carrier]]	**The delivery carrier	UPS
[[tracking-number]]	**Carrier Tracking Number	1Z66A4719871236251
[[estimated-arrival]]	**Estimated Arrival Date (From Amazon's data - ONLY AVAILABLE FOR FBA Orders)	2013-04-20
				 */
			}
		}

		public static function isLocal()
		{
			$locals = array();
			$locals[] = "127.0.0.1";
			$locals[] = "::1";

			$current_server = \Request::server();
			if (isset($current_server['REMOTE_ADDR']))
				if (in_array($current_server['REMOTE_ADDR'], $locals))
					return TRUE;
			return FALSE;
		}

		public static function saveLog($file, $data)
		{
			$path = public_path() . "/logs/";

			if (!($dir = is_dir($path)))
			{
				$dir = mkdir($path, '0777', true);
			}
			if ($dir)
			{
				$myfile = file_put_contents($path . $file . ".txt", PHP_EOL . json_encode((array)$data, 5) . PHP_EOL, FILE_APPEND);
				return TRUE;
			}
			return FALSE;
		}

		public static function recursive_cast_to_array($object)
		{
			$array = (array)$object;
			foreach ($array as &$value)
			{
				if (is_object($value))
				{
					$value = recursive_cast_to_array($value);
				}
			}
			return $array;
		}

	}

?>