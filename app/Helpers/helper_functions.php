<?php
    use Illuminate\Support\Facades\Schema;
    if(!function_exists("debug"))
    {
        function debug($data, $exit = false)
        {
            if (is_object($data) || is_array($data))
            {
                echo "<pre>";
                print_r($data);
                echo "</pre>";
            }
            else
                var_dump($data);
            if ($exit)
                exit;
        }
    }
    if((!function_exists('find_or_create_table')))
    {
        function find_or_create_table($profile_id,$type)
        {
            try
            {
                $table  =   "";
                switch($type)
                {
                    case "orders":
                    case "emails":
                                    $table  =   "amazon_".$type."_".$profile_id;
                                     break;
                    default:
                                    break;

                }
                if(!empty($table))
                {
                    if (Schema::hasTable($table))
                    {
                        return $table;
                    }
                    else
                    {
                        Schema::connection('mysql')->create($table, function ($table)
                        {
                            $table->increments('id');
                            $table->string('order_id',100);
                            $table->string('buyer_name',200);
                            $table->string('buyer_email',100);
                            $table->string('status',50);
                            $table->integer('setting_id');
                            $table->dateTime('created_before');
                            $table->dateTime('created_at')->default('0000-00-00 00:00:00');
                            $table->dateTime('updated_at')->default('0000-00-00 00:00:00');
                            $table->softDeletes();
                        });
                        return $table;
                    }
                }

            }
            catch(\Exception $exp)
            {
                //echo $exp->getMessage(); exit;
            }

            return FALSE;
        }
    }
    if((!function_exists('alertMessage')))
    {
        function alertMessage($mod = 'success', $message)
        {
            if (strstr($message, "="))
            {
                $msg = explode("=", $message);
                $mod = $msg[0];
                $message = $msg[1];
            }
            $alert = "<div class='alert alert-" . $mod . "' role='alert'>" . $message . "</div>";
            return $alert;
        }
    }
    if((!function_exists('uploadFile')))
    {
        function uploadFile($file, $path = 'media', $file_to_replace = "", $user_id = "")
        {
            if (!empty($file))
            {
                try
                {
                    $file_name = (!empty($user_id) ? $user_id . "_" : "") . md5(time());
                    $extension = $file->getClientOriginalExtension();
                    $file_name = $file_name . "." . $extension;
                    $path = base_path("/assets") . $path;
                    if (!($dir = is_dir($path)))
                    {
                        $dir = mkdir($path, '0777', true);
                    }
                    else
                        $dir = $path;

                    $uploaded = $file->move($dir, $file_name);
                    if (!empty($file_to_replace))
                    {
                        \File::delete($path . "/" . $file_to_replace);
                    }
                }
                catch (\Exception $exp)
                {
                    return FALSE;
                }
            }
            if (!empty($uploaded))
            {

                return $file_name;
            }
            else
            {
                return FALSE;
            }
        }
    }
    if((!function_exists('objToArray')))
    {
        function objToArray($obj = "")
        {
            $array = array();
            if (is_object($obj))
            {
                $array = json_encode($obj);
                $array = json_decode($array, true);
            }
            return $array;
        }
    }
    if((!function_exists('sendMWSRequest')))
    {
        function sendMWSRequest($type, $settings, $next_token = "")
        {
            $result = FALSE;
            try
            {
                sleep(3);
                $access_key = $settings    ['access_key_id'];
                $secret_key = $settings    ['secret_key'];
                $application_name = ' Amazon Tool ';
                $application_version = ' 1.0 ';
                $merchant_id = $settings    ['seller_id'];

                $marketplace = $settings    ['marketplace']['marketplace_id'];

                $serviceUrl = "https://mws.amazonservices.com/Orders/2013-09-01";
                $config = array(
                    'ServiceURL' => $serviceUrl,
                    'ProxyHost' => null,
                    'ProxyPort' => -1,
                    'ProxyUsername' => null,
                    'ProxyPassword' => null,
                    'MaxErrorRetry' => 3,
                );
                $service = new MarketplaceWebServiceOrders_Client($access_key,
                    $secret_key, $application_name,
                    $application_version, $config);


                switch ($type)
                {
                    case "list_orders":
                        $create_after = date("c", time() - (5 * 24 * 60 * 60));
                        $create_before = date("c", time() - (2 * 60));

                        $request = new MarketplaceWebServiceOrders_Model_ListOrdersRequest();
                        $request->setSellerId($merchant_id);
                        $request->setMarketplaceId($marketplace);
                        $request->setCreatedAfter($create_after);
                        // $request->setCreatedBefore($create_before);
                        $result = $service->ListOrders($request);
                        $result = (array)($result);
                        $result = reset($result);
                        $result = reset($result);
                        $result = reset($result);
                        $result = (array)($result);
                        $result = reset($result);
                        break;

                    case "list_orders_by_next_token":
                        if (!empty($next_token))
                        {
                            $request = new MarketplaceWebServiceOrders_Model_ListOrdersByNextTokenRequest();
                            $request->setSellerId($merchant_id);
                            $request->setNextToken($next_token);
                            $result = $service->ListOrdersByNextToken($request);
                            $result = (array)($result);
                            $result = reset($result);
                            $result = reset($result);
                            $result = reset($result);
                            $result = (array)($result);
                            $result = reset($result);
                        }
                        break;

                    default:
                        break;
                }

            }
            catch (Exception $ex)
            {
                echo "<h1>error:=" . $ex->getMessage() . "</h1>";

                $result = FALSE;
            }

            return $result;

        }
    }

    if((!function_exists('composeMessage')))
    {
        function composeMessage($message_id, $order_id)
        {
            $message = \Message::findOrFail($message_id);
            $order = \AmazonOrder::findOrFail($order_id);
            if ($message && $order)
            {
                $message = $message->toArray();
                $order = $order->toArray();
                /*
                 *
                 *
                 *
                 *
                 *
                 *
                 * [[logo]]	The logo for your store	Please upload a logo on the Settings page first
        [[logo-link]]	The logo for your store, which links to your Amazon store	Please upload a logo on the Settings page first
        [[logo-link-alt]]	The logo for your store, with alternate link to your Amazon store (page with listings and no feedback)	Please upload a logo on the Settings page first
        [[buyer-name]]	The buyer's full name	John Doe
        [[first-name]]	The buyer's first name	John
        [[product-name]]	The name of the item that the customer bought	X-Box 360 Slim
        [[order-id]]	The Customer order ID	105-4697172-1635425
        [[msku]]	Your Merchant SKU (msku) for the item	ABC-123
        [[asin]]	The Amazon ASIN for the product	B000123456
        [[quantity]]	The quantity ordered	1
        [[price-item]]	The Price of the item (without currency sign)	29.99
        [[price-shipping]]	The amount paid for shipping (without currency sign)	3.99
        [[condition-note]]	The 'condition-note' provided with the listing
        [[buyer-email]]	The encoded email address of the buyer	cfabcdes1h1ty82@marketplace.amazon.com
        [[recipient]]	Recipient Name (may not be the same as the buyer)	John Doe
        [[ship-address1]]	Address Line 1	123 Oak Street
        [[ship-address2]]	Address Line 2	Apt #3A
        [[ship-city]]	Shipping City	Athens
        [[ship-state]]	Shipping State	GA
        [[ship-zip]]	Postal Code Code	30601
        [[ship-country]]	Shipping Country	US
        [[carrier]]	**The delivery carrier	UPS
        [[tracking-number]]	**Carrier Tracking Number	1Z66A4719871236251
        [[estimated-arrival]]	**Estimated Arrival Date (From Amazon's data - ONLY AVAILABLE FOR FBA Orders)	2013-04-20
                 */
            }
        }
    }
    if((!function_exists('isLocal')))
    {
        function isLocal()

        {
            $locals = array();
            $locals[] = "127.0.0.1";
            $locals[] = "::1";

            $current_server = \Request::server();
            if (isset($current_server['REMOTE_ADDR']))
                if (in_array($current_server['REMOTE_ADDR'], $locals))
                    return TRUE;
            return FALSE;
        }
    }
    if((!function_exists('saveLog')))
    {
        function saveLog($file, $data)
        {
            $path = public_path() . "/logs/";

            if (!($dir = is_dir($path)))
            {
                $dir = mkdir($path, '0777', true);
            }
            if ($dir)
            {
                $myfile = file_put_contents($path . $file . ".txt", PHP_EOL . json_encode((array)$data, 5) . PHP_EOL, FILE_APPEND);
                return TRUE;
            }
            return FALSE;
        }
    }

    if(!function_exists("recursive_cast_to_array"))
    {
        function recursive_cast_to_array($object)
        {
            $array = (array)$object;
            foreach ($array as &$value)
            {
                if (is_object($value))
                {
                    $value = recursive_cast_to_array($value);
                }
            }
            return $array;
        }
    }
?>