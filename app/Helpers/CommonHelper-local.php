<?php
use Illuminate\Support\Facades\Schema;
	class CommonHelper
	{
		public static function debug($data, $exit = false)
		{
			if (is_object($data) || is_array($data))
			{
				echo "<pre>";
				print_r($data);
				echo "</pre>";
			}
			else
				echo $data;
			if ($exit)
				exit;
		}

		public static function alertMessage($mod = 'success', $message)
		{
			if (strstr($message, "="))
			{
				$msg = explode("=", $message);
				$mod = $msg[0];
				$message = $msg[1];
			}
			$alert = "<div class='alert alert-" . $mod . "' role='alert'>" . $message . "</div>";
			return $alert;
		}

		public static function uploadFile($file, $path = 'media', $file_to_replace = "", $user_id = "")
		{
			if (!empty($file))
			{
				try
				{
					$file_name = (!empty($user_id) ? $user_id . "_" : "") . md5(time());
					$extension = $file->getClientOriginalExtension();
					$file_name = $file_name . "." . $extension;
					$path = base_path("/assets") . $path;
					if (!($dir = is_dir($path)))
					{
						$dir = mkdir($path, '0777', true);
					}
					else
						$dir = $path;

					$uploaded = $file->move($dir, $file_name);
					if (!empty($file_to_replace))
					{
						\File::delete($path . "/" . $file_to_replace);
					}
				}
				catch (\Exception $exp)
				{
					return FALSE;
				}
			}
			if (!empty($uploaded))
			{

				return $file_name;
			}
			else
			{
				return FALSE;
			}
		}

		public static function objToArray($obj = "")
		{
			$array = array();
			if (is_object($obj))
			{
				$array = json_encode($obj);
				$array = json_decode($array, true);
			}
			return $array;
		}
//
//sendMWSRequest('request_report', $settings, $next_token = "", $order_id = "")
		public static function sendMWSRequest($type, $settings, $next_token = "", $order_id = "")
		{
			$result = FALSE;
			//try
			//{
				sleep(3);
				$access_key = $settings    ['access_key_id'];
				$secret_key = $settings    ['secret_key'];
				$application_name = ' Amazon Tool ';
				$application_version = ' 1.0 ';
				$merchant_id = $settings    ['seller_id'];

				$marketplace = $settings    ['marketplace']['marketplace_id'];

				$serviceUrl = "https://mws.amazonservices.com/Orders/2013-09-01";
				$config = array(
					'ServiceURL' => $serviceUrl,
					'ProxyHost' => null,
					'ProxyPort' => -1,
					'ProxyUsername' => null,
					'ProxyPassword' => null,
					'MaxErrorRetry' => 3,
				);



				$service = new MarketplaceWebServiceOrders_Client($access_key,
					$secret_key, $application_name,
					$application_version, $config);

				switch ($type)
				{
					case "list_orders":
						$create_after = date("c", time() - (10 * 24 * 60 * 60));
						$create_before = date("c", time() - (2 * 60));

						$request = new MarketplaceWebServiceOrders_Model_ListOrdersRequest();
						$request->setSellerId($merchant_id);
						$request->setMarketplaceId($marketplace);
						$request->setCreatedAfter($create_after);
						// $request->setCreatedBefore($create_before);
						$result = $service->ListOrders($request);
						$result = (array)($result);
						$result = reset($result);
						$result = reset($result);
						$result = reset($result);
						$result = (array)($result);
						$result = reset($result);

						break;

					case "list_orders_by_next_token":
						if (!empty($next_token))
						{
							$request = new MarketplaceWebServiceOrders_Model_ListOrdersByNextTokenRequest();
							$request->setSellerId($merchant_id);
							$request->setNextToken($next_token);
							$result = $service->ListOrdersByNextToken($request);
							$result = (array)($result);
							$result = reset($result);
							$result = reset($result);
							$result = reset($result);
							$result = (array)($result);
							$result = reset($result);
						}
						break;
					case "list_order_items":
						if (!empty($order_id))
						{
							$request = new MarketplaceWebServiceOrders_Model_ListOrderItemsRequest();
							$request->setSellerId($merchant_id);
							$request->setAmazonOrderId($order_id);
							$result = $service->ListOrderItems($request);
							$result = (array)$result;
							if (!empty($result))
								$result = reset($result);
							if (is_object($result))
								$result = (array)$result;
							if (is_array($result) && !empty($result))
								$result = reset($result);
							if (is_object($result))
								$result = (array)$result;
							if (is_array($result) && !empty($result))
								$result = reset($result);
							if (is_object($result))
								$result = (array)$result;
							if (is_array($result) && !empty($result))
								$result = reset($result);

						}
						break;
					case "request_report":
						$serviceUrl = "https://mws.amazonservices.com";
						$config = array(
							'ServiceURL' => $serviceUrl,
							'ProxyHost' => null,
							'ProxyPort' => -1,
							'MaxErrorRetry' => 3
						);

						$marketplaceIdArray = array("Id" => array($settings['marketplace']['marketplace_id']));
						$parameters = array (
										'Merchant' => $merchant_id,
										'MarketplaceIdList' => $marketplaceIdArray,
										'ReportType' => '_GET_XML_ALL_ORDERS_DATA_BY_ORDER_DATE_',
										'ReportOptions' => 'ShowSalesChannel=true',
										'MWSAuthToken' => $access_key,
										'StartDate' => date('Y-m-d',strtotime("-2 days")),
										'EndDate' => date('Y-m-d',strtotime("-2 days"))
										);



						$request = new MarketplaceWebService_Model_RequestReportRequest($parameters);
						$service = new MarketplaceWebService_Client(
										$access_key,
										$secret_key,
										$config,
										$application_name,
										$application_version
										);

						$result = $service->requestReport($request);
						if($result instanceof MarketplaceWebService_Model_RequestReportResponse)
						{
							$response	=	$result->getRequestReportResult();
							for($i=0; $i<4; $i++)
							{
								if(!empty($response))
								{
									if (is_array($response))
										$response = reset($response);
									else
									{
										$response = (array)($response);
										$response = reset($response);
									}
								}
							}

						}
						if(!empty($response))
							$result	=	$response;
						else
							$result	=	FALSE;

						break;
					case "request_report_list":
						$serviceUrl = "https://mws.amazonservices.com";
						$config = array(
							'ServiceURL' => $serviceUrl,
							'ProxyHost' => null,
							'ProxyPort' => -1,
							'MaxErrorRetry' => 3,
						);

						$service = new MarketplaceWebService_Client(
							$access_key,
							$secret_key,
							$config,
							$application_name,
							$application_version
						);

						$parameters = array (
							'Merchant' => $merchant_id,
							'MWSAuthToken' => $access_key
						);

						$request = new MarketplaceWebService_Model_GetReportRequestListRequest($parameters);
						$response = $service->getReportRequestList($request);


						\CommonHelper::debug($response,true);

						break;
					case "get_report":

						$serviceUrl = "https://mws.amazonservices.com";

						$config = array(
							'ServiceURL' => $serviceUrl,
							'ProxyHost' => null,
							'ProxyPort' => -1,
							'MaxErrorRetry' => 3,
						);
						$service = new MarketplaceWebService_Client(
							$access_key,
							$secret_key,
							$config,
							$application_name,
							$application_version
						);

						$reportId = '1245877133016840';

						$parameters = array (
						  'Merchant' => $merchant_id,
						  'Report' => @fopen('php://memory', 'rw+'),
						  'ReportId' => $reportId,
						   'MWSAuthToken' => $access_key
						 );
						$request = new MarketplaceWebService_Model_GetReportRequest($parameters);
						$response = $service->getReport($request);
					$dump	=stream_get_contents($request->getReport());
						$xml = simplexml_load_string($dump);
						$json = json_encode($xml);
						$dump = json_decode($json,TRUE);
					\CommonHelper::debug($dump,true);

						echo("Service Response\n");
						echo("=============================================================================\n");

						echo("<br/><br/>        GetReportResponse\n<br/><br/>");
						if ($response->isSetGetReportResult()) {
							$getReportResult = $response->getGetReportResult();
							echo("<br/><br/>            GetReport <br/><br/>");

							if ($getReportResult->isSetContentMd5()) {
								echo("<br/><br/>                ContentMd5<br/><br/>");
								echo("<br/><br/>                " . $getReportResult->getContentMd5() . "\n");
							}
						}
						if ($response->isSetResponseMetadata()) {
							echo("<br/><br/>            ResponseMetadata\n");
							$responseMetadata = $response->getResponseMetadata();
							if ($responseMetadata->isSetRequestId()) {
								echo("<br/><br/>               RequestId\n");
								echo("<br/><br/>                    " . $responseMetadata->getRequestId() . "\n");
							}
						}

						echo("<br/><br/>        Report Contents\n<br/><br/><br/><br/>");
						echo(stream_get_contents($request->getReport()) . "\n");

						echo("<br/><br/>            ResponseHeaderMetadata: " . $response->getResponseHeaderMetadata() . "\n");

						break;
					default:
						break;
				}

			//}
			/*catch (Exception $ex)
			{
				echo "<h1>error:=" . $ex->getMessage() . "</h1>";

				$result = FALSE;
			}*/

			return $result;

		}

		public static function find_or_create_table($profile_id, $type)
		{
			try
			{
				$table = FALSE;
				switch ($type)
				{
					case "orders":
							$table = "amazon_" . $type . "_" . $profile_id;
							if (!Schema::hasTable($table))
							{
								Schema::connection('mysql')->create($table, function ($table)
								{
									$table->increments('id');
									$table->string('order_id', 50);
									$table->string('buyer_name', 200);
									$table->string('buyer_email', 100);


									$table->dateTime('earliest_ship_date')->default('0000-00-00 00:00:00');
									$table->dateTime('latest_ship_date')->default('0000-00-00 00:00:00');
									$table->dateTime('purchase_date')->default('0000-00-00 00:00:00');
									$table->string('fulfillment_channel', 10)->nullable();
									$table->string('shipping_address', 100)->nullable();
									$table->string('shipping_city', 20)->nullable();
									$table->string('shipping_state', 15)->nullable();
									$table->string('shipping_postal_code', 20)->nullable();
									$table->string('shipping_country', 5)->nullable();
									$table->string('marketplace_id', 20)->nullable();
									$table->string('shipping_name', 50)->nullable();
									$table->string('shipping_phone',30)->nullable();


									$table->string('status', 50);
									$table->integer('setting_id');
									$table->dateTime('created_before');
									$table->dateTime('created_at')->default('0000-00-00 00:00:00');
									$table->dateTime('updated_at')->default('0000-00-00 00:00:00');
									$table->softDeletes();
								});
							}
							break;
					case "emails":
							$table = "amazon_" . $type . "_" . $profile_id;
							if (!Schema::hasTable($table))
							{
								Schema::connection('mysql')->create($table, function ($table)
								{
									$table->increments('id');
									$table->string('order_id', 50);
									$table->string('buyer_name', 200);
									$table->string('buyer_email', 100);
									$table->string('status', 50);
									$table->integer('setting_id');
									$table->dateTime('created_before');
									$table->dateTime('created_at')->default('0000-00-00 00:00:00');
									$table->dateTime('updated_at')->default('0000-00-00 00:00:00');
									$table->softDeletes();
								});
							}

							break;
					case "feedbacks":
							$table = "amazon_" . $type . "_" . $profile_id;
							if (!Schema::hasTable($table))
							{
								Schema::connection('mysql')->create($table, function ($table)
								{
									$table->increments('id');
									$table->integer('setting_id');
									$table->string('rater', 50);
									$table->char('rating', 2);
									$table->enum('status', ['new','waiting_for_user','responded','ticket_open','resolved','wontfix'])->nullable();
									$table->string('rating_text', 500)->nullable();
									$table->string('responseRatingData',5)->nullable();
									$table->string('hasResponse',5)->nullable();
									$table->dateTime('rating_date')->default('0000-00-00 00:00:00');
									$table->dateTime('created_at')->default('0000-00-00 00:00:00');
									$table->dateTime('updated_at')->default('0000-00-00 00:00:00');
									$table->softDeletes();

								});
							}
							break;
					default:
						break;

				}

			}
			catch (\Exception $exp)
			{
				echo $exp->getMessage(); exit;
			}

			return $table;
		}


		public static function composeMessage($message_id, $order_id)
		{
			$message = \Message::findOrFail($message_id);
			$order = \AmazonOrder::findOrFail($order_id);
			if ($message && $order)
			{
				$message = $message->toArray();
				$order = $order->toArray();
				/*
				 *
				 *
				 *
				 *
				 *
				 *
				 * [[logo]]	The logo for your store	Please upload a logo on the Settings page first
[[logo-link]]	The logo for your store, which links to your Amazon store	Please upload a logo on the Settings page first
[[logo-link-alt]]	The logo for your store, with alternate link to your Amazon store (page with listings and no feedback)	Please upload a logo on the Settings page first
[[buyer-name]]	The buyer's full name	John Doe
[[first-name]]	The buyer's first name	John
[[product-name]]	The name of the item that the customer bought	X-Box 360 Slim
[[order-id]]	The Customer order ID	105-4697172-1635425
[[msku]]	Your Merchant SKU (msku) for the item	ABC-123
[[asin]]	The Amazon ASIN for the product	B000123456
[[quantity]]	The quantity ordered	1
[[price-item]]	The Price of the item (without currency sign)	29.99
[[price-shipping]]	The amount paid for shipping (without currency sign)	3.99
[[condition-note]]	The 'condition-note' provided with the listing
[[buyer-email]]	The encoded email address of the buyer	cfabcdes1h1ty82@marketplace.amazon.com
[[recipient]]	Recipient Name (may not be the same as the buyer)	John Doe
[[ship-address1]]	Address Line 1	123 Oak Street
[[ship-address2]]	Address Line 2	Apt #3A
[[ship-city]]	Shipping City	Athens
[[ship-state]]	Shipping State	GA
[[ship-zip]]	Postal Code Code	30601
[[ship-country]]	Shipping Country	US
[[carrier]]	**The delivery carrier	UPS
[[tracking-number]]	**Carrier Tracking Number	1Z66A4719871236251
[[estimated-arrival]]	**Estimated Arrival Date (From Amazon's data - ONLY AVAILABLE FOR FBA Orders)	2013-04-20
				 */
			}
		}

		public static function isLocal()
		{
			$locals = array();
			$locals[] = "127.0.0.1";
			$locals[] = "::1";

			$current_server = \Request::server();
			if (isset($current_server['REMOTE_ADDR']))
				if (in_array($current_server['REMOTE_ADDR'], $locals))
					return TRUE;
			return FALSE;
		}

		public static function saveLog($file, $data)
		{
			$path = public_path() . "/logs/";

			if (!($dir = is_dir($path)))
			{
				$dir = mkdir($path, '0777', true);
			}
			if ($dir)
			{
				$myfile = file_put_contents($path . $file . ".txt", PHP_EOL . json_encode((array)$data, 5) . PHP_EOL, FILE_APPEND);
				return TRUE;
			}
			return FALSE;
		}

		public static function recursive_cast_to_array($object)
		{
			$array = (array)$object;
			foreach ($array as &$value)
			{
				if (is_object($value))
				{
					$value = recursive_cast_to_array($value);
				}
			}
			return $array;
		}

	}

?>