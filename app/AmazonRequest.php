<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AmazonRequest extends Model {

    public function __construct() {
        
    }

    public static function getResult($params = array(), $endpoint) {
        $aws_access_key_id = env('SES_KEY');
        $aws_secret_key = env('SES_SECRET');

        $uri = "/onca/xml";
        if (!isset($params["Timestamp"])) {
            $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
        }
        ksort($params);

        $pairs = array();

        foreach ($params as $key => $value) {
            array_push($pairs, rawurlencode($key) . "=" . rawurlencode($value));
        }
        $canonical_query_string = join("&", $pairs);
        $string_to_sign = "GET\n" . $endpoint . "\n" . $uri . "\n" . $canonical_query_string;
        $signature = base64_encode(hash_hmac("sha256", $string_to_sign, $aws_secret_key, true));
        $request_url = 'http://' . $endpoint . $uri . '?' . $canonical_query_string . '&Signature=' . rawurlencode($signature);
        $result = file_get_contents($request_url);
        $xml_object = simplexml_load_string(trim($result));

        
    }
    
    public static function getReviewLink($asin, $endpoint) {
        
       $params = array();
        $aws_access_key_id = env('SES_KEY');
        $aws_secret_key = env('SES_SECRET');

        $uri = "/onca/xml";
        if (!isset($params["Timestamp"])) {
            $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
        }
        ksort($params);

        $pairs = array();

        foreach ($params as $key => $value) {
            array_push($pairs, rawurlencode($key) . "=" . rawurlencode($value));
        }
        $canonical_query_string = join("&", $pairs);
        $string_to_sign = "GET\n" . $endpoint . "\n" . $uri . "\n" . $canonical_query_string;
        $signature = base64_encode(hash_hmac("sha256", $string_to_sign, $aws_secret_key, true));
 
        $url_1 = "http://www.amazon.com/reviews/iframe?akid=".$aws_access_key_id."&alinkCode=xm2&asin=".$asin."&atag=".env('SES_TAG')."&exp=";
        $url_2 = urlencode($params["Timestamp"]) . "&v=2&sig=".urlencode($signature);
        return $url_1.$url_2;
    }

    static function curl($params = NULL, $endpoint) {

        $aws_access_key_id = env('SES_KEY');
        $aws_secret_key = env('SES_SECRET');

        $uri = "/onca/xml";

        if (!isset($params["Timestamp"])) {
            $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
        }
        ksort($params);

        $pairs = array();

        foreach ($params as $key => $value) {
            array_push($pairs, rawurlencode($key) . "=" . rawurlencode($value));
        }
        $canonical_query_string = join("&", $pairs);
        $string_to_sign = "GET\n" . $endpoint . "\n" . $uri . "\n" . $canonical_query_string;
        $signature = base64_encode(hash_hmac("sha256", $string_to_sign, $aws_secret_key, true));
        $request_url = 'http://' . $endpoint . $uri . '?' . $canonical_query_string . '&Signature=' . rawurlencode($signature);


        $headers = array(
            'User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0',
            'Accept: text/xml,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Content-type: text/xml;charset="utf-8"',
            'Accept-Encoding: gzip, deflate',
            'Connection: keep-alive'
        );


//        $CURLOPT_PROXYTYPE = 'HTTP';
//        $CURLOPT_PROXY = '204.152.206.145';
//        $CURLOPT_PROXYPORT = '55555';
//        $CURLOPT_PROXYUSERPWD = 'claytons:321123bp';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $request_url);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1); // automatically set the Referer: field in requests where it follows a Location: redirect.
        curl_setopt($ch, CURLOPT_REFERER, $request_url);
        curl_setopt($ch, CURLOPT_ENCODING, ''); // Hack for 'Accept - Encoding: gzip, deflate'
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);


//        if (!empty($CURLOPT_PROXY)) {
//            //Proxy setup for curl start
//            curl_setopt($ch, CURLOPT_PROXYTYPE, $CURLOPT_PROXYTYPE);
//            curl_setopt($ch, CURLOPT_PROXY, $CURLOPT_PROXY);
//            curl_setopt($ch, CURLOPT_PROXYPORT, $CURLOPT_PROXYPORT);
//            curl_setopt($ch, CURLOPT_PROXYUSERPWD, $CURLOPT_PROXYUSERPWD);
//            //Proxy setup for curl end
//        }

        $response = false;
        if (!curl_errno($ch)) {
            $response = curl_exec($ch);
        }
        curl_close($ch);

        $response = trim($response);
        $xml_object = simplexml_load_string(trim($response));

        return $xml_object;
    }

}
