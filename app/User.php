<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Validator;
use Session;
use Illuminate\Support\Facades\Schema;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
//use Illuminate\Support\Facades\Request;


class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];



    protected  static $validation_rules  = array('user_email'     =>  "required|email",'user_password'   =>  "required");
    private static $validator;

    public static  function validateCredentials($data=array())
    {

        $rules = User::$validation_rules;

        $validator = Validator::make($data, $rules);
        self::$validator = $validator;
        return $validator->passes();
    }

    public static function validate_remember_me(Request $request)
    {
        if(!empty($request))
        {
            $old_cookie     =   $request->cookie('remember_me');
            if(!empty($old_cookie))
            {
                $user   =   User::where(array('remember_token'=>$old_cookie,
                                              'status'=>'ACTIVE',
                                              ))
                                ->where('expired_at',">", Carbon::now())
                                 ->get();
                if(!$user->isEmpty())
                {
                    $user   =   $user->first();
                    $user   =   $user->toArray();

                    self::createSession($user,TRUE);
                    return TRUE;
                }
                else
                    \Cookie::forget('remember_me');
            }
        }
        return FALSE;
    }
    private static function refresh_token(Request $request)
    {
        if ($request->input('remember_token') && $request->input('user_email'))
        {
            $old_cookie     =   $request->cookie('remember_me');
            if(!empty($old_cookie))
            {
                \Cookie::forget('remember_me');
            }
            $now            =   time();
            $remember_token = md5($now);
            $expired_at     = $now+(24*60*60);
            return array('remember_token' => $remember_token , 'expired_at' =>  $expired_at);
        }
        return FALSE;
    }
    public static function loginUser(Request $request,$data=array())
    {
        $result = array("status" => '3', "message" => " Email or Password incorrect! ");

        if (!empty($data))
        {
            if (User::validateCredentials($data))
            {
                $remember_me    =   self::validate_remember_me($request);
                if($remember_me)
                {
                    $result['status']   =   '1';
                    $result['message']   =   "You are already logged in";
                    return $result;
                }
                $authentic_data = self::authenticate_bpc($data['user_email'], $data['user_password']);
                if (!empty($authentic_data) && is_array($authentic_data))
                {
                    $user_exists = User::where(array("profile_id" => $authentic_data['PROFILEID'], "email" => $authentic_data['EMAIL']))
                        ->get();
                    $authentic_data ['remember_token']  =   "";
                    $authentic_data ['expired_at']      =   "";
                    if($request->input("remember_token"))
                    {
                        $token_data = self::refresh_token($request);
                        if(!empty($token_data['remember_token']) && !empty($token_data['expired_at']))
                        {
                            $authentic_data ['remember_token']  =   $token_data['remember_token'];
                            $authentic_data ['expired_at']      =   $token_data['expired_at'];
                        }
                    }
                    if($user_exists->isEmpty())
                    {
                        self::registeUser($authentic_data,$request);
                    }
                    else
                    {

                        $user_exists    =   $user_exists->first();
                        if(!empty($token_data['remember_token']) && !empty($token_data['expired_at']))
                        {
                            $user_exists->remember_token = $authentic_data ['remember_token'];
                            $user_exists->expired_at = date("Y-m-d h:i:s", $authentic_data ['expired_at']);
                        }
                        $user_exists->save();

                    }
                    self::createSession($authentic_data);
                    $result['status'] = "1";
                    $result['remember_token'] = $authentic_data ['remember_token'] ;
                    $result['expired_at'] =  $authentic_data ['expired_at'];
                    $result['message'] = " Welcome ";
                }

            }
            else
            {
                $result['validator'] = self::$validator;
                self::$validator = "";
                $result ["status"] = '2';
                $result['message'] = " Validation Failed. ";
            }

        }
        return $result;
    }



    private static function authenticate_bpc ( $email='' , $password='' )
    {
        if (empty($email) || empty($password ))
            return FALSE;

        try
        {
            //BPC SOAP CALLS
            $url = 'http://blueprintcentral.net/fn_authenticate.asp';
            $postData = 'applicationid=161&';
            $postData .= 'email=' . $email . '&';
            $postData .= 'password=' . $password . '&';
            $postData .= 'resultformat=xml';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, false);

            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
            $output = curl_exec($ch);
            curl_close($ch);

            $xml_object = simplexml_load_string(trim($output));


            $xml_array = (array)$xml_object->Result;

            if ($xml_array['ERROR'] != '*')
                return FALSE;
            else
            {
                return $xml_array;
            }
        }
        catch(\Exception $exp)
        {
            return FALSE;
        }
    }

    private static function registeUser ( $data=array(),Request $request )
    {
        $record = new User();
        if (isset($data['FIRSTNAME']))
            $record->first_name = $data['FIRSTNAME'];
        if (isset($data['LASTNAME']))
            $record->last_name = $data['LASTNAME'];
        if (isset($data['PROFILEID']))
            $record->profile_id = $data['PROFILEID'];
        if (isset($data['EMAIL']))
            $record->email = $data['EMAIL'];
        if (isset($data['ADMIN']))
            $record->is_admin = $data['ADMIN'];

        if (isset($data['remember_token']))
            $record->remember_token = $data['remember_token'];
        if (isset($data['expired_at']))
            $record->expired_at = date("Y-m-d h:i:s",$data['expired_at']);

        $record->created_at = date('Y-m-d H:i:s');
        $record->updated_at = date('Y-m-d H:i:s');
/*        $record->is_approved = 1;*/
        $record->save();
    }

    private static function createLog(array $data  = array())
    {
        // Make an entry in Log Book
        $log_entry = new LogEntry();
        $log_entry->log_ip = \Request::ip();;
        $log_entry->logged_on = date('Y-m-d H:i:s');
        $log_entry->bpc_profile_id = (!empty($data['PROFILEID']) ? $data['PROFILEID'] : '');;
        $log_entry->save();
    }
    private static function createSession ( array $data  = array(),$local_data=FALSE )
    {

        $session_data = array();

        if(!empty($local_data))
        {
            $session_data['first_name'] = (!empty($data['first_name']) ? $data['first_name'] : '');
            $session_data['last_name'] = (!empty($data['last_name']) ? $data['last_name'] : '');
            $session_data['profile_id'] = (!empty($data['profile_id']) ? $data['profile_id'] : '');
            $session_data['email'] = (!empty($data['email']) ? $data['email'] : '');
            $session_data['is_admin'] = (!empty($data['is_admin']) ? $data['is_admin'] : '');

        }
        else
        {
            $session_data['first_name'] = (!empty($data['FIRSTNAME']) ? $data['FIRSTNAME'] : '');
            $session_data['last_name'] = (!empty($data['LASTNAME']) ? $data['LASTNAME'] : '');
            $session_data['profile_id'] = (!empty($data['PROFILEID']) ? $data['PROFILEID'] : '');
            $session_data['email'] = (!empty($data['EMAIL']) ? $data['EMAIL'] : '');
            $session_data['is_admin'] = (!empty($data['ADMIN']) ? $data['ADMIN'] : '');
        }
        $session_data['is_logged'] = TRUE;

        Session::set('user', $session_data);

        User::createLog($data);

    }
    public static function getActiveUsersWithMessages()
    {
        try
        {
            $users  =   array();
            $users = User::where(array('status' => "Active"))
                ->with("messages")
                ->whereHas('messages',function($query){
                    $query->where('status', "Active");
                })
                ->get();
            if(!$users->isEmpty())
            {
                $users = $users->toArray();
                foreach($users as $key=>$user)
                {
                    $table  =   "amazon_orders_".$user['profile_id'];
                    if(Schema::hasTable($table))
                    {
                        $results    =   \DB::table($table)->count();
                        if(!empty($results))
                        {
                            continue;
                        }
                    }
                    unset($users[$key]);
                }
            }

        }
        catch(\Exception $exp)
        {
            echo $exp->getMessage();
            exit;
            $users  =   array();
        }
        return $users;
    }

    public function messages()
    {
        return $this->hasMany("App\Message","profile_id","profile_id");
    }
    public function settings()
    {
        return $this->hasMany("App\UserAmazonSetting","profile_id","profile_id");
    }

}
