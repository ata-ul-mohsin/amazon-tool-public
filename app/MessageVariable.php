<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;

class MessageVariable extends Model
{
    use SoftDeletes;

    protected  static $validation_rules  = array('name'     =>  "required",'value'   =>  "required", "profile_id"    => "required");
    private static $validator;
    protected $dates = ['deleted_at'];

    public static  function validateVariable($data=array())
    {
        $rules              =   MessageVariable::$validation_rules;
        if(!empty($data['variable_id']))
            $rules ["variable_id"]   =   'required|integer|min:0';

        $validator              =   Validator::make($data,$rules);
        self::$validator        =   $validator;
        return $validator->passes();
    }

    public static function saveToDb($data=array())
    {
        $result =   array("status"=>'3',"message"=>" Error in saving data! ");

        if(!empty($data))
        {
            if(MessageVariable::validateVariable($data))
            {
                if(!empty($data['variable_id']))
                {
                    $variable        =   MessageVariable::where('id', $data['variable_id'])->first();
                    if (!$variable)
                    {
                        $result['message']  =   " Message not found in the system. ";
                        return $result;
                    }
                }
                else
                {
                    $variable = new MessageVariable();
                    $variable->profile_id = $data['profile_id'];
                    $variable->created_at = date("Y-m-d h:i:s");
                }

                $variable->name                             =   $data['name'];
                $variable->value                           =   $data['value'];

                if($variable->save())
                {
                    $result ["status"]   =   '1';
                    $result ["message"]  =   " Successfully saved to database ";
                }
            }
            else
            {
                $result['validator'] =   self::$validator;
                self::$validator     =   "";
                $result ["status"]   =   '2';
                $result['message']   =   " Validation Failed. ";
            }

            return $result;
        }
    }
    public static function deleteVariable($id="")
    {
        try
        {
            $variable = MessageVariable::findOrFail($id);
            if($variable)
            {
                $variable->delete();
                return TRUE;
            }
        }
        catch(\Exception $ex)
        {

        }
        return FALSE;
    }
}