<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Validator;
class MessageSetting extends Model
{
    protected  static $validation_rules  = array('seller_central_email' =>  "email",
                                                 'testing_to_email'     =>  "email",
                                                 "profile_id"           =>    "required");
    private static $validator;
    private static $logo_dir;
    public function __construct()
    {
        self::$logo_dir     =   '/users/messages/logos';
    }
    public static  function validateSetting($data=array())
    {
        $rules                  =   MessageSetting::$validation_rules;
        $validator              =   Validator::make($data,$rules);
        self::$validator        =   $validator;
        return $validator->passes();
    }
    public static function saveToDb($data=array())
    {
        $result =   array("status"=>'3',"message"=>" Error in saving data! ");

        if(!empty($data))
        {
            $oldLogo    =   "";
            if(MessageSetting::validateSetting($data))
            {

                $setting        =   MessageSetting::where('profile_id', $data['profile_id'])->first();

                if(empty($setting))
                {
                    $setting = new MessageSetting();
                    $setting->created_at = date("Y-m-d h:i:s");
                }

                $setting->updated_at = date("Y-m-d h:i:s");


                if(isset($data['account_name']))
                    $setting->account_name                   =   $data['account_name'];

                if(isset($data['seller_central_email']))
                    $setting->seller_central_email                        =   $data['seller_central_email'];


                if(isset($data['testing_to_email']))
                    $setting->testing_to_email            =   $data['testing_to_email'];

                if(isset($data['time_zone']))
                    $setting->time_zone                    =   $data['time_zone'];

                if(isset($data['profile_id']))
                    $setting->profile_id                      =   $data['profile_id'];


                if($setting->save())
                {
                    $result ["status"]   =   '1';
                    $result ["message"]  =   " Successfully saved to database. ";
                }

            }
            else
            {
                $result['validator'] =   self::$validator;
                self::$validator     =   "";
                $result ["status"]   =   '2';
                $result['message']   =   " Validation Failed. ";
            }
            return $result;
        }
    }
}