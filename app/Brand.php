<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Session;

class Brand extends Model
{

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    private static $logo_dir;

    public function __construct()
    {
        self::$logo_dir     =   '/users/brands/logos';
    }
    public static function saveToDb($data = array())
    {
        $result = array("status" => '3', "message" => " Error in saving data! ");

        if (!empty($data))
        {
            if(!empty($data['id']))
            {
                $brand = Brand::where(array('setting_id'=>$data['setting_id'],'profile_id'=>$data['profile_id'],'id'=>$data['id']))->first();
                if(!$brand)
                {
                    $result ['message'] =   "brand not found in the system";
                    return $result;
                }
            }
            else
            {
                $brand = new Brand();
                $brand->created_at = date("Y-m-d h:i:s");
            }

            $brand->updated_at = date("Y-m-d h:i:s");


            if (isset($data['name']))
                $brand->name = $data['name'];

            if (isset($data['signature']))
                $brand->signature = $data['signature'];

            if (isset($data['setting_id']))
                $brand->setting_id = $data['setting_id'];

            if (isset($data['profile_id']))
                $brand->profile_id = $data['profile_id'];


            if ($brand->save())
            {
                $result ["status"] = '1';
                $result ["message"] = " Successfully saved to database. ";
                $logo_msg = " no logo uploaded ";


                if (\Input::hasFile('brand_logo'))
                {
                    $new_logo = \CommonHelper::uploadFile(\Input::file('brand_logo'), self::$logo_dir, $brand->logo, $brand->profile_id);
                    if ($new_logo)
                    {
                        $brand->logo = $new_logo;
                        $brand->update();
                        $logo_msg = " new logo uploaded ";
                    }
                }
                $result['message'] .= $logo_msg;
            }
        }
        return $result;
    }
    public static function getBrands($profile_id="",$setting_id="",$is_default=TRUE)
    {
        try
        {
            $brands =   array();
            if (empty($profile_id))
            {
                $user = Session::get('user');
                $profile_id = $user['profile_id'];
            }

            $brands = Brand::where(array("profile_id" => $profile_id))
                ->whereNull('deleted_at');

            if (!empty($is_default))
            {
                $brands = $brands->with("sellers")
                    ->whereHas("sellers",function ($query)
                    {
                        $query->where("user_amazon_settings.is_default", "=", "TRUE");
                    });
            }
            elseif (!empty($setting_id))
            {
                $brands = $brands->with("sellers")
                    ->whereHas("sellers",function ($query) use ($setting_id)
                    {
                        $query->where("user_amazon_settings.id", "=", $setting_id);
                    });
            }
            else
            {
                $brands = $brands->with("sellers");
            }
            $brands = $brands->orderBy("setting_id", "ASC")->get();


            if (!$brands->isEmpty())
                $brands = $brands->toArray();
        }
        catch(\Exception $exp)
        {
            echo $exp->getMessage();exit;
        }
        if(!is_array($brands))
            $brands=    array();

        return $brands;
    }

    public static function getBrandsBySetting($params = array(), $profile_id = "", $setting_id = "")
    {
        try
        {

            $brands = array();
            $no_of_rec = $params['length'];
            $search_cols = $params['columns'];
            $setting_id = $search_cols[3]['search']['value'];
            $global_search = $params['search']['value'];
            $skip = $params['start'];
            $i = $skip + 1;
            $count = 0;

            if (empty($profile_id))
            {
                $user = Session::get("user");
                $profile_id = $user['profile_id'];

            }
            $setting_where = array();
            $setting_where['profile_id'] = $profile_id;
            $setting_where['setting_type'] = "marketplace";

            if (empty($setting_id))
            {
                $setting_where['is_default'] = "TRUE";

            }
            else
            {
                $setting_where['id'] = $setting_id;
            }

            $default_setting = UserAmazonSetting::where($setting_where)->get();


            if (!$default_setting->isEmpty())
            {
                $default_setting = $default_setting->first();
                $default_setting = $default_setting->toArray();
                $setting_id = $default_setting['id'];
            }

            if (!empty($profile_id) && !empty($default_setting['id']))
            {


                $brands_data = Brand::where("setting_id", $setting_id);


                if (!empty($global_search))
                {
                    $brands_data->where("name", "like", "%" . $global_search . "%");
                }

                $count       =  $brands_data->count();
                $brands_data =  $brands_data->take($no_of_rec);
                $brands_data =  $brands_data->skip($skip);
                $brands_data =  $brands_data->with('sellers');
                $brands_data =  $brands_data->whereNull('deleted_at');
                $brands_data =  $brands_data->get();


                //Brand Name	Logo	Signature	Account Name	Action
                if (!$brands_data->isEmpty())
                {
                    $brands_data = $brands_data->toArray();

                    foreach ($brands_data as $brand)
                    {
                        $logo           =   self::build_logo_html($brand);
                        $action_html    =   self::build_action_html($brand);

                        $brands[] = array($i++,
                            $brand['name'],
                            $logo,
                            $brand['signature'],
                            $brand['sellers']['account_name']." (".$brand['sellers']['seller_id'].") ",
                            $action_html
                         );
                    }
                }


            }
        }
        catch (Exception $exp)
        {
            echo $exp->getMessage();
            exit;
        }
        $brands['total'] = $count;
        $brands['filtered'] = $count;
        return $brands;

    }

    private static function build_logo_html($brand)
    {
        $brand_logo =   "";
        $logo_path  =  asset("assets/".self::$logo_dir)."/";  //http://localhost/amazontool/users/brands/logos/

        if(!empty($brand['logo']))
        {
            $brand_logo=$brand['logo'];
            if(file_exists(base_path("assets/".self::$logo_dir."/".$brand['logo'])))
            {
                $brand_logo =   "<img src='".$logo_path.$brand['logo']."' width='50' height='50' />";
            }
            else
            {
                $brand_logo =   "<img src='".$logo_path."not-found.png' width='50' height='50' />";
            }

        }
        else
            $brand_logo =   "<img src='".$logo_path."brand-logo-placeholder.png' width='50' height='50' />";
        return $brand_logo;
    }

    private static function build_action_html($brand)
    {
        $action_html    =   "";
        if(!empty($brand['id']))
        {
            $action_html    =   "<a href='".route('brands.edit',array('id'=>$brand['id']))."' class='btn btn-default'><i class='fa fa-pencil text'></i></a>&nbsp<a data-href='".route('brands.destroy',array('id'=>$brand['id']))."' class='btn btn-default  confirm_delete'><i class='fa fa-trash-o text'></i></a>";
        }
        return $action_html;
    }
    public function sellers()
    {
        return $this->belongsTo('App\UserAmazonSetting', 'setting_id', 'id');
    }
    public static function deleteBrand(Brand $brand)
    {
        try
        {
            $brand->delete();
            return TRUE;
        }
        catch (\Exception $ex)
        {

        }
        return FALSE;
    }
    public function products()
    {
        return $this->belongsToMany('App\AmazonProduct', 'products_brands', 'id', 'brand_id');

    }
    public static function assignBrandsToProducts($data = array())
    {
        $result = array("status" => '3', "message" => " Error in assigning brand! ");

        if (!empty($data))
        {
            $profile_id =   $data['profile_id'];
            $brand_id   =   $data['brand_select_filter'];
            $setting_id =   $data['marketplace_select_filter'];
            unset($data['profile_id']);
            unset($data['brand_select_filter']);
            unset($data['marketplace_select_filter']);
            if(isset($data['server_sisde_dtable_length']))
                unset($data['server_sisde_dtable_length']);
            if(isset($data['_token']))
                unset($data['_token']);
            if(isset($data['save']))
                unset($data['save']);

            if(!empty($brand_id) && !empty($setting_id) && !empty($profile_id))
            {
                $changed_products   =   array();
                $new_products       =   array();
                $query  =   \DB::table('products_brands')
                            ->where(array("brand_id"=>$brand_id));
                foreach($data as $key=>$product)
                {
                    if(substr( $key, 0, 8 ) === "product_")
                    {
                        $changed_products[] =   $product;
                    }
                    else if(substr( $key, 0, 12 ) === "chk_product_")
                    {
                        $new_products[] =   $product;
                    }
                }

                $products_to_be_assigned    =   AmazonProduct::whereIn("id",$changed_products)->get();

                if(!$products_to_be_assigned->isEmpty())
                {
                    foreach($products_to_be_assigned as $product_assigned)
                    {
                        if(in_array($product_assigned->id,$new_products))
                        {
                            if($product_assigned->brand_id==$brand_id)
                                continue;
                            else
                            {
                                $product_assigned->brand_id = $brand_id;
                                $product_assigned->save();
                            }
                        }
                        else
                        {
                            $old_brand  =   $product_assigned->brand_id;
                                if(empty($old_brand))
                                    continue;
                                else
                                {
                                    if($old_brand == $brand_id)
                                    {
                                        $product_assigned->brand_id = NULL;
                                        $product_assigned->save();
                                    }

                                }
                        }

                    }
                    $result ["status"] = '1';
                    $result ["message"] = " Successfully made new assignments. ";
                }
                /*foreach($changed_products as $new_assignment)
                {
                    $query =   $query->where(array("product_id"=>$new_assignment));
                    $exists =   $query->first();
                    if(in_array($new_assignment,$new_products))
                    {
                        if(!$exists)
                            \DB::table('products_brands')
                                ->insertGetId(array(
                                "product_id"    =>  $new_assignment,
                                "brand_id"      =>  $brand_id
                            ));
                    }
                    else
                        if($exists)
                            $query->delete();

                    $result ["status"] = '1';
                    $result ["message"] = " Successfully made new assignments. ";
                }*/
            }


        }
        return $result;
    }
}