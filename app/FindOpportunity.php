<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

class FindOpportunity extends Model {

    protected $table = 'find_opportunity';
    public $timestamps = false;

    public static function getList($filters = array()) {

        if (!empty($filters['keywords'])) {
            
        } else {
            
        }
    }

    public static function makeQueryResults($inputs = array()) {

        $qry = '';
//        $qry_select = "SELECT p.name, p.price, p.rank, p.est_sales, p.est_revenue, p.reviews, p.rating, p.amazon_link, bn.browsnode_name FROM `find_opportunity` p INNER JOIN browse_nodes bn ON bn.child_browsnode_id = p.child_browsnode_id WHERE 1 And p.rank > 0 ";
//        $qry_count = "SELECT count(p.id) as count FROM `find_opportunity` p INNER JOIN browse_nodes bn ON bn.child_browsnode_id = p.child_browsnode_id WHERE 1 And p.rank > 0 ";
        $qry_select = "SELECT p.name, p.price, p.rank, p.est_sales, p.est_revenue, p.reviews, p.rating, p.amazon_link, p.marketplace FROM `find_opportunity` p WHERE 1 And p.rank > 0 And p.price > 0 ";
        $qry_count = "SELECT count(p.id) as count FROM `find_opportunity` p WHERE 1 And p.rank > 0 And p.price > 0 ";

        if (!empty($inputs['keywords'])) {
            $main_keywords = explode(' ', trim($inputs['keywords']));
            $query_parts = array();
            foreach ($main_keywords as $val) {
                $query_parts[] = " '%" . ($val) . "%'";
            }
            $string = implode(' Or p.name Like ', $query_parts);
            $qry = " And (p.name like " . $string . ") ";
        }

        $qry .= " And p.marketplace = " . $inputs['marketplace'] . " ";

        if (!empty($inputs['category'])) {
            $catStr = implode(",", explode('_', $inputs['category']));
            $qry .= " And p.browsnode_id IN ($catStr) ";
        }

        if (!empty($inputs['top_seller'])) {
            $qry .= " And p.top_seller = 1 ";
        } else {
            $qry .= " And p.top_seller = 0 ";
        }

        if (!empty($inputs['min_price']) && !empty($inputs['max_price'])) {
            $qry .= " And (p.price BETWEEN " . $inputs['min_price'] . " And " . $inputs['max_price'] . ") ";
        } elseif (!empty($inputs['min_price']) && empty($inputs['max_price'])) {
            $qry .= " And (p.price >= " . $inputs['min_price'] . ") ";
        } elseif (empty($inputs['min_price']) && !empty($inputs['max_price'])) {
            $qry .= " And (p.price <= " . $inputs['max_price'] . ") ";
        }

        if (!empty($inputs['min_rank']) && !empty($inputs['max_rank'])) {
            $qry .= " And (p.rank BETWEEN " . $inputs['min_rank'] . " And " . $inputs['max_rank'] . ") ";
        } elseif (!empty($inputs['min_rank']) && empty($inputs['max_rank'])) {
            $qry .= " And (p.rank >= " . $inputs['min_rank'] . ") ";
        } elseif (empty($inputs['min_rank']) && !empty($inputs['max_rank'])) {
            $qry .= " And (p.rank <= " . $inputs['max_rank'] . ") ";
        }

        if (!empty($inputs['min_estsales']) && !empty($inputs['max_estsales'])) {
            $qry .= " And (p.est_sales BETWEEN " . $inputs['min_estsales'] . " And " . $inputs['max_estsales'] . ") ";
        } elseif (!empty($inputs['min_estsales']) && empty($inputs['max_estsales'])) {
            $qry .= " And (p.est_sales >= " . $inputs['min_estsales'] . ") ";
        } elseif (empty($inputs['min_estsales']) && !empty($inputs['max_estsales'])) {
            $qry .= " And (p.est_sales <= " . $inputs['max_estsales'] . ") ";
        }

        if (!empty($inputs['min_estrev']) && !empty($inputs['max_estrev'])) {
            $qry .= " And (p.est_sales BETWEEN " . $inputs['min_estrev'] . " And " . $inputs['max_estrev'] . ") ";
        } elseif (!empty($inputs['min_estrev']) && empty($inputs['max_estrev'])) {
            $qry .= " And (p.est_sales >= " . $inputs['min_estrev'] . ") ";
        } elseif (empty($inputs['min_estrev']) && !empty($inputs['max_estrev'])) {
            $qry .= " And (p.est_sales <= " . $inputs['max_estrev'] . ") ";
        }

        if (!empty($inputs['min_rating']) && !empty($inputs['max_rating'])) {
            $qry .= " And (p.rating BETWEEN " . $inputs['min_rating'] . " And " . $inputs['max_rating'] . ") ";
        } elseif (!empty($inputs['min_rating']) && empty($inputs['max_rating'])) {
            $qry .= " And (p.rating >= " . $inputs['min_rating'] . ") ";
        } elseif (empty($inputs['min_rating']) && !empty($inputs['max_rating'])) {
            $qry .= " And (p.rating <= " . $inputs['max_rating'] . ") ";
        }

        $searchKeywords = $inputs['search']['value'];

        if (!empty($searchKeywords)) {
            $qry .= " And (p.name like '%" . $searchKeywords . "%' or p.price like '%" . $searchKeywords . "%' or p.rank like '%" . $searchKeywords . "%' or p.est_sales like '%" . $searchKeywords . "%' or p.est_revenue like '%" . $searchKeywords . "%' or p.rating like '%" . $searchKeywords . "%') ";
        }

        //===================Count==========================
        $countResQry = $qry_count . $qry;
        //=============================================

        $start = $inputs['start'];
        $length = $inputs['length'];

        $orderColumn = $inputs['order'][0]['column'];
        $orderDirection = $inputs['order'][0]['dir'];

        
        if ($inputs['flag'] == 'true') {
            if ($inputs['defaultvar'] == 'on') {
                $qry .= " Order By p.rank ASC, p.est_sales DESC, p.est_revenue DESC, p.price ASC, p.rating DESC ";
            } else {

                if (isset($inputs['rank_default']) || isset($inputs['estsales_default']) || isset($inputs['estrev_default']) || isset($inputs['price_default']) || isset($inputs['rating_default'])) {
                    $flag = FALSE;
                    $qry2 = '';
                    if (isset($inputs['rank_default'])) {
                        if ($inputs['rank_default'] == 'on') {
                            $qry2 .= " p.rank ASC, ";
                            $flag = TRUE;
                        }
                    }
                    if (isset($inputs['estsales_default'])) {
                        if ($inputs['estsales_default'] == 'on') {
                            $qry2 .= " p.est_sales DESC, ";
                            $flag = TRUE;
                        }
                    }
                    if (isset($inputs['estrev_default'])) {
                        if ($inputs['estrev_default'] == 'on') {
                            $qry2 .= " p.est_revenue DESC, ";
                            $flag = TRUE;
                        }
                    }
                    if (isset($inputs['price_default'])) {
                        if ($inputs['price_default'] == 'on') {
                            $qry2 .= " p.price ASC, ";
                            $flag = TRUE;
                        }
                    }
                    if (isset($inputs['rating_default'])) {
                        if ($inputs['rating_default'] == 'on') {
                            $qry2 .= " p.rating DESC, ";
                            $flag = TRUE;
                        }
                    }
                    $qry2 = rtrim($qry2, ", ");
                    if ($flag) {
                        $qry = $qry . " Order By " . $qry2;
                    } else {
                        //$qry .= " Order By p.rank ASC ";
                    }
                }
            }
        } else {
            if ($orderColumn == 0) {
                if ($orderColumn == 0 && $inputs['draw'] != 1) {
                    $qry .= " Order By p.name $orderDirection ";
                }
            } elseif ($orderColumn == 1) {
                $qry .= " Order By p.price $orderDirection ";
            } elseif ($orderColumn == 2) {
                $qry .= " Order By p.est_sales $orderDirection ";
            } elseif ($orderColumn == 3) {
                $qry .= " Order By p.est_revenue $orderDirection ";
            } elseif ($orderColumn == 4) {
                $qry .= " Order By p.reviews $orderDirection ";
            } elseif ($orderColumn == 5) {
                $qry .= " Order By p.rating $orderDirection ";
            }
        }





//echo $qry;exit;
        $qry .= " LIMIT " . $start . " , " . $length . " ";

        $ResQry = $qry_select . $qry;


        $resultsCount = \DB::select(\DB::raw($countResQry));
        $results = \DB::select(\DB::raw($ResQry));

        $dataField = array();
        foreach ($results as $result) {

            if ($result->marketplace == 1) {
                $currency = '$';
            } elseif ($result->marketplace == 2) {
                $currency = '£';
            }

            $data['name'] = "<a target=_blank href='" . $result->amazon_link . "'>" . $result->name . "</a>";
//            $data['browsnode_name'] = $result->browsnode_name;
            $data['price'] = $currency . $result->price;
            $data['rank'] = $result->rank;
            $data['est_sales'] = $result->est_sales;
            $data['est_revenue'] = $result->est_sales * $result->price;
            $data['reviews'] = $result->reviews;
            $data['rating'] = $result->rating;

            $dataField[] = array_values($data);
        }

        $result1['draw'] = $inputs['draw'];
        $result1['recordsTotal'] = $resultsCount[0]->count;
        $result1['recordsFiltered'] = $resultsCount[0]->count;

        $result1['data'] = $dataField;
        return json_encode($result1);
    }

    public function populate($data) {
        if (array_key_exists('browsnode_id', $data)) {
            $this->browsnode_id = $data['browsnode_id'];
        }
        if (array_key_exists('child_browsnode_id', $data)) {
            $this->child_browsnode_id = $data['child_browsnode_id'];
        }
        if (array_key_exists('price', $data)) {
            $this->price = $data['price'];
        }
        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists('amazon_link', $data)) {
            $this->amazon_link = $data['amazon_link'];
        }
        if (array_key_exists('rank', $data)) {
            $this->rank = $data['rank'];
        }
        if (array_key_exists('asin', $data)) {
            $this->asin = $data['asin'];
        }
        if (array_key_exists('reviews_link', $data)) {
            $this->reviews_link = $data['reviews_link'];
        }
        if (array_key_exists('est_sales', $data)) {
            $this->est_sales = $data['est_sales'];
        }
        if (array_key_exists('est_revenue', $data)) {
            $this->est_revenue = $data['est_revenue'];
        }
        if (array_key_exists('top_seller', $data)) {
            $this->top_seller = $data['top_seller'];
        }
        if (array_key_exists('marketplace', $data)) {
            $this->marketplace = $data['marketplace'];
        }
    }

    static function productsToDB($res, $data = array(), $marketPlace) {

        if ($marketPlace == 'webservices.amazon.com') {
            $marketPlace = 1;
        } elseif ($marketPlace == 'webservices.amazon.co.uk') {
            $marketPlace = 2;
        } else {
            $marketPlace = 0;
        }



        $data['price'] = 0;
        if (isset($res->Items->Item)) {

            foreach ($res->Items->Item as $result) {

                $data['name'] = $result->ItemAttributes->Title;
                $data['asin'] = $result->ASIN;
                $data['marketplace'] = $marketPlace;
                $asin_num = $result->ASIN;
                $data['amazon_link'] = $result->DetailPageURL;
                $data['rank'] = $result->SalesRank;

                if ($data['rank'] <= 50000) {

                    if (isset($result->Offers)) {
                        if (isset($result->Offers->Offer)) {
                            $data['price'] = str_replace(array('$', 'Â£', '£'), '', $result->Offers->Offer->OfferListing->Price->FormattedPrice);
                        }
                    }

                    if ($data['price'] == 0) {
                        if (isset($result->ItemAttributes)) {
                            if (isset($result->ItemAttributes->ListPrice)) {
                                $data['price'] = str_replace(array('$', 'Â£', '£'), '', $result->ItemAttributes->ListPrice->FormattedPrice);
                            }
                        }
                    }
                    $data['reviews_link'] = '';
                    if ($result->CustomerReviews->HasReviews) {
                        $data['reviews_link'] = $result->CustomerReviews->IFrameURL;
                    }
                    $data['est_sales'] = self::calculateEstimatedSales($data['rank']);
                    $data['est_revenue'] = $data['est_sales'] * round(str_replace('$', '', $data['price']));

                    $record = FindOpportunity::where('asin', '=', $asin_num)
                            ->where('top_seller', '=', $data['top_seller'])
                            ->where('browsnode_id', '=', $data['browsnode_id'])
                            //->where('child_browsnode_id', '=', $data['child_browsnode_id'])
                            ->where('marketplace', '=', $marketPlace)
                            ->first();

                    if ($record) {
                        $record->populate($data);
                    } else {
                        $record = new FindOpportunity();
                        $record->populate($data);
                    }
                    $rec = $record->save();
                }
            }
        }
    }

    public static function calculateEstimatedSales($x = '') {

        if ($x >= 1 && $x <= 5) {
            $est_sales = ((7000 - 4000) / 5) * (5 - $x) + 4000;
        }
        if ($x >= 6 && $x <= 20) {
            $est_sales = ((4000 - 3000) / (20 - 5)) * (20 - $x) + 3000;
        }
        if ($x >= 21 && $x <= 35) {
            $est_sales = ((3000 - 2000) / (35 - 20)) * (35 - $x) + 2000;
        }
        if ($x >= 36 && $x <= 100) {
            $est_sales = ((2000 - 1000) / (100 - 35)) * (100 - $x) + 1000;
        }
        if ($x >= 101 && $x <= 200) {
            $est_sales = ((1000 - 500) / (200 - 100)) * (200 - $x) + 500;
        }
        if ($x >= 201 && $x <= 350) {
            $est_sales = ((500 - 250) / (350 - 200)) * (350 - $x) + 250;
        }
        if ($x >= 351 && $x <= 500) {
            $est_sales = ((250 - 175) / (500 - 350)) * (500 - $x) + 175;
        }
        if ($x >= 501 && $x <= 750) {
            $est_sales = ((175 - 120) / (750 - 500)) * (750 - $x) + 120;
        }
        if ($x >= 751 && $x <= 1500) {
            $est_sales = ((120 - 100) / (1500 - 750)) * (1500 - $x) + 100;
        }
        if ($x >= 1501 && $x <= 3000) {
            $est_sales = ((100 - 70) / (3000 - 1500)) * (3000 - $x) + 70;
        }
        if ($x >= 3001 && $x <= 5500) {
            $est_sales = ((70 - 25) / (5500 - 3000)) * (5500 - $x) + 25;
        }
        if ($x >= 5501 && $x <= 10000) {
            $est_sales = ((25 - 15) / (10000 - 5500)) * (10000 - $x) + 15;
        }
        if ($x >= 10001 && $x <= 50000) {
            $est_sales = ((15 - 5) / (50000 - 10000)) * (50000 - $x) + 5;
        }
        if ($x >= 50001 && $x <= 100000) {
            $est_sales = ((5 - 1) / (100000 - 50000)) * (100000 - $x) + 1;
        }
        if ($x < 1 || $x > 100000) {
            $est_sales = 'NA';
        }

        if ($est_sales != 'NA') {
            $est_sales = round($est_sales);
        }

        return $est_sales;
    }

}
